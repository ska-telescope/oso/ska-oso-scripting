# Contributing to OET Scripts

Thank you for taking the time to contribute!

OET Scripts and all packages can be developed locally. For instructions on how to do
this, see the [OET Scripts Documentation](https://developer.skatelescope.org/projects/ska-oso-scripting/en/latest/index.html)
on the SKA developer portal.

For a full guide on how to contribute to the OET Scripts see [Contributing to OET Scripts](https://developer.skatelescope.org/projects/ska-oso-scripting/en/latest/contributing_to_oet_scripts.html)
section of the documentation.

