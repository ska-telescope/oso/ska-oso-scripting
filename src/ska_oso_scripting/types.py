"""
The ska_oso_scripting.types module contains type hints for classes that currently map
to a simple Python primitive but may eventually become real classes with behaviour all
of their own.

For instance, at the time of writing, SubArrayID maps to an integer but may eventually
become a class that whose value is between 1 and 16.
"""

SubarrayID = int
