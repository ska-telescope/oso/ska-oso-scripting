"""
This module contains custom exceptions related to device control
"""
import queue


class SkuidRuntimeError(RuntimeError):
    """Base class for all errors raised by SKUID."""


class DeviceControlError(RuntimeError):
    """Base class for all errors raised by device control."""

    pass


class AlreadySubscribedError(DeviceControlError):
    """Raised when more than one observer tries to subscribe
    to a Tango attribute."""


class EventTimeoutError(DeviceControlError, queue.Empty):
    """
    Exception raised to unblock function while reading an event from the queue.
    Subclasses queue.Empty to add custom messages and value.
    """

    def __init__(
        self,
        msg="Timeout error occurred while read event from queue",
        value=None,
    ):
        super().__init__(msg)
        self.msg = msg
        self.value = value
