import logging
import time
from os import getenv
from pathlib import Path
from typing import Optional, Union

import ska_tmc_cdm.schemas as schemas
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from ska_tmc_cdm.messages.central_node.release_resources import ReleaseResourcesRequest
from ska_tmc_cdm.schemas import CODEC

import ska_oso_scripting.functions.environment as env
from ska_oso_scripting import WORKAROUNDS
from ska_oso_scripting.constant import (
    DEFAULT_TIMEOUT_FOR_ASSIGN_RESOURCE_CMD,
    DEFAULT_TIMEOUT_FOR_RELEASE_RESOURCE_CMD,
)
from ska_oso_scripting.event import lifecycle_topics
from ska_oso_scripting.functions.devicecontrol.common import (
    TANGO_REGISTRY,
    _call_and_wait_for_transition,
    get_cdm_interface,
    get_request_json,
)
from ska_oso_scripting.functions.devicecontrol.tango_executor import Command
from ska_oso_scripting.functions.messages import send_message
from ska_oso_scripting.types import SubarrayID

from .exception import EventTimeoutError

LOGGER = logging.getLogger(__name__)


def get_assign_resources_command(request_json: str) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would allocate
    resources from a sub-array.

    :param request_json: assign resources allocation template
    """
    central_node_fqdn = TANGO_REGISTRY.get_central_node()
    return Command(central_node_fqdn, "AssignResources", request_json)


def assign_resources(
    subarray_id: SubarrayID,
    request: Union[AssignResourcesRequest, str, Path],
    with_processing=True,
    timeout: float = None,
):
    """
    Assign resources to a sub-array with the given ID. The command payload can be
    either a JSON string, a path to a JSON file or a CDM AssignResourcesRequest object.
    If with_processing flag is True and request type is a file path or string, the
    JSON payload will be validated against a CDM object and missing fields (e.g.
    interface or sdp) will be added if required. Setting with_processing flag to False
    means the given JSON will be passed directly to Central Node without any validation.

    :param subarray_id: ID of the sub-array to assign resources to
    :param request: assign resources allocation as a file, JSON string or CDM object
    :param timeout: custom timeout provided while execution of assign resource command
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    kept timeout as optional user can set that value otherwise system can set default value.
    :param with_processing: When True, validate request JSON when loading from
        file or using a JSON string
    """
    request_json = get_request_json(request, AssignResourcesRequest, with_processing)

    if with_processing:
        request_json = _set_eb_id(request_json)
        env.check_environment_for_consistency(request_json)

    command = get_assign_resources_command(request_json)
    subarray_device = TANGO_REGISTRY.get_subarray_node(subarray_id)
    # below is set default timeout value for assign resource.
    timeout = DEFAULT_TIMEOUT_FOR_ASSIGN_RESOURCE_CMD if timeout is None else timeout
    try:
        _call_and_wait_for_transition(
            command,
            [ObsState.RESOURCING, ObsState.IDLE],
            "obsState",
            device_to_monitor=subarray_device,
            timeout=timeout,
        )
    except EventTimeoutError as evterror:
        raise EventTimeoutError(
            "Read event timeout from queue occurred while execution of"
            f"{command.command_name} command"
        ) from evterror
    send_message(lifecycle_topics.subarray.resources.allocated, subarray_id=subarray_id)


def release_resources(
    subarray_id: SubarrayID,
    release_all: bool = False,
    request: ReleaseResourcesRequest = None,
    timeout: float = None,
):
    """
    Release sub-array resources.

    :param subarray_id: the ID of the sub-array to control
    :param release_all: True to release all sub-array resources, False to
        release just those resources specified in the request argument
    :param request: ReleaseResourcesRequest object specifying resources to release.
        Only required if release_all is False
    :param timeout: custom timeout provided while execution of release resource command
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    kept timeout as optional user can set that value otherwise system can set default value.
    """
    if not isinstance(release_all, bool):
        raise ValueError("release_all must be a boolean")
    if release_all is False and request is None:
        raise ValueError("Either release_all or request must be defined")
    if env.is_ska_low_environment() and not release_all:
        # Not releasing all resources is not yet supported in SKA Low
        raise NotImplementedError(
            "Partial resource release not yet supported in SKA Low"
        )

    command = get_release_resources_command(subarray_id, release_all, request)
    # set default timeout value for release resource comamnd.
    timeout = DEFAULT_TIMEOUT_FOR_RELEASE_RESOURCE_CMD if timeout is None else timeout
    # Calling SubArrayNode.ReleaseResources too soon after End will result in
    # a hang as SAN never becomes EMPTY.
    if WORKAROUNDS.pause_before_releaseresources:
        LOGGER.info(
            "Workaround: pausing 1 second before releasing resources "
            "due to SAN/SDP state mismatch"
        )
        time.sleep(2)
    try:
        _call_and_wait_for_transition(
            command,
            [ObsState.RESOURCING, ObsState.EMPTY],
            "obsState",
            device_to_monitor=TANGO_REGISTRY.get_subarray_node(subarray_id),
            timeout=timeout,
        )
    except EventTimeoutError as evterror:
        raise EventTimeoutError(
            "Read event timeout from queue occurred while execution of in "
            f"{command.command_name} command"
        ) from evterror
    send_message(
        lifecycle_topics.subarray.resources.deallocated, subarray_id=subarray_id
    )


def get_release_resources_command(
    subarray_id: SubarrayID,
    release_all,
    request: Optional[ReleaseResourcesRequest] = None,
) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would release
    resources from a sub-array.

    :param subarray_id: the ID of the sub-array to control
    :param release_all: True to release all resources, False to release just
        the resources specified in the request argument
    :param request: ReleaseResourcesRequest object specifying resources to release.
        Only required if release_all is False
    :return: OET Command to release sub-array resources
    """
    central_node_fqdn = TANGO_REGISTRY.get_central_node()
    interface = get_cdm_interface(ReleaseResourcesRequest)

    if release_all is True:
        request = ReleaseResourcesRequest(
            subarray_id=subarray_id, release_all=True, interface=interface
        )
    elif request.interface is None:
        request.interface = interface
    request_json = schemas.CODEC.dumps(request)
    return Command(central_node_fqdn, "ReleaseResources", request_json)


def _set_eb_id(request_json):
    if (eb_id := getenv("EB_ID")) is None:
        LOGGER.warning(
            "EB_ID environment variable is not set, so it cannot be inserted into the AssignResourcesRequest. "
            "An Execution Block should have been created at the start of the session using the "
            "ska-oso-scripting.create_eb function. See the RTD for more details."
        )
        return request_json

    request_object: AssignResourcesRequest = CODEC.loads(
        AssignResourcesRequest, request_json
    )
    request_object.sdp_config.execution_block.eb_id = eb_id
    return CODEC.dumps(request_object)
