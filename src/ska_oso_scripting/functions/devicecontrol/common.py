import contextvars
import copy
import json
import logging
import os
from pathlib import Path
from typing import Any, Iterable, NamedTuple, Union

import ska_telmodel.tmc.version as telmodel_tmc_version
import ska_tmc_cdm.messages.central_node.release_resources as cdm_release
import ska_tmc_cdm.messages.subarray_node.scan as cdm_scan
import tango
from ska_ser_log_transactions import transaction
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm.schemas import CODEC

from ska_oso_scripting import WORKAROUNDS
from ska_oso_scripting.functions import environment
from ska_oso_scripting.functions.devicecontrol.tango_executor import (
    Attribute,
    Command,
    TangoExecutor,
)
from ska_oso_scripting.types import SubarrayID

scan_duration = contextvars.ContextVar("scan_duration")

LOGGER = logging.getLogger(__name__)

WAIT_FOR_TRANSITION_SUCCESS_RESPONSE = "SUCCESS"
WAIT_FOR_TRANSITION_FAILURE_RESPONSE = "FAILURE"

# Type hint for transition monitoring functions so that they can monitor
# default Tango device states and custom enumeration states
DeviceStateTypes = Union[ObsState, tango.DevState]


class TangoRegistry:  # pylint: disable=too-few-public-methods
    """
    Registry used to look up Tango FQDNs.

    This is a simple class used to decouple TangoExecutor from Tango FQDNs.
    At some point, this class could change to do something more complicated,
    e.g., perform database lookups, or perhaps the FQDNs might be specified
    on the domain classes, but for now these are simple string formatting
    methods.
    """

    @staticmethod
    def get_central_node():
        """
        Get the FQDN of the CentralNode appropriate to the object.
        """
        default = f"{environment.get_current_env()}/tm_central/central_node"
        return os.getenv("CENTRALNODE_FQDN", default)

    @staticmethod
    def get_subarray_node(subarray_id: SubarrayID):
        """
        Get the FQDN of the Subarray appropriate to the object.
        """
        default = f"{environment.get_current_env()}/tm_subarray_node"
        prefix = os.getenv("SUBARRAYNODE_FQDN_PREFIX", default)
        return f"{prefix}/{subarray_id}"


# Used as a singleton to look up Tango device FQDNs
TANGO_REGISTRY = TangoRegistry()

# Used a singleton to execute Tango commands. The object is kept as a
# module attribute so that tests can mock the executor's 'execute()'
# function.
EXECUTOR = TangoExecutor()


class ValueTransitionError(Exception):
    """
    Exception raised when unexpected ObsState is encountered
    """

    def __init__(
        self,
        value,
        expected_value,
        attribute,
        msg="Unexpected value transition",
    ):
        super().__init__(msg)
        self.msg = msg
        self.value = value
        self.expected_value = expected_value
        self.attribute = attribute

    def __str__(self):
        return (
            f"{self.msg}: Expected {self.attribute} to transition to"
            f" {self.expected_value} but instead received {self.value}"
        )


class WaitForTransitionResponse(NamedTuple):
    """
    Represent the status response from wait_for_attribute_value() function
    """

    response_msg: str
    final_value: DeviceStateTypes


def _call_and_wait_for_transition(
    command: Command,
    target_transitions: Iterable[DeviceStateTypes],
    attribute_name: str,
    device_to_monitor: str = None,
    optional_transitions: Iterable[DeviceStateTypes] = None,
    timeout: float = None,
):
    """
    Send a command and block until attribute value has transitioned to/through the
    requested target state(s).

    :param command: command to execute
    :param target_transitions: happy path transitions
    :param attribute_name: attribute which is monitored
    :param device_to_monitor: optional device for attribute monitoring, if None the device defined
        in command.device is used.
    :param optional_transitions: optional states that are not the target state but should not be
        counted as error states either
    :return: Command response
    :param timeout: custom timeout provided while execution of command's
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    """
    if device_to_monitor is None:
        device_to_monitor = command.device

    attribute = Attribute(device_to_monitor, attribute_name)
    try:
        LOGGER.info(
            "Using pub/sub to track %s of %s",
            attribute_name,
            device_to_monitor,
        )
        event_id = EXECUTOR.subscribe_event(attribute)
    except tango.DevFailed as dev_error:
        LOGGER.error(
            "Could not subscribe to %s of %s. "
            "Check that LMC base class version is 0.6.1 or newer.",
            attribute_name,
            device_to_monitor,
        )
        raise dev_error

    try:
        if command.command_name in ["AssignResources", "Configure"]:
            """
            This block will execute and add the transaction id in the request payload
            of AssignResource and Configure command.
            """
            with transaction(command.command_name, logger=LOGGER) as transaction_id:
                command_with_txn = copy.deepcopy(command)
                command_args = list(command_with_txn.args)
                payload = json.loads(command_args[0])
                payload["transaction_id"] = transaction_id
                if "tmc" in payload and "scan_duration" in payload["tmc"]:
                    scan_duration.set(payload["tmc"]["scan_duration"])
                command_args[0] = json.dumps(payload)
                command_with_txn.args = tuple(command_args)

                LOGGER.info(f"Added transaction_id to command: {command_with_txn}")
                response = execute_command_and_wait_for_transition(
                    command=command_with_txn,
                    target_transitions=target_transitions,
                    attribute=attribute,
                    optional_transitions=optional_transitions,
                    timeout=timeout,
                )
        else:
            """
            else block will execute for the commands ( restart, abort, telescopeOn, telescopeOff)
            where request payload input is not needed.
            """
            response = execute_command_and_wait_for_transition(
                command=command,
                target_transitions=target_transitions,
                attribute=attribute,
                optional_transitions=optional_transitions,
                timeout=timeout,
            )
    finally:
        EXECUTOR.unsubscribe_event(attribute, event_id)

    return response


def execute_command_and_wait_for_transition(
    command: Command,
    target_transitions: Iterable[DeviceStateTypes],
    attribute: Attribute,
    optional_transitions: Iterable[DeviceStateTypes] = None,
    timeout: float = None,
):
    """
    Send a command and block until attribute value has transitioned to/through the
    requested target state(s).

    :param command: command to execute
    :param target_transitions: happy path transitions
    :param attribute: attribute which is monitored
    :param timeout: custom timeout provided while execution of command's
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    :param optional_transitions: optional states that are not the target state but should not be
        counted as error states either
    :return: Command response
    """
    response = EXECUTOR.execute(command)
    for target_value in target_transitions:
        value_response = wait_for_transition(
            attribute=attribute,
            target_transition=target_value,
            optional_transitions=optional_transitions,
            timeout=timeout,
        )
        if value_response.response_msg == WAIT_FOR_TRANSITION_FAILURE_RESPONSE:
            raise ValueTransitionError(
                value_response.final_value, target_value, attribute
            )
    return response


def wait_for_value(
    attribute: Attribute,
    target_values: Iterable[Any],
    key=lambda _: _,
    timeout: float = None,
) -> Any:
    """
    Block until a Tango device attribute has reached one of target values.
    This function requires a subscription to have been established to the
    target attribute before calling this function. Subscribing is not handled
    within this function in case multiple reads of the same attribute are
    required, when we would NOT want to subscribe/unsubscribe after each event.

    If defined, the optional 'key' function will be used to process the device
    attribute value before comparison to the target value.

    :param attribute: device attribute to monitor
    :param target_values: target value to wait for
    :param timeout: custom timeout provided while execution of command's
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    :param key: function to process each attribute value before comparison
    :return: Attribute value read from device (one of target_values)
    """
    while True:
        response = EXECUTOR.read_event(attribute, timeout=timeout)
        if response.err:
            if (
                WORKAROUNDS.ignore_poll_thread_out_of_sync_error
                and response.errors[0].reason == "API_PollThreadOutOfSync"
            ):
                LOGGER.warning("API_PollingThreadOutOfSync error ignored")
            else:
                raise Exception(
                    "Encountered an error in tango.EventData:" f" {response.errors}"
                )
        else:
            processed = key(response)
            if processed in target_values:
                return processed


# TODO: return value to use Either pattern
def wait_for_transition(
    attribute: Attribute,
    target_transition: DeviceStateTypes,
    optional_transitions: Iterable[DeviceStateTypes] = None,
    timeout: float = None,
) -> WaitForTransitionResponse:
    #   timeout) -> Either[Left,Right]
    """
    Block until a Tango attribute has reached a target enumeration value. An
    error response will be returned if the value does not match the target
    value.

    This function is intended to be used to monitor enumerated device states
    such as telescopeState and ObsState, which emit a change event when the
    device transitions to a new state.

    If defined, the optional 'key' function will be used to process the device
    attribute value before comparison to the target value.

    :param attribute: attribute to monitor
    :param target_transition: target state to wait for
    :param timeout: custom timeout provided while execution of command's
    if systems do not respond within reasonable timescales then method raised EventTimeoutError.
    :return: attribute value (either target state or error state)
    :param optional_transitions: optional states that are not the target state but should not be
        counted as error states either
    """
    # two sets of logic are required: one for built-in Tango device states,
    # and one for additional non-Tango states such as SKA's ObsState. We switch
    # logic depending on the type of the target value.
    target_cls = type(target_transition)
    # Maybe the logic can be generalised to Tango and non-Tango. Not sure yet...

    # function to extract the value from an event
    state_parse_functions = {
        ObsState: parse_oet_obsstate_from_tango_eventdata,
        tango._tango.DevState: get_value_from_tango_eventdata,
    }
    key = state_parse_functions.get(target_cls, lambda _: _)

    if optional_transitions is None:
        optional_transitions = []

    # function to get every device state except the target state
    error_value_functions = {
        ObsState: lambda v: {
            e for e in ObsState if (e != v and e not in optional_transitions)
        },
        tango._tango.DevState: lambda s: {
            v
            for k, v in tango.DevState.values.items()
            if (v != s and v not in optional_transitions)
        },
    }
    error_values_fn = error_value_functions.get(target_cls, lambda _: [])
    error_values = error_values_fn(target_transition)

    LOGGER.info("Waiting for %s to transition to %s", attribute.name, target_transition)

    # wait_for_value should block until state transitions to any happy path
    # or sad path state. This list holds the union of states.
    values_union = set(error_values)
    values_union.add(target_transition)
    received = wait_for_value(attribute, values_union, key=key, timeout=timeout)
    if received != target_transition:
        LOGGER.warning(
            "%s state expected to go to %s but instead went to %s",
            attribute.name,
            target_transition,
            received,
        )
        return WaitForTransitionResponse(WAIT_FOR_TRANSITION_FAILURE_RESPONSE, received)

    LOGGER.info("%s reached target state %s", attribute.name, target_transition)
    return WaitForTransitionResponse(WAIT_FOR_TRANSITION_SUCCESS_RESPONSE, received)


def cast_tango_obsstate_to_oet_obstate(other):
    """
    Cast the given parameter into OET ObsState (tango ObsState expected as input)

    :param other: Object to be converted
    :return: OET ObsState
    """
    try:
        oet_obsstate = ObsState[other.name]
        return oet_obsstate
    except Exception as exception:
        raise TypeError(
            f"Could not convert attribute value to OET ObsState: {exception}"
        ) from exception


def parse_oet_obsstate_from_tango_eventdata(
    event: tango.EventData,
) -> ObsState:
    """
    Parse OET ObsState from the value stored in tango EventData object

    :param event: Tango EventData object
    :return: OET ObsState
    """
    try:
        oet_obsstate = ObsState(get_value_from_tango_eventdata(event))
        return oet_obsstate
    except Exception as exception:
        raise TypeError(
            f"Could not extract OET ObsState from EventData: {exception}"
        ) from exception


def get_value_from_tango_eventdata(event: tango.EventData) -> Any:
    """
    Get the value stored in tango EventData object

    :param event: Tango EventData object
    :return: EventData.attr_value.value
    """
    return event.attr_value.value


def get_cdm_interface(cdm_cls):
    """
    Get schema string for a CDM class.

    This is used by the scripting library for TMC commands where the command
    is constructed by the library itself (Scan and ReleaseResources). The
    interface versions returned by this function reflect the current interface
    the scripting library will send TMC.
    """
    if environment.is_ska_mid_environment():
        # TODO: Get these interfaces from telescope model when it supports Mid schemas
        mid_interfaces = {
            cdm_release.ReleaseResourcesRequest: telmodel_tmc_version.tmc_releaseresources_uri(
                2.1
            ),
            cdm_scan.ScanRequest: telmodel_tmc_version.tmc_scan_uri(2.1),
        }

        return mid_interfaces.get(cdm_cls, None)

    low_interfaces = {
        cdm_release.ReleaseResourcesRequest: telmodel_tmc_version.low_tmc_releaseresources_uri(
            3.0
        ),
        cdm_scan.ScanRequest: telmodel_tmc_version.low_tmc_scan_uri(3.0),
    }
    return low_interfaces.get(cdm_cls, None)


def get_request_json(request, cdm_cls, with_processing=True):
    strictness = 2

    if isinstance(request, Path):
        if with_processing:
            cdm_request = CODEC.load_from_file(cdm_cls, request, strictness=strictness)
            request_json = CODEC.dumps(cdm_request, strictness=strictness)
        else:
            with open(request, "r") as json_file:
                request_json = "".join(json_file.readlines())
    elif isinstance(request, cdm_cls):
        request_json = CODEC.dumps(request, strictness=strictness)
    elif isinstance(request, str):
        if with_processing:
            cdm_request = CODEC.loads(cdm_cls, request, strictness=strictness)
            request_json = CODEC.dumps(cdm_request, strictness=strictness)
        else:
            request_json = copy.deepcopy(request)
    else:
        raise NotImplementedError(
            f"Cannot convert request type {type(request)} to JSON"
        )
    return request_json
