import tango

from ska_oso_scripting.functions.devicecontrol.common import (
    TANGO_REGISTRY,
    _call_and_wait_for_transition,
)
from ska_oso_scripting.functions.devicecontrol.tango_executor import Command


def telescope_on():
    """
    Start up the telescope.

    This command powers up Dishes that are currently in standby.
    """
    state_attribute = "telescopeState"

    _call_and_wait_for_transition(
        get_telescope_start_up_command(),
        [tango.DevState.ON],
        state_attribute,
        optional_transitions=[tango.DevState.UNKNOWN],
    )


def get_telescope_start_up_command() -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    CentralNode.telescope_start_upOn().

    :return: the OET Command
    """
    central_node_fqdn = TANGO_REGISTRY.get_central_node()
    return Command(central_node_fqdn, "TelescopeOn")


def telescope_off(final_state: tango.DevState = tango.DevState.OFF):
    """
    Instruct telescope devices to switch to OFF mode.

    Background on final_state parameter:
    At the time of writing (PI15), CentralNode has new behaviour where it
    reports the aggregated state of the systems it controls. Hence, it is not
    yet clear which state CentralNode will reach when commanded to turn the
    telescope off, as this state could change depending on what subsystems are
    connected to TMC. For instance, if CSP reports STANDBY, CentralNode will
    report STANDBY. If CSP was not present, CentralNode might report OFF.

    :param final_state: Final Tango state to expect (default=OFF)
    """
    state_attribute = "telescopeState"
    _call_and_wait_for_transition(
        get_telescope_off_command(),
        [final_state],
        state_attribute,
        optional_transitions=[tango.DevState.UNKNOWN],
    )


def get_telescope_off_command() -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    CentralNode.TelescopeOff().

    :return: the OET Command
    """
    central_node_fqdn = TANGO_REGISTRY.get_central_node()
    return Command(central_node_fqdn, "TelescopeOff")
