import logging
import os
import time
from pathlib import Path
from typing import Union

import ska_tmc_cdm.messages.subarray_node.scan as cdm_scan
import ska_tmc_cdm.schemas as schemas
from ska_ser_skuid.client import SkuidClient
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest

import ska_oso_scripting.functions.environment as env
from ska_oso_scripting import WORKAROUNDS
from ska_oso_scripting.constant import (
    DEFAULT_TIMEOUT_FOR_CONFIGURE_CMD,
    DEFAULT_TIMEOUT_FOR_SCAN_CMD,
)
from ska_oso_scripting.event import lifecycle_topics
from ska_oso_scripting.functions.devicecontrol.common import (
    TANGO_REGISTRY,
    _call_and_wait_for_transition,
    get_cdm_interface,
    get_request_json,
    scan_duration,
)
from ska_oso_scripting.functions.devicecontrol.tango_executor import Command
from ska_oso_scripting.functions.messages import send_message
from ska_oso_scripting.types import SubarrayID

from .exception import EventTimeoutError, SkuidRuntimeError

LOGGER = logging.getLogger(__name__)


def get_configure_command(request_json: str, subarray_id: SubarrayID) -> Command:
    """
    Create a Configure Command object

    :param request_json: Configuration JSON to include in the command
    :param subarray_id: sub-array command is sent to
    :return: Configure Command
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    command = Command(subarray_node_fqdn, "Configure", request_json)
    return command


def configure(
    subarray_id: SubarrayID,
    request: Union[str, Path, ConfigureRequest],
    with_processing=True,
    timeout: float = None,
):
    """
    Configure sub-array. The function accepts the request as either a JSON string, a Path object
    pointing to a JSON file or a CDM ConfigureRequest object. Any input is validated by default
    using the Telescope Model schemas but this can be bypassed by setting the with_processing
    flag to False.

    :param subarray_id: the ID of the sub-array to control
    :param request: The payload for the configure command
    :param with_processing: Can be set to False to bypass any validation or modifications
    to the given request
    :param timeout: custom timeout value provided while execution of configure command
    Kept timeout as optional user can set that value otherwise system can set default value.
    """
    request_json = get_request_json(request, ConfigureRequest, with_processing)

    if with_processing:
        env.check_environment_for_consistency(request_json)

    command = get_configure_command(request_json, subarray_id)
    timeout = DEFAULT_TIMEOUT_FOR_CONFIGURE_CMD if timeout is None else timeout
    try:
        _call_and_wait_for_transition(
            command,
            [ObsState.CONFIGURING, ObsState.READY],
            "obsState",
            timeout=timeout,
        )
    except EventTimeoutError as evterror:
        raise EventTimeoutError(
            "Read event timeout from queue occurred while execution of "
            f"{command.command_name} command"
        ) from evterror
    send_message(lifecycle_topics.subarray.configured, subarray_id=subarray_id)


def _fetch_global_scan_id():
    if "SKUID_URL" not in os.environ:
        raise SkuidRuntimeError(
            "SKUID_URL environment variable not set, cannot fetch scan IDs. "
            "Set SKUID_URL environment variable to the URL of a running SKUID "
            "instance and try again."
        )

    skuid_url = os.environ["SKUID_URL"]
    skuid_client = SkuidClient(skuid_url)
    try:
        return skuid_client.fetch_scan_id()
    except Exception as exception:
        raise SkuidRuntimeError(
            f"Could not fetch scan ID from {skuid_url}. "
            "Ensure SKUID is running and accessible at the given URL, "
            "or redefine SKUID_URL to point to a valid SKUID instance."
        ) from exception


def get_scan_command(subarray_id: SubarrayID) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would start a
    scan.

    :param subarray_id: the sub-array to control
    :return: OET command ready to start a scan
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    scan_id = _fetch_global_scan_id()
    request = cdm_scan.ScanRequest(scan_id=scan_id)
    request.interface = get_cdm_interface(cdm_scan.ScanRequest)
    request_json = schemas.CODEC.dumps(request)
    return Command(subarray_node_fqdn, "Scan", request_json)


def scan(subarray_id: SubarrayID, timeout: float = None):
    """
    Execute a scan.

    :param subarray_id: the ID of the sub-array to control
    :param timeout: custom timeout provided while executing scan command,
    if user not provided timeout system can set timeout which provided in
    configure payload, formula becomes timeout = scan_duration + 5 extra second.
    """
    command = get_scan_command(subarray_id)

    # get scan_duration value which set in configure command.
    timeout = (
        scan_duration.get(0) + DEFAULT_TIMEOUT_FOR_SCAN_CMD
        if timeout is None
        else timeout
    )
    # transition sequence: READY -> SCANNING -> READY
    send_message(lifecycle_topics.subarray.scan.started, subarray_id=subarray_id)
    try:
        _call_and_wait_for_transition(
            command,
            [ObsState.SCANNING, ObsState.READY],
            "obsState",
            timeout=timeout,
        )
    except EventTimeoutError as evterror:
        raise EventTimeoutError(
            "Read event timeout from queue occurred while execution of "
            f"{command.command_name} command"
        ) from evterror
    send_message(lifecycle_topics.subarray.scan.finished, subarray_id=subarray_id)

    if WORKAROUNDS.pause_before_reconfigure:
        LOGGER.info("Workaround: pausing after the scan for 5 seconds for Mid.CBF")
        time.sleep(5)

    # SubArrayNode jumps to READY state while SDP is still SCANNING. This
    # usually manifests itself as a problem when releasing resources, when
    # End() is called while SDP is still processing.
    if WORKAROUNDS.SKB38:
        LOGGER.info("Adding delay after scan to work around SKB-38")
        time.sleep(1)


def end(subarray_id: SubarrayID):
    """
    Send the 'end' command to the SubArrayNode, marking the end of the
    current observation.

    :param subarray_id: the subarray to command
    """
    _call_and_wait_for_transition(
        get_end_command(subarray_id), [ObsState.IDLE], "obsState"
    )


def get_end_command(subarray_id: SubarrayID) -> Command:
    """
    Return an OET Command that, when passed to a TangoExecutor, would call
    SubArrayNode.End().

    :param subarray_id: the ID of the sub-array to control
    :return: the OET Command
    """
    subarray_node_fqdn = TANGO_REGISTRY.get_subarray_node(subarray_id)
    return Command(subarray_node_fqdn, "End")
