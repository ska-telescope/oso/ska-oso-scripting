"""
The pdm_transforms.sdp_convert module contains code to transform SDP Project Data
Model (PDM) entities to Configuration Data Model (CDM) entities.
"""
import logging
from typing import Dict, List

from ska_oso_pdm.sb_definition import ScanDefinition, Target
from ska_oso_pdm.sb_definition.sdp import (
    Beam,
    BeamMapping,
    Channels,
    ExecutionBlock,
    PbDependency,
    Polarisation,
    ProcessingBlock,
    Resources,
    ScanType,
    Script,
    SDPConfiguration,
    SpectralWindow,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    BeamConfiguration as cdm_BeamConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import Channel as cdm_Channel
from ska_tmc_cdm.messages.central_node.sdp import (
    ChannelConfiguration as cdm_ChannelConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import EBScanType as cdm_EBScanType
from ska_tmc_cdm.messages.central_node.sdp import EBScanTypeBeam as cdm_ScanTypeBeam
from ska_tmc_cdm.messages.central_node.sdp import (
    ExecutionBlockConfiguration as cdm_EBConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import PbDependency as cdm_PbDependency
from ska_tmc_cdm.messages.central_node.sdp import (
    PolarisationConfiguration as cdm_PolarisationConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ProcessingBlockConfiguration as cdm_ProcessingBlockConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ScriptConfiguration as cdm_ScriptConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    SDPConfiguration as cdm_centralnode_SDPConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.sdp import (
    SDPConfiguration as cdm_subarraynode_SDPConfiguration,
)

from ska_oso_scripting.functions.pdm_transforms.sdp_create import (
    convert_target_to_fieldconfiguration,
)

LOG = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(message)s"

logging.basicConfig(level=logging.INFO, format=FORMAT)


# Not every function in this module should be called externally
__all__ = [
    "convert_sdpconfiguration_centralnode",
    "convert_sdpconfiguration_subarraynode",
]

VIS_CHANNELS = "vis_channels"
POLARISATIONS_ID = "all"
BEAM_ID = "vis0"


def convert_sdpconfiguration_centralnode(
    pdm_config: SDPConfiguration,
    pdm_targets: List[Target],
) -> cdm_centralnode_SDPConfiguration:
    """
    Convert a PDM SDPConfiguration to the equivalent CDM SDPConfiguration.

    In a SchedulingBlockDefinition, Targets are recorded exactly once as PDM
    Targets separate and external to any SDPConfiguration. Targets to be
    inserted into the output SDPConfiguration should be passed to this
    function.

    :param pdm_config: the SDPConfiguration to convert
    :param pdm_targets: Targets to inject into output SDP configuration
    :raises TypeError: if pdm_config is not an SDPConfiguration
    """
    if not isinstance(pdm_config, SDPConfiguration):
        raise TypeError(f"Expected PDM SDPConfiguration, got {type(pdm_config)}")

    if pdm_config.execution_block:
        execution_block = convert_execution_block(
            pdm_config.execution_block, pdm_targets
        )
    else:
        execution_block = None

    if pdm_config.processing_blocks is not None:
        processing_blocks = [
            convert_processing_block(obj) for obj in pdm_config.processing_blocks
        ]
    else:
        processing_blocks = None

    if pdm_config.resources:
        resources = convert_resources(pdm_config.resources)
    else:
        resources = None

    return cdm_centralnode_SDPConfiguration(
        execution_block=execution_block,
        processing_blocks=processing_blocks,
        resources=resources,
    )


def convert_execution_block(
    pdm_config: ExecutionBlock,
    pdm_targets: List[Target],
) -> cdm_EBConfiguration:
    """
    Convert a PDM ExecutionBlock to the equivalent CDM SDP EBConfiguration.

    :param pdm_config: the ExecutionBlock to convert
    :param pdm_targets: Targets to insert into the EBConfiguration
    :raises TypeError: if pdm_config is not a ExecutionBlock
    """
    if not isinstance(pdm_config, ExecutionBlock):
        raise TypeError(f"Expected PDM ExecutionBlock, got {type(pdm_config)}")

    beams = [convert_beam(beam) for beam in pdm_config.beams]
    scan_types = [convert_scantypes(scan_type) for scan_type in pdm_config.scan_types]
    channels = [convert_channels(channels) for channels in pdm_config.channels]
    polarisations = [
        convert_polarisation(polarisation) for polarisation in pdm_config.polarisations
    ]
    fields = [convert_target_to_fieldconfiguration(target) for target in pdm_targets]

    return cdm_EBConfiguration(
        eb_id=pdm_config.eb_id,
        max_length=pdm_config.max_length,
        context=pdm_config.context,
        beams=beams,
        scan_types=scan_types,
        channels=channels,
        polarisations=polarisations,
        fields=fields,
    )


def convert_processing_block(
    pdm_config: ProcessingBlock,
) -> cdm_ProcessingBlockConfiguration:
    """
    Convert a PDM ProcessingBlock to the equivalent CDM ProcessingBlockConfiguration.
    """
    if pdm_config.dependencies:
        pdm_dependencies = [
            convert_dependencies(obj) for obj in pdm_config.dependencies
        ]
    else:
        pdm_dependencies = None
    cdm_scriptconfiguration = convert_script(pdm_config.script)
    LOG.info(f"Setting ProcessingBlock Id : {pdm_config.pb_id} ")
    return cdm_ProcessingBlockConfiguration(
        pb_id=pdm_config.pb_id,
        script=cdm_scriptconfiguration,
        parameters=pdm_config.parameters,
        dependencies=pdm_dependencies,
        sbi_ids=pdm_config.sbi_refs,
    )


def convert_script(pdm_script: Script) -> cdm_ScriptConfiguration:
    """
    Convert a PDM Script to the equivalent CDM ScriptConfiguration.
    """
    LOG.info(f"Setting ScriptConfiguration : {pdm_script.name} ")
    return cdm_ScriptConfiguration(
        kind=pdm_script.kind.value,
        name=pdm_script.name,
        version=pdm_script.version,
    )


def convert_dependencies(pdm_config: PbDependency) -> cdm_PbDependency:
    """
    Convert a PDM PbDependency to the equivalent CDM PbDependency.
    """
    LOG.info(f"Setting PbDependency Id : {pdm_config.pb_ref} ")
    return cdm_PbDependency(pb_id=pdm_config.pb_ref, kind=pdm_config.kind)


def convert_polarisation(
    pdm_instance: Polarisation,
) -> cdm_PolarisationConfiguration:
    """
    Convert a PDM Polarisation to the equivalent CDM PolarisationConfiguration.
    """
    return cdm_PolarisationConfiguration(
        polarisations_id=pdm_instance.polarisations_id,
        corr_type=pdm_instance.corr_type,
    )


def convert_resources(pdm_resources: Resources) -> Dict:
    """
    Convert a PDM Resources to equivalent Resources Dict
    """
    resources_dict = {}
    if pdm_resources.csp_links:
        resources_dict["csp_links"] = pdm_resources.csp_links
    if pdm_resources.receptors:
        resources_dict["receptors"] = pdm_resources.receptors
    if pdm_resources.receive_nodes:
        resources_dict["receive_nodes"] = pdm_resources.receive_nodes

    return resources_dict


def convert_scantypes(
    pdm_instance: ScanType,
) -> cdm_EBScanType:
    """
    Convert a PDM ScanType to the equivalent CDM EBScanType.
    """
    beams = {beam.beam_ref: convert_beam_mapping(beam) for beam in pdm_instance.beams}

    return cdm_EBScanType(
        scan_type_id=pdm_instance.scan_type_id,
        beams=beams,
        derive_from=pdm_instance.derive_from,
    )


def convert_beam_mapping(pdm_instance: BeamMapping) -> cdm_ScanTypeBeam:
    """
    Convert a PDM BeamMapping to the equivalent CDM EBScanTypeBeam.
    """
    return cdm_ScanTypeBeam(
        field_id=pdm_instance.field_ref,
        channels_id=pdm_instance.channels_ref,
        polarisations_id=pdm_instance.polarisations_ref,
    )


def convert_spectral_window(pdm_instance: SpectralWindow) -> cdm_Channel:
    """
    Convert a PDM SpectralWindow to the equivalent CDM Channel.
    """
    LOG.info(
        f"Setting channel attribute -> count:{pdm_instance.count} ,"
        f" start:{pdm_instance.start} , stride:{pdm_instance.stride},"
        f" freq_min:{pdm_instance.freq_min}, freq_max:{pdm_instance.freq_max} ,"
        f" link_map:{pdm_instance.link_map} "
    )
    return cdm_Channel(
        count=pdm_instance.count,
        start=pdm_instance.start,
        stride=pdm_instance.stride,
        freq_min=pdm_instance.freq_min,
        freq_max=pdm_instance.freq_max,
        link_map=pdm_instance.link_map,
        spectral_window_id=pdm_instance.spectral_window_id,
    )


def convert_channels(pdm_instance: Channels) -> cdm_ChannelConfiguration:
    """
    Convert a PDM Channels to the equivalent CDM cdm_ChannelConfiguration.
    """
    LOG.info(
        f"Setting ChannelConfiguration id:{pdm_instance.channels_id} ,"
        f" spectral_windows:{pdm_instance.spectral_windows} "
    )

    spectral_windows = [
        convert_spectral_window(obj) for obj in pdm_instance.spectral_windows
    ]

    return cdm_ChannelConfiguration(
        channels_id=pdm_instance.channels_id, spectral_windows=spectral_windows
    )


def convert_sdpconfiguration_subarraynode(
    scan_definition: ScanDefinition,
) -> cdm_subarraynode_SDPConfiguration:
    """
    Convert a PDM Scan Definition to an SDP Configuration aspect
    of a TMC SubArrayNode.Configure call
    """
    if not isinstance(scan_definition, ScanDefinition):
        raise TypeError(f"Expected PDM ScanDefinition, got {type(scan_definition)}")
    scan_type = scan_definition.scan_type_ref
    return cdm_subarraynode_SDPConfiguration(scan_type=scan_type)


def convert_beam(beam: Beam) -> cdm_BeamConfiguration:
    """
    Convert a PDM Beam to a CDM BeamConfiguration.

    :param beam: PDM beam to convert to CDM format
    """
    if not isinstance(beam, Beam):
        raise TypeError(f"Expected PDM Beam, got {type(beam)}")

    converted = cdm_BeamConfiguration(
        beam_id=beam.beam_id,
        function=beam.function.value,
        timing_beam_id=beam.timing_beam_id,
        vlbi_beam_id=beam.vlbi_beam_id,
        search_beam_id=beam.search_beam_id,
    )
    return converted
