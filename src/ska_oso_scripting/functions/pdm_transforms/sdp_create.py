"""
The pdm_transforms.sdp module contains code to create SDP Configuration
Data Model (CDM) entities from a Project Data Model (PDM) Scheduling Block
Definition.
"""
import logging
import os
from dataclasses import dataclass
from datetime import datetime, timezone
from typing import Dict, List, Optional, Union

import astropy.units as u
from astropy.coordinates import SkyCoord
from ska_oso_pdm import ICRSCoordinates, SBDefinition, TelescopeType
from ska_oso_pdm.sb_definition import (
    Beam,
    CSPConfiguration,
    EquatorialCoordinates,
    FivePointParameters,
    GalacticCoordinates,
    PointedMosaicParameters,
    PointingKind,
    PointingPatternParameters,
    ScanDefinition,
    SinglePointParameters,
    SolarSystemObject,
    Target,
)
from ska_oso_pdm.sb_definition.csp.lowcbf import Correlation
from ska_oso_pdm.sb_definition.csp.midcbf import CorrelationSPWConfiguration
from ska_oso_pdm.sb_definition.sdp import BeamFunction
from ska_ser_skuid.client import SkuidClient
from ska_ser_skuid.services import SKAEntityTypeService
from ska_tmc_cdm.messages.central_node.sdp import BeamConfiguration
from ska_tmc_cdm.messages.central_node.sdp import Channel as cdm_Channel
from ska_tmc_cdm.messages.central_node.sdp import (
    ChannelConfiguration as cdm_ChannelConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import EBScanType, EBScanTypeBeam
from ska_tmc_cdm.messages.central_node.sdp import (
    ExecutionBlockConfiguration as cdm_EBConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    FieldConfiguration as cdm_FieldConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import PbDependency
from ska_tmc_cdm.messages.central_node.sdp import PhaseDir as cdm_PhaseDir
from ska_tmc_cdm.messages.central_node.sdp import (
    PolarisationConfiguration as cdm_PolarisationConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ProcessingBlockConfiguration as cdm_ProcessingBlockConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    ScriptConfiguration as cdm_ScriptConfiguration,
)
from ska_tmc_cdm.messages.central_node.sdp import (
    SDPConfiguration as cdm_centralnode_SDPConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.sdp import (
    SDPConfiguration as cdm_subarraynode_SDPConfiguration,
)

from ska_oso_scripting.functions import messages
from ska_oso_scripting.functions.pdm_transforms.csp import (
    LOW_CHANNEL_WIDTH,
    MID_CHANNEL_WIDTH,
)

LOG = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(message)s"

logging.basicConfig(level=logging.INFO, format=FORMAT)


# Not every function in this module should be called externally
__all__ = [
    "create_sdpconfiguration_centralnode",
    "create_sdpconfiguration_subarraynode",
]

VIS_CHANNELS = "vis_channels"
PULSAR_CHANNELS = "pulsar_channels"
POLARISATIONS_ID = "all"
BEAM_ID = "vis0"
FIVE_POINT_SCANTYPE_PREFIX = "pointing-"
PST_SPWS_FACTOR = 1.5

mid_processing_block_parameters_dict = {
    "channels_per_port": 20,
    "dry_run": "true",
    "pod_settings": [{"networkMapping": {"ip": "1.2.3.4/0"}}],
    # temporarily removed due to SKB-690
    # "telstate": {
    #     "target_fqdn": "tango://mid-dish/dish-master/{dish_id}/programTrackTable",
    #     "source_offset_fqdn": "tango://ska_mid/tm_leaf_node/{dish_id}/sourceOffset",
    #     "direction_fqdn": "tango://mid-dish/ds-manager/{dish_id}/achievedPointing",
    # },
}

mid_processing_block_five_point_parameters_dict = {"num_scans": 5}

low_processing_block_parameters_dict = {
    "dry_run": "true",
    "pod_settings": [{"networkMapping": {"ip": "1.2.3.4/0"}}],
}


class TargetConversion:
    """
    TargetConversion is a class holding static methods related to PDM Target
    conversion.

    This class should become a separate module as/when we refactor this SDP
    module into an SDP package containing a module for each SDP entity.
    """

    SUPPORTED_PATTERNS = (
        PointingKind.SINGLE_POINT,
        PointingKind.FIVE_POINT,
        PointingKind.POINTED_MOSAIC,
    )

    @staticmethod
    def convert_target_to_fieldconfiguration(
        target: Target,
        reference_time: Optional[datetime] = None,
        pointing_fqdn: Optional[str] = None,
    ) -> cdm_FieldConfiguration:
        """
        Convert a PDM Target to the equivalent CDM FieldConfiguration.

        The only supported coordinate is a single pointing with Equatorial
        coordinate. Any other coordinate or pattern will raise a
        NotImplementedError.

        ADR-63 states that:

            a "reference_time" field (given according to ADR-37 How to
            represent time as an ISO 8601 string) should be given that
            specifies the time when the direction matches the zeroth-order
            Taylor term.

        The default reference time of now will be inserted into the output
        FieldConfiguration.

        :param target: the PDM Target to convert
        :param reference_time: reference time for CDM Emphemeris targets.
        :param pointing_fqdn: Tango FQDN of device outputting ephemeris coords
        :raises NotImplementError: if converting an unsupported coordinate system
            or pointing pattern.
        """
        LOG.info(
            f"Setting target attribute -> target_id:{target.target_id} ,"
            f" reference_coordinate:{target.reference_coordinate} ",
        )

        if reference_time is None:
            reference_time = datetime.now(timezone.utc)
        reference_time = reference_time.isoformat()

        # get phase centre transformed by the applicable pointing pattern. This gets
        # the coordinate position in the original PDM reference frame
        skycoord = TargetConversion._apply_pattern_to_coord(target)
        # convert the resulting coordinate to a reference frame supported by SDP
        in_sdp_frame = TargetConversion._convert_to_supported_sdp_frame(skycoord)
        # transform the Astropy SkyCoord to a CDM object
        phase_dir = TargetConversion._as_phase_dir(in_sdp_frame, reference_time)

        LOG.info(
            f"Resulting SDP coordinate for targetid:{target.target_id} ->"
            f" system:{phase_dir.reference_frame} , ra:{phase_dir.ra} , dec:{phase_dir.dec}"
        )

        return cdm_FieldConfiguration(
            field_id=target.target_id,
            pointing_fqdn=pointing_fqdn,
            phase_dir=phase_dir,
        )

    @staticmethod
    def convert_pst_to_fieldconfiguration(
        pst_beam: Beam,
    ) -> cdm_FieldConfiguration:
        """
        Convert a PDM Target PST to the equivalent CDM FieldConfiguration.

        The only supported coordinate is a single pointing with Equatorial
        coordinate.

        The default reference time of now will be inserted into the output
        FieldConfiguration.

        :param pst_beam: the PDM Target PST to convert
        """
        coord = pst_beam.beam_coordinate.to_sky_coord()
        reference_time = datetime.now(timezone.utc).isoformat()

        # transform the Astropy SkyCoord to a CDM object
        phase_dir = TargetConversion._as_phase_dir(coord, reference_time)

        if pst_beam.beam_name:
            field_id = pst_beam.beam_name
        else:
            field_id = pst_beam.beam_coordinate.target_id

        return cdm_FieldConfiguration(
            field_id=field_id,
            phase_dir=phase_dir,
        )

    @staticmethod
    def _apply_pattern_to_coord(target: Target) -> SkyCoord:
        """
        Get target phase centre transformed to match target pointing pattern.

        :param target: Target to process
        :raises NotImplementedError: if coordinate type or pattern is unsupported
        """

        # SDP doesn't support special yet so return obviously invalid coordinate
        # to allow AIV to use scripting to test TMC
        ref_coord = target.reference_coordinate
        match ref_coord:
            case SolarSystemObject():
                # Attempt to warn the user
                msg = r"SDP does not support non-sidereal observations at this time"
                messages.publish_event_message(msg=msg)
                LOG.warning(msg)

                return ICRSCoordinates(
                    ra_str="00:00:00.00", dec_str="-90:00:00"
                ).to_sky_coord()

            case EquatorialCoordinates() | ICRSCoordinates() | GalacticCoordinates():
                coord = ref_coord.to_sky_coord()

            # SDP doesn't support az/el coordinates yet
            case _:
                raise NotImplementedError(rf"{ref_coord} not yet supported")

        pattern_parameters = TargetConversion._pattern_parameters_for(target)

        match pattern_parameters:
            case SinglePointParameters():
                return coord.spherical_offsets_by(
                    pattern_parameters.offset_x_arcsec * u.arcsec,
                    pattern_parameters.offset_y_arcsec * u.arcsec,
                )
            case FivePointParameters():
                return coord
            case PointedMosaicParameters():
                offset = pattern_parameters.offsets[0]
                return coord.spherical_offsets_by(
                    offset.x * u.arcsec,
                    offset.y * u.arcsec,
                )
            case _:
                raise NotImplementedError(
                    f"Unhandled pointing pattern parameters for SDP: {pattern_parameters.kind}"
                )

    @staticmethod
    def _pattern_parameters_for(target: Target) -> PointingPatternParameters:
        """
        Get the active pointing pattern parameters for a target.

        :raises NotImplementedError: if the active pointing pattern is not a
            supported type.
        """
        active_pattern = target.pointing_pattern.active

        # We can only convert single pointing targets for SDP, raise an exception for anything else
        if active_pattern not in TargetConversion.SUPPORTED_PATTERNS:
            raise NotImplementedError(
                f"Unhandled pointing pattern parameters for SDP: {active_pattern}"
            )

        # PDM guarantees one pattern params per type so should be safe to create a dict
        # mapping kind to instance
        pointing_params_by_type = {
            p.kind: p for p in target.pointing_pattern.parameters
        }
        pattern_parameters = pointing_params_by_type[active_pattern]

        return pattern_parameters

    @staticmethod
    def _convert_to_supported_sdp_frame(coord: SkyCoord) -> SkyCoord:
        """
        Convert a SkyCoord to the closest supported SDP frame.

        The current algorithm is to convert all coordinates to ICRS, which is
        the closest equivalent to the ICRF3 frame supported by SDP.
        """
        return coord.transform_to("icrs")

    @staticmethod
    def _as_phase_dir(coord: SkyCoord, reference_time: str) -> cdm_PhaseDir:
        """
        Convert an AstroPy SkyCoord to an CDM SDP PhaseDir.

        Currently we just replace ICRS with ICRF3 in the output coord as
        ICRS and ICRF3 frames are virtually identical and astropy doesn't
        support ICRS -> ICRF3 conversion yet.

        :raises ValueError: if the coordinate to transform is not in a
            transformable reference frame.
        """
        match coord.frame.name:
            case "galactic":
                coord_icrs = coord.icrs
                ra = coord_icrs.ra.deg
                dec = coord_icrs.dec.deg
            case "icrs":
                ra = coord.ra.degree
                dec = coord.dec.degree
            case _:
                raise ValueError("Cannot convert non-ICRS or Galactic reference frames")

        phase_dir = cdm_PhaseDir(
            ra=[ra],
            dec=[dec],
            reference_time=reference_time,
            reference_frame="ICRF3",
        )
        return phase_dir


convert_target_to_fieldconfiguration = (
    TargetConversion.convert_target_to_fieldconfiguration
)
convert_pst_to_fieldconfiguration = TargetConversion.convert_pst_to_fieldconfiguration


def create_scan_type(scan: ScanDefinition, is_five_point: bool) -> EBScanType:
    """
    Create an EBScanType.
    :param scan: ScanDefinition used to create the EBScanType
    :param is_five_point: Set to True if target referenced by Scan Definition is a Five Point
    """
    if is_five_point:
        scan_type_id = FIVE_POINT_SCANTYPE_PREFIX + scan.scan_definition_id
    else:
        scan_type_id = scan.scan_definition_id

    beam_id = BEAM_ID

    eb_scan_type = EBScanType(
        scan_type_id=scan_type_id,
        beams={
            beam_id: EBScanTypeBeam(
                field_id=scan.target_ref,
                channels_id=VIS_CHANNELS,
                polarisations_id=POLARISATIONS_ID,
            )
        },
    )

    return eb_scan_type


def create_pst_scan_type(beam: Beam) -> Dict[str, EBScanTypeBeam]:
    beam_id = f"pst{beam.beam_id}"

    if hasattr(beam.beam_coordinate, "target_id"):
        field_id = beam.beam_coordinate.target_id
    else:
        field_id = beam.beam_name

    pst_scan_type = {
        beam_id: EBScanTypeBeam(
            field_id=field_id,
            channels_id=PULSAR_CHANNELS,
            polarisations_id=POLARISATIONS_ID,
        )
    }
    return pst_scan_type


def create_spectral_window(
    sw_id: int,
    spectral_window: Union[Correlation, CorrelationSPWConfiguration],
    spectral_window_type: BeamFunction,
    start: int = 0,
) -> cdm_Channel:
    """
    Create a SpectralWindow.

    :param sw_id: the id version of the spectral window type
    :param spectral_window: the spectral window produced by the correlator either PDM Correlation in the case of SKA MID
    :param spectral_window_type: the type of spectral window being created
    :param start: the start channel of the spectral window being created
    or PDM CorrelationSPWConfiguration in the case of SKA LOW
    """
    match spectral_window:
        case CorrelationSPWConfiguration():
            zoom_channel_width = (
                MID_CHANNEL_WIDTH.to(u.Hz).value / 2**spectral_window.zoom_factor
            )
            count = spectral_window.number_of_channels
        case Correlation():
            zoom_channel_width = LOW_CHANNEL_WIDTH.to(u.Hz).value
            count = spectral_window.number_of_channels * 144
        case _:
            raise TypeError(
                f"expected PDM Correlation or CorrelationSPWConfiguration, got {type(spectral_window)}"
            )

    freq_min = spectral_window.centre_frequency - (
        (zoom_channel_width * spectral_window.number_of_channels) / 2
    )
    freq_max = spectral_window.centre_frequency + (
        (zoom_channel_width * spectral_window.number_of_channels) / 2
    )

    match spectral_window_type:
        case BeamFunction.VISIBILITIES:
            spectral_window_id = f"vis_spw_{sw_id}"
        case BeamFunction.PULSAR_TIMING:
            spectral_window_id = "spw_pulsar"
            count = count * PST_SPWS_FACTOR
        case _:
            raise TypeError(
                f"unsupported BeamFunction type {type(spectral_window_type)}"
            )

    return cdm_Channel(
        spectral_window_id=spectral_window_id,
        count=count,
        start=start,
        freq_min=freq_min,
        freq_max=freq_max,
        stride=1,
    )


def create_spws(
    csp_config: CSPConfiguration, spw_type: BeamFunction = BeamFunction.VISIBILITIES
) -> list[cdm_Channel]:
    """
    Create a list of spectral window definitions based on the correlator configuration.

    :param csp_config: the PDM CSPConfiguration to use to create the CDM Channel
    :param spw_type: the function of the Beam spectral window to create
    """
    if csp_config.midcbf:
        correlation_spws = csp_config.midcbf.subbands[0].correlation_spws
    else:
        correlation_spws = csp_config.lowcbf.correlation_spws

    counter = 0
    spectral_windows = []
    for sw_count, obj in enumerate(correlation_spws, start=1):
        spectral_window = create_spectral_window(
            sw_id=sw_count,
            spectral_window=obj,
            spectral_window_type=spw_type,
            start=counter,
        )
        counter = counter + spectral_window.count
        spectral_windows.append(spectral_window)

    return spectral_windows


#  TODO: Code block to generate the mapping of numeric station IDs to string IDs.
#   The conversion logic is base on a Python version in a Jupyter notebook linked
#   in the comments of the ADR-62 Confluence page:
#   https://confluence.skatelescope.org/display/SWSI/ADR-62+Agree+standard+naming+and+identifications+for+LOW+Stations+and+sub-stations
#   This should be a temporary fix and the mapping should in the future be pulled
#   from OSD or tmdata or similar.


@dataclass
class ChunkSpec:
    first_id: int
    last_id: int
    first_cluster: Union[int, None]
    last_cluster: Union[int, None]
    labels: str


def label_from_id(rid):
    chunks = [
        ChunkSpec(1, 224, None, None, "C"),
        ChunkSpec(225, 296, 1, 4, "ENS"),
        ChunkSpec(297, 386, 5, 9, "ESN"),
        ChunkSpec(387, 512, 10, 16, "ESN"),
    ]

    for chunk_range in chunks:
        if chunk_range.first_id <= rid <= chunk_range.last_id:
            if not chunk_range.first_cluster:
                # This range is not in clusters. Just prepend the label to the ID
                return f"{chunk_range.labels[0]}{rid}"

            # This range is in clusters.
            clusters = chunk_range.last_cluster - chunk_range.first_cluster + 1
            offset = rid - chunk_range.first_id
            block, clst_n = divmod(offset // 6, clusters)

            return f"{chunk_range.labels[block]}{chunk_range.first_cluster+clst_n}-{offset % 6 + 1}"

    raise ValueError(f"id {rid} did not match any valid chunk specifier")


def create_resources(pdm_config: SBDefinition) -> dict:
    """
    Create resources.

    :param pdm_config: the SBDefinition to use in resource creation
    """
    if pdm_config.csp_configurations[0].midcbf:
        resources = {"receptors": sorted(pdm_config.dish_allocations.dish_ids)}
    else:
        station_ids = [
            aperture.station_id
            for beam in pdm_config.mccs_allocation.subarray_beams
            for aperture in beam.apertures
        ]
        resources = {"receptors": [label_from_id(rid) for rid in station_ids]}

    return resources


def create_processing_blocks(telescope: TelescopeType, targets: List[Target]):
    uid_client = SkuidClient(os.environ["SKUID_URL"])

    match telescope:
        case TelescopeType.SKA_MID:
            parameters = mid_processing_block_parameters_dict
        case TelescopeType.SKA_LOW:
            parameters = low_processing_block_parameters_dict
        case _:
            raise ValueError(f"Telescope {telescope.value} not supported")

    vis_receive_pb = cdm_ProcessingBlockConfiguration(
        pb_id=uid_client.fetch_skuid(
            SKAEntityTypeService.DefaultEntityTypes.ProcessingBlock
        ),
        sbi_ids=[],
        script=cdm_ScriptConfiguration(
            version="4.5.0", name="vis-receive", kind="realtime"
        ),
        parameters=parameters,
    )

    processing_blocks = [vis_receive_pb]

    if any(
        target.pointing_pattern.active == PointingKind.FIVE_POINT for target in targets
    ):
        pointing_pb = cdm_ProcessingBlockConfiguration(
            pb_id=uid_client.fetch_skuid(
                SKAEntityTypeService.DefaultEntityTypes.ProcessingBlock
            ),
            sbi_ids=[],
            script=cdm_ScriptConfiguration(
                version="0.8.0", name="pointing-offset", kind="realtime"
            ),
            parameters=mid_processing_block_five_point_parameters_dict,
            dependencies=[
                PbDependency(
                    pb_id=vis_receive_pb.pb_id, kind=[vis_receive_pb.script.name]
                )
            ],
        )
        processing_blocks.append(pointing_pb)
        processing_blocks[0].dependencies = [
            PbDependency(pb_id=pointing_pb.pb_id, kind=[pointing_pb.script.name])
        ]

    return processing_blocks


def create_sdpconfiguration_centralnode(
    pdm_config: SBDefinition,
) -> cdm_centralnode_SDPConfiguration:
    """
    Create a SDPConfiguration.

    :param pdm_config: the SBDefinition to use in creation
    :raises TypeError: if pdm_config is not an SBDefinition
    """
    if not isinstance(pdm_config, SBDefinition):
        raise TypeError(f"Expected PDM SBDefinition, got {type(pdm_config)}")

    if (eb_id := os.getenv("EB_ID")) is None:
        raise RuntimeError("environment variable EB_ID is not set")

    add_pst = (
        pdm_config.telescope == TelescopeType.SKA_LOW
        and pdm_config.targets[0].tied_array_beams.pst_beams
    )
    channel_config = create_channels(pdm_config=pdm_config, add_pst=add_pst)
    fields = create_fields(pdm_config=pdm_config, add_pst=add_pst)
    beams = create_beams(pdm_config=pdm_config, add_pst=add_pst)
    scan_types = create_scan_types(pdm_config=pdm_config, add_pst=add_pst)
    execution_block = cdm_EBConfiguration(
        eb_id=eb_id,
        max_length=1000,
        context={},
        beams=beams,
        scan_types=scan_types,
        channels=channel_config,
        polarisations=[
            cdm_PolarisationConfiguration(
                polarisations_id=POLARISATIONS_ID, corr_type=["XX", "XY", "YX", "YY"]
            )
        ],
        fields=fields,
    )
    return create_central_node_sdp_config(execution_block, pdm_config)


def create_channels(
    pdm_config: SBDefinition, add_pst: bool
) -> list[cdm_ChannelConfiguration]:
    spectral_windows = create_spws(csp_config=pdm_config.csp_configurations[0])
    channel_config = [
        cdm_ChannelConfiguration(
            channels_id=VIS_CHANNELS, spectral_windows=spectral_windows
        )
    ]

    if add_pst:
        pst_spectral_windows = create_spws(
            csp_config=pdm_config.csp_configurations[0],
            spw_type=BeamFunction.PULSAR_TIMING,
        )
        channel_config.append(
            cdm_ChannelConfiguration(
                channels_id=PULSAR_CHANNELS, spectral_windows=pst_spectral_windows
            )
        )

    return channel_config


def create_fields(
    pdm_config: SBDefinition, add_pst: bool
) -> list[cdm_FieldConfiguration]:
    fields = [
        convert_target_to_fieldconfiguration(target) for target in pdm_config.targets
    ]

    if add_pst:
        fields.extend(
            convert_pst_to_fieldconfiguration(pst)
            for target in pdm_config.targets
            for pst in target.tied_array_beams.pst_beams
        )

    return fields


def create_beams(pdm_config: SBDefinition, add_pst: bool) -> list[BeamConfiguration]:
    beams = [BeamConfiguration(beam_id=BEAM_ID, function="visibilities")]

    if add_pst:
        pst_beams = [
            BeamConfiguration(
                beam_id=f"pst{pst.beam_id}",
                function="pulsar timing",
                timing_beam_id=pst.beam_id,
            )
            for target in pdm_config.targets
            for pst in target.tied_array_beams.pst_beams
        ]

        beams.extend(pst_beams)

    return beams


def create_scan_types(pdm_config: SBDefinition, add_pst: bool) -> list[EBScanType]:
    scan_types = [
        create_scan_type(
            scan,
            next(
                (
                    target.pointing_pattern.active == PointingKind.FIVE_POINT
                    for target in pdm_config.targets
                    if scan.target_ref == target.target_id
                ),
                False,
            ),
        )
        for scan in pdm_config.scan_definitions
    ]

    if add_pst:
        pst_scan_beams = [
            create_pst_scan_type(beam=pst_beam)
            for target in pdm_config.targets
            for pst_beam in target.tied_array_beams.pst_beams
        ]

        for eb_scan_type in scan_types:
            for pst_beam in pst_scan_beams:
                eb_scan_type.beams.update(pst_beam)

    return scan_types


def create_central_node_sdp_config(
    execution_block: cdm_EBConfiguration, pdm_config: SBDefinition
) -> cdm_centralnode_SDPConfiguration:
    return cdm_centralnode_SDPConfiguration(
        execution_block=execution_block,
        resources=create_resources(pdm_config=pdm_config),
        processing_blocks=create_processing_blocks(
            pdm_config.telescope, pdm_config.targets
        ),
    )


def create_sdpconfiguration_subarraynode(
    scan_definition: ScanDefinition, target: Target
) -> cdm_subarraynode_SDPConfiguration:
    """
    Convert a PDM Scan Definition to an SDP Configuration aspect
    of a TMC SubArrayNode.Configure call if no PDM SDPConfiguration
    is present
    """
    if not isinstance(scan_definition, ScanDefinition):
        raise TypeError(f"Expected PDM ScanDefinition, got {type(scan_definition)}")
    if target.pointing_pattern.active == PointingKind.FIVE_POINT:
        scan_type = FIVE_POINT_SCANTYPE_PREFIX + scan_definition.scan_definition_id
    else:
        scan_type = scan_definition.scan_definition_id
    return cdm_subarraynode_SDPConfiguration(scan_type=scan_type)
