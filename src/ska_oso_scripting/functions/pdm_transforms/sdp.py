# SDP creation and conversion functions have been separated to
# different modules for better readability. SDP conversion functions
# should be deprecated in the future but for now import both here
# for backwards compatibility
from ska_oso_scripting.functions.pdm_transforms.sdp_convert import (
    convert_sdpconfiguration_centralnode,
    convert_sdpconfiguration_subarraynode,
)
from ska_oso_scripting.functions.pdm_transforms.sdp_create import (
    create_sdpconfiguration_centralnode,
    create_sdpconfiguration_subarraynode,
)
