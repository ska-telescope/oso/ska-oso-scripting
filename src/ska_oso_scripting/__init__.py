"""
ska_oso_scripting module initialiser
"""
from pubsub import pub

# These functions are imported so that a scripting user can import them from the scripting namespace rather than ska-db-oda.
# This means the user doesn't have to know about the ODA dependency and protects against future breaking changes.
from ska_db_oda.client import oda_helper
from ska_ser_logging import configure_logging

from ska_oso_scripting.event import lifecycle_topics, user_topics
from ska_oso_scripting.functions.environment import is_ska_mid_environment

pub.addTopicDefnProvider(lifecycle_topics, pub.TOPIC_TREE_FROM_CLASS)
pub.addTopicDefnProvider(user_topics, pub.TOPIC_TREE_FROM_CLASS)

# Configure the logger to use SKA format
configure_logging()


# equivalent of feature flags for hacks required during integration testing
class WorkaroundState:
    # calling End() too soon after scan causes problems
    SKB38 = False

    # SKB-720 Mid.CBF is not ready to be configured until 5s pause after previous scan
    pause_before_reconfigure = is_ska_mid_environment()

    # Mid.CBF fails if these are sent, even as none values. Not supported until AA2
    exclude_frequency_band_offsets = True

    # calling ReleaseResources too soon after End() causes problems
    pause_before_releaseresources = False

    # obsState=IDLE doesn't mean assigned_resources is ready to read, so
    # insert a time delay between allocation and reading assigned_resources
    delay_before_reading_assigned_resources = 1

    # Subscribing to telescopeState sometimes throws APIPollThreadOurOfSync errors.
    # It seems to be possible to ignore these errors and keep reading events.
    ignore_poll_thread_out_of_sync_error = True

    # During execution of a real SB, the reconfigure seems to take a longer time than expected for Mid.
    extend_configure_timeout = is_ska_mid_environment()

    # TMC does not yet (in ska-tmc-mid chart version 0.24.0) support pointing groups as per TMC Configure 4.1 schema.
    # When this flag is True, send an empty list for pointing.groups for TMC configure payload.
    disable_pointing_groups = True


WORKAROUNDS = WorkaroundState()
