class subarray:
    """
    Root topic for events emitted relating to individual Subarray activites
    """

    class resources:
        """
        Topic for events relating to Subarray resources
        """

        class allocated:
            """
            Emitted when resources have been allocated to a subarray
            """

            def msgDataSpec(msg_src, subarray_id):
                """
                - msg_src: component from which the request originated
                - sb_id: Subarray ID
                """

        class deallocated:
            """
            Emitted when resources have been deallocated from a subarray
            """

            def msgDataSpec(msg_src, subarray_id):
                """
                - msg_src: component from which the request originated
                - sb_id: Subarray ID
                """

    class configured:
        """
        Emitted when subarray has been configured
        """

        def msgDataSpec(msg_src, subarray_id):
            """
            - msg_src: component from which the request originated
            - sb_id: Subarray ID
            """

    class scan:
        """
        Topic for events emitted when a scan is run on subarray
        """

        class started:
            """
            Emitted when a scan is started
            """

            def msgDataSpec(msg_src, subarray_id):
                """
                - msg_src: component from which the request originated
                - sb_id: Subarray ID
                """

        class finished:
            """
            Emitted when a scan is finished
            """

            def msgDataSpec(msg_src, subarray_id):
                """
                - msg_src: component from which the request originated
                - sb_id: Subarray ID
                """

    class fault:
        """
        Topic for events emitted when subarray cannot be reached
        """

        def msgDataSpec(msg_src, subarray_id, error):
            """
            - msg_src: component from which the request originated
            - sb_id: Subarray ID
            - error: Error response received from Subarray
            """
