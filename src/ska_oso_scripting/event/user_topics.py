class script:
    """
    UNDOCUMENTED: created as parent without specification
    """

    class announce:
        """
        UNDOCUMENTED: created without spec
        """

        def msgDataSpec(msg_src, msg):
            """
            - msg_src: component from which the request originated
            - msg: user message
            """
