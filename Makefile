# Project makefile for a ska-oso-scripting project.
CAR_OCI_REGISTRY_HOST ?= artefact.skao.int
CAR_OCI_REGISTRY_USERNAME ?= ska-telescope
PROJECT_NAME = ska-oso-scripting
K8S_CHART = ska-oso-scripting-testing

# Set sphinx documentation build to fail on warnings (as it is configured
# in .readthedocs.yaml as well)
DOCS_SPHINXOPTS ?= -W --keep-going

# include makefile to pick up the standard Make targets from the submodule
-include .make/base.mk
-include .make/python.mk
-include .make/oci.mk
-include .make/k8s.mk
-include .make/helm.mk
-include .make/xray.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(strip $(OCI_IMAGE)):$(VERSION)


# Set python-test make target to only run unit tests
PYTHON_TEST_FILE = tests/unit/ tests/scripts/


# unset defaults so settings in pyproject.toml take effect
PYTHON_SWITCHES_FOR_BLACK =
PYTHON_SWITCHES_FOR_ISORT =

# Restore Black's preferred line length which otherwise would be overridden by
# System Team makefiles' 79 character default
PYTHON_LINE_LENGTH = 88

PYTHON_SWITCHES_FOR_PYLINT=--disable=C,R,W,E,I1101
# PYTHON_SWITCHES_FOR_PYLINT=--fail-under=7

PYTHON_VARS_BEFORE_INTEGRATION_PYTEST += ODA_URL=$(ODA_URL)
PYTHON_VARS_BEFORE_INTEGRATION_PYTEST += SKUID_URL=$(SKUID_URL)

PYTHON_VARS_AFTER_INTEGRATION_PYTEST = --cucumberjson=build/cucumber.json \
	--json-report --json-report-file=build/report.json

CLUSTER_DOMAIN = "techops.internal.skao.int"
# Set cluster_domain to minikube default (cluster.local) in local development
# (CI_ENVIRONMENT_SLUG should only be defined when running on the CI/CD pipeline)
ifeq ($(CI_ENVIRONMENT_SLUG),)
CLUSTER_DOMAIN = "cluster.local"
K8S_CHART_PARAMS += --set global.cluster_domain="cluster.local"
endif

ODA_API_VERSION ?= $(shell helm dependency list ./charts/ska-oso-scripting-testing/ | grep ska-db-oda | gawk -F'[[:space:]]+|[.]' '{print $$2}')
# The default ODA_URL points to the umbrella chart ODA deployment where data is
# lost on chart teardown. For longer-term data persistence, override ODA_URL to
# point to the persistent ODA deployment.
ODA_URL ?= http://ska-db-oda-rest-$(HELM_RELEASE):5000/$(KUBE_NAMESPACE)/oda/api/v$(ODA_API_VERSION)

SKUID_URL ?= http://ska-ser-skuid-$(HELM_RELEASE)-svc.$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):9870

MINIKUBE_IP = $(shell minikube ip)
HOSTNAME = $(shell hostname)
MOUNT_ODA ?= false

# override k8s-test so that:
# - pytest --forked is run, working around Tango segfault issue with standard pytest
# - only integration tests run and unit tests are ignored
# - adds 'rP' to print captured output for successful tests
K8S_TEST_TEST_COMMAND = $(PYTHON_VARS_BEFORE_INTEGRATION_PYTEST) $(PYTHON_RUNNER) \
						pytest --forked -rP \
						$(PYTHON_VARS_AFTER_INTEGRATION_PYTEST) ./tests/integration \
						 | tee pytest.stdout ## k8s-test test command to run in container

# To view the ODA as a local directory, set the ODA charts to use a
# PersistentVolume mounted from the working directory
ifeq ($(MOUNT_ODA),true)
K8S_CHART_PARAMS += --set ska-db-oda-umbrella.ska-db-oda.rest.backend.filesystem.use_pv=true \
	--set ska-db-oda-umbrella.ska-db-oda.rest.backend.filesystem.pv_hostpath=$(PWD)/oda
endif

oda_dir = $(PWD)/oda
$(oda_dir):
	@if [ $(MOUNT_ODA) = true ]; then \
		echo "ODA backing directory $(PWD))/oda does not exist. Creating it now..."; \
		echo ; \
		mkdir $(PWD)/oda; \
	fi

diagrams:  ## recreate PlantUML diagrams whose source has been modified
	@for i in $$(git diff --name-only -- '*.puml'); \
	do \
		echo "Recreating $${i%%.*}.svg"; \
		cat $$i | docker run --rm -i think/plantuml -tsvg - > $${i%%.*}.svg; \
		done

# Developer/demo environment make target to
# - launch ODA with filesystem backend
# - launch shell inside devpod
#
# Prerequisites:
# - Transparent NFS sharing between host and Kubernetes MUST be set up!
# - The project directory is mounted at /app so changes to the source code should have immediate effect
# - The ODA backing directory is ./oda when MOUNT_ODA is set to true
devpod: K8S_CHART_PARAMS += --set ska-oso-devpod.enabled=true \
	--set ska-oso-devpod.env.oda_url=$(ODA_URL) \
	--set ska-oso-devpod.env.skuid_url=$(SKUID_URL) \
	--set ska-oso-devpod.hostPath=$(PWD) \
	--set ska-oso-devpod.image.tag=$(VERSION)

devpod: k8s-install-chart k8s-wait | $(oda_dir)  ## bring up full OSO integration environment
	@echo "==================================================================="
	@echo "           OSO developer/integration environment setup"
	@echo "==================================================================="
	@echo
	@echo "Addresses to connect to from host:"
	@echo
	@echo "    * ODA Swagger UI: $(subst ska-db-oda-rest-$(HELM_RELEASE):5000,$(MINIKUBE_IP),$(ODA_URL))/ui"
	@echo "    * ODA REST API: $(subst ska-db-oda-rest-$(HELM_RELEASE):5000,$(MINIKUBE_IP),$(ODA_URL))"
	@echo
	@kubectl -n $(PROJECT_NAME) wait --for=condition=ready pod devpod
	@echo "Now launching a bash terminal inside devpod..."
	@echo
	@kubectl -n $(PROJECT_NAME) exec --stdin --tty devpod -- /bin/bash
