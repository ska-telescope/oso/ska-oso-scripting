"""
Example script for abort sub-array activity
"""
import functools
import logging
import os

from ska_oso_scripting.functions.devicecontrol import abort

LOG = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(message)s"

logging.basicConfig(level=logging.INFO, format=FORMAT)


def init(subarray_id: int):
    global main  # pylint: disable=global-statement
    main = functools.partial(_main, subarray_id)
    LOG.info("Script bound to sub-array %s", subarray_id)


def _main(subarray_id: int, *args, **kwargs):
    """
    Send the 'abort' command to the SubArrayNode, halt the subarray
    activity.

    :param subarray_id: numeric subarray ID
    :return:
    """
    LOG.info("Running abort script in OS process %s", os.getpid())
    LOG.info(f"Called with main(subarray_id={subarray_id})")

    abort(subarray_id)

    LOG.info("Abort script complete")
