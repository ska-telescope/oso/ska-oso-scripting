"""
Example script for running an SB-driven observation. Last updated
07/08/24. ConfigureRequest is created from the SB in its entirety.
5PointScan observations are supported.
"""
import functools
import logging
import os

from ska_oso_pdm import SBDefinition

from ska_oso_scripting import oda_helper
from ska_oso_scripting.functions import (
    devicecontrol,
    messages,
    pdm_transforms,
    sb,
)
from ska_oso_scripting.event import user_topics
from ska_oso_scripting.functions.devicecontrol import release_all_resources
from ska_oso_scripting.functions.devicecontrol.common import ValueTransitionError
from ska_oso_scripting.objects import SubArray

LOG = logging.getLogger(__name__)
FORMAT = "%(asctime)-15s %(message)s"

logging.basicConfig(level=logging.INFO, format=FORMAT)


def init(subarray_id: int):
    """
    Initialise the script, binding the sub-array ID to the script.
    """
    LOG.debug(f"Initializing script {__name__} with subarray_id={subarray_id}")
    global main
    main = functools.partial(_main, subarray_id)
    LOG.info(f"Script bound to sub-array {subarray_id}")


def assign_resources(subarray: SubArray, sbi: SBDefinition):
    """
    assign resources to a target sub-array using a Scheduling Block (SB).
    :param subarray: subarray ID
    :param sbi: ska_oso_pdm.SBDefinition
    :return:
    """
    LOG.info(
        f"Running assign_resources(subarray={subarray.id} sbi.sbd_id={sbi.sbd_id})"
    )

    cdm_allocation = pdm_transforms.create_cdm_assign_resources_request_from_scheduling_block(
            subarray.id, sbi
        )

    response = devicecontrol.assign_resources_from_cdm(subarray.id, cdm_allocation)
    LOG.info(f"Resources Allocated: {response}")

    LOG.info("Allocation complete")


def observe(subarray: SubArray, sbi: SBDefinition):
    """
    Observe using a Scheduling Block (SB) and template CDM file.

    :param subarray:  SubArray instance containing subarray ID
    :param sbi: Instance of a SBDefinition
    :return:
    """

    LOG.info(
        f"Starting observing for Scheduling Block: {sbi.sbd_id}, subarray_id={subarray.id})"
    )

    if not sbi.scan_sequence:
        LOG.info(f"No scans defined in Scheduling Block {sbi.sbd_id}. No observation performed.")
        return
    cdm_configure_requests = (
        pdm_transforms.create_cdm_configure_request_from_scheduling_block(sbi)
    )

    for scan_definition_id in sbi.scan_sequence:
        cdm_configs = cdm_configure_requests[scan_definition_id]
        for index, cdm_config in enumerate(cdm_configs):
            scan_id_string = (f"{scan_definition_id} "
                              f"({str(index + 1)}/{str(len(cdm_configs))})" if len(cdm_configs) > 1 else '')
            try:
                # With the CDM modified, we can now issue the Configure instruction...
                LOG.info(f"Configuring subarray {subarray.id} for scan: {scan_id_string}")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Configuring subarray {subarray.id} for scan: {scan_id_string}"
                )
                devicecontrol.configure_from_cdm(subarray.id, cdm_config)
            except ValueTransitionError as err:
                LOG.error(f"Error configuring subarray: {err}")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Error configuring subarray for scan {scan_id_string}"
                )
                raise err
            else:
                LOG.info(f"Configuration for scan {scan_id_string} complete")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Configuration for scan {scan_id_string} complete"
                )
            try:
                # with configuration complete, we can begin the scan.
                LOG.info(f"Starting scan: {scan_id_string}")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Starting scan: {scan_id_string}"
                )
                devicecontrol.scan(subarray.id)
            except ValueTransitionError as err:
                LOG.error(f"Error when executing scan: {scan_id_string}: {err}")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Error when executing scan: {scan_id_string}"
                )
                raise err
            else:
                LOG.info(f"Scan {scan_id_string} complete")
                messages.send_message(
                    user_topics.script.announce,
                    msg=f"Scan {scan_id_string} complete"
                )

    # All scans are complete. Observations are concluded with an 'end'
    # command.
    LOG.info(f"End scheduling block: {sbi.sbd_id}")
    devicecontrol.end(subarray.id)

    LOG.info("Observation script complete")


def _main(subarray_id: int, sb_json: str, sbi_id: str):
    LOG.info(f"Running OS process {os.getpid()}")
    LOG.info(f"Called with main(subarray_id={subarray_id}, sbi_id={sbi_id})")
    LOG.debug(f"main() sb_json={sb_json}")
    sbd: SBDefinition = sb.load_sbd(sb_json)
    eb_id = oda_helper.create_eb(sbd.telescope, sbi_ref=sbi_id)
    LOG.info(f"Created Execution Block {eb_id}")
    subarray = SubArray(subarray_id)
    assign_resources(subarray, sbd)
    observe(subarray, sbd)
    release_all_resources(subarray_id)
