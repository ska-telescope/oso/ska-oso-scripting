.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
   :maxdepth: 2
   :caption: Releases
   :hidden:

   CHANGELOG.rst

.. toctree::
   :maxdepth: 1
   :caption: Developer guide

   developer_guide/integration_tests.md

.. toctree::
   :maxdepth: 1
   :caption: Home
   :hidden:

   observing_scripts
   writing_scripts_for_the_oet
   writing_control_scripts_without_sbs
   execution_blocks
   environment_variables

.. COMMUNITY SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 2
  :caption: Public API
  :hidden:

  package/objects
  package/functions/devicecontrol
  package/functions/environment
  package/functions/pdm_transforms
  package/functions/messages
  package/functions/sb


.. toctree::
  :maxdepth: 2
  :caption: Private API
  :hidden:

  package/functions/tango

=================
ska-oso-scripting
=================

Overview
========

The ska-oso-scripting project provides a Python library intended to be useful for
engineers and scientists writing :ref:`observing scripts <observing-scripts>` and
engineering tests. The helper functions and classes contained in the library support
the high-level configuration and control of an SKA subarray, hiding the low-level
details of how the related Tango devices are controlled from the script author.

This library provides a simple object-oriented interface and a functional interface,
located in :mod:`ska_oso_scripting.objects` and :mod:`ska_oso_scripting.functions`
respectively. The objects are recommended as the most user-friendly option but the
functions can also be called if preferred. Regardless, the same code is called at the
lowest level: object methods call 'public' scripting functions, which then call
'private' lower-level scripting functions held in submodules of
:mod:`~ska_oso_scripting.functions.devicecontrol`.

Observing scripts can be run interactively in a Jupyter notebook, or remotely executed
by the Observation Execution Tool (OET). For documentation on how to use OET to run
observing scripts, see the
:doc:`ska-oso-oet project documentation <ska-oso-oet:index>`.

A major use case for the ska-oso-scripting library is to support the execution of
Scheduling Blocks (SBs). Hence, in addition to Tango device control, much of the
ska-oso-scripting library is concerned with translating SBs into the equivalent JSON
configuration and control strings and with the issuing of commands to TMC Tango
devices at the appropriate times.

Quickstart
==========
Like all SKA projects, this project uses containers for development and testing so that
the build environment, test environment and test results are reproducible and
independent of the host environment. ``make`` is used to provide a consistent UI.

Build a new container image for the OET with:

::

  make oci-build

Execute the test suite with:

::

  make python-test

Format and lint the Python code with:

::

  make python-format
  make python-lint
