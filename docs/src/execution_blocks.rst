.. _execution-blocks:

*****************
Execution Blocks
*****************

----------------------------
What is an Execution Block?
----------------------------

Execution Blocks (EBs) are a record of the requests to and responses from the telescope during an observing session,
and provide a way to link the data to this observing session. For more information and a sample of an EB,
see the `documentation for the PDM project <https://developer.skao.int/projects/ska-oso-pdm/en/latest/execution_blocks.html>`_,
which is where the EB is defined.

EBs are stored in the OSO Data Archive (ODA), and the ODA application provides custom API resources for
creating and updating EBs. The ODA also provides a client with functions to call this create API and a decorator
which will send the decorated function request and response to the endpoint to update the EB.
See the `documentation for the ODA project <https://developer.skao.int/projects/ska-db-oda/en/latest/client/odahelper.html>`_ for more details.

The client is implemented in a way that minimises the visibility of the EB lifecycle, however the script author or
notebook user should be aware of the purpose of an EB and should follow the instructions below so ensure the session is
properly captured in an EB.

-----------------------------------------------------
Capturing an observing session in an Execution Block
-----------------------------------------------------

An observing session typically means either the Scheduling Block driven execution of observing scripts defined in this project or elsewhere,
or a Jupyter notebook session where a user is interacting with the telescope, either through the functions in this project or at a lower level.

In either case, the same process for capturing data in an Execution Block should be followed:

1. Set the ODA_URL environment variable to the location of a running instance of the ODA, eg ODA_URL=https://k8s.stfc.skao.int/button-dev-ska-db-oda/oda/api/v2/
2. At the start of the session or script, call the create_eb from the ODA EB client discussed above

.. code-block:: python

    from ska-oso-scripting import oda_helper

    oda_helper.create_eb()


3. Functions that are decorated with ``@oda_helper.capture_request_response`` will send the request_responses to the ODA with the relevant eb_id.
   The public functions in `ska_oso_scripting.functions.devicecontrol <https://developer.skao.int/projects/ska-oso-scripting/en/latest/package/functions/devicecontrol.html#module-ska_oso_scripting.functions.devicecontrol>`_
   are already decorated. To capture custom function calls, the decorator can either imported and added to the function definition, or functions calls can be decorated on the fly during the execution in the session:

.. code-block:: python

    from ska-oso-scripting import oda_helper

    # Decorate the function in the source code
    @oda_helper.capture_request_response
    def my_function_to_record(args):
        ...

    # OR 'decorate' the function at runtime when calling
    my_response = oda_helper.capture_request_response(my_function_to_record)(args)
