.. _writing-control-scripts-without-sbs:

*********************************
Controlling subarrays without SBs
*********************************

At the highest level, SKA subarrays are configured and controlled by a handful of
commands, some of which require JSON control strings. Scripted control of subarrays
can be achieved by creating these JSON control strings - either directly or by using
the ska-cdm-library to create the Python equivalent of the JSON control strings - and
then sending these to the subarray via :class:`~ska_oso_scripting.objects.SubArray`
methods or the appropriate ska-oso-scripting functions.

Consult the API documentation for :class:`ska_oso_scripting.objects.SubArray` for
details on the methods used to assign resources to a subarray, configure a subarray,
perform a scan, etc.

See :class:`ska_oso_scripting.objects.Telescope` for methods used to turn the telescope
on and off.

See :mod:`ska_oso_scripting.functions.devicecontrol` for functions you can use for
high-level telescope and subarray control.

------------------------------
Tweaking configuration strings
------------------------------

Resource allocation and configuration require lengthy JSON strings as input. Modifying
these JSON payloads is most easily achieved by using classes from the :doc:`ska-tmc-cdm <ska-tmc-cdm:index>`
project. Use of the Control Data Model (CDM) objects allows JSON payloads to be
modified via Python rather than by modifying JSON strings directly and reading/writing
those changes to a file.

In the absence of an SB, the recommended way to control the telescope is to:

#. use the ska-oso-cdm library to convert JSON contained in a file or string to the
   equivalent Python objects
#. perform any required modifications to the CDM objects
#. relay the instructions to the control system using the appropriate methods
   (:meth:`~ska_oso_scripting.objects.SubArray.assign_from_cdm`,
   :meth:`~ska_oso_scripting.objects.SubArray.configure_from_cdm`, etc.)

The script below illustrates how this can be done.

.. code-block:: python
   :linenos:

    from datetime import timedelta
    from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
    from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest
    from ska_tmc_cdm.schemas import CODEC
    from ska_oso_scripting.objects import SubArray, Telescope

    # Create telescope object to control telescope start-up and shut-down
    telescope = Telescope()
    # Create sub-array object which will form the target for subsequent instructions
    subarray = SubArray(1)

    # Turn the telescope on
    telescope.on()

    # Create a CDM AssignResourcesRequest object. This example loads JSON
    # from file but a request can also be formed from a JSON string using
    # CODEC.loads(AssignResourcesRequest, allocation_json_string)
    request = CODEC.load_from_file(AssignResourcesRequest, file_path, timeout=None)
    # Modify request object here if necessary, e.g.
    request.dish.receptor_ids = ["SKA001", "SKA002", "SKA003", "SKA004"]
    # issue resource allocation request
    subarray.assign_from_cdm(request, timeout=None)

    # Similarly, create a CDM ConfigureRequest object. Again, this could
    # also be formed from a JSON string using
    # CODEC.loads(ConfigureRequest, configuration_json_string)
    request = CODEC.load_from_file(ConfigureRequest, file_path)
    # Modify request object here if necessary, e.g.
    request.tmc.scan_duration = timedelta(seconds=10.0)
    # issue sub-array configuration request
    subarray.configure_from_cdm(request)

    # Execute scan
    subarray.scan(timeout=None)

    # End the Scheduling Block
    subarray.end()

    # release all sub-array resources
    subarray.release(timeout=None)

    # Set telescope to standby
    telescope.off()


-------------------------
Control using static JSON
-------------------------

For an interaction where no modifications to the CDM are required, you can also use
the :meth:`~ska_oso_scripting.objects.SubArray.assign_from_file` and
:meth:`~ska_oso_scripting.objects.SubArray.configure_from_file` methods, which will
relay the JSON directly to the control system. The JSON will be validated against the
required JSON schema and any elements that are required to be unique from observation
to observation, such as scheduling block ID and processing block ID, will managed as
necessary.

.. note::
    You can also send the raw JSON directly to the control system without performing
    any validation or ID updates by setting ``with_processing=False`` for these methods.
    However, it is then your responsibility to ensure that the CDM payloads are valid!

.. code-block:: python

    from ska_oso_scripting.objects import SubArray, Telescope

    # Create telescope object to control telescope start-up and shut-down
    telescope = Telescope()

    # Turn the telescope on
    telescope.on()

    # Create domain object for the sub-array the commands will be sent to
    subarray = SubArray(1)

    # Allocate resources, provide a path to a file with allocation JSON
    subarray.assign_from_file(path_to_allocation_json_file, timeout=None)

    # Configure sub-array, provide a path to a file with configuration JSON
    subarray.configure_from_file(configuration_json_file, scan_duration=10.0, timeout=None)

    # Execute scan sub-array was configured for
    subarray.scan()

    # End the Scheduling Block
    subarray.end()

    # Set telescope to standby
    telescope.off()
