.. _observing-scripts:

*****************
Observing Scripts
*****************

----------------------------
What is an Observing Script?
----------------------------
An observing script is some Python code whose purpose is to perform the observation
defined in a Scheduling Block (SB). In practice, there may be several observing scripts
that process different parts of the SB; for example, a script to allocate resources, a
script to perform the observation; a script to deallocate resources, etc.

The :doc:`SKA Project Data Model <ska-oso-pdm:index>` (PDM) defines the structure of
Scheduling Blocks (SBs), and how an SB can be serialised as a JSON file. The syntax of
the commands and configurations required by the telescope are described in the
:doc:`SKA Control Data Model <ska-tmc-cdm:index>` (CDM). A major function of an
observing script is to convert the SB PDM entity to the appropriate set of CDM
allocation and configuration entities, which form the arguments for telescope control
commands.

To achieve its aims the script must send a series of commands in the correct order.
The typical sequence for 'executing' an SB from start to finish is:

1. Command the CentralNode to allocate the required resources and instantiate
   the SubArrayNode. The observing script parses the SB JSON, reading what
   resources are required and constructing the equivalent CDM JSON which it then
   issues to the control system.
2. Using the SubArrayNode, loop through the scans required by the SB:

   a. Command the SubArrayNode to configure for the scan. The script reads the
      subarray configurations, calculates the CDM JSON required for the scan
      configuration, and configures the subarray accordingly.
   b. Command the SubArrayNode to scan (that is, take data).

3. Tell the SubArrayNode and CentralNode that the SB is complete.
4. Command the CentralNode to release the resources in the SubArrayNode.


-------
Context
-------

The relationship between SBs, observing scripts, and the devices that the scripts
control is shown in the figure below.

.. figure:: diagrams/context.svg

   Observing scripts in the context of SBs, the OET, and SKA Tango devices.

Scheduling Blocks (SBs) are the atomic units of SKA observations. Each SB defines the
required resources, configurations, scan sequences, timing constraints, and data
processing that is required for that observation. Each SB also references an
*observing script* - a set of Python instructions that will process the SB, translating
the SB to the series of lower-level commands and JSON configuration strings that will
control the SKA telescopes and take the required data.

During standard operations, the SKA will have an observing queue with a pool of
pending SBs awaiting execution. SBs will be selected by the SKA Scheduler and sent to
the Observation Execution Tool (OET) for execution. The OET is responsible for loading
and executing the scripts, retrieving the script referenced by an SB, installing any
dependencies into a Python environment and then executing the script.

The telescope appears to the script author as two Tango devices: the
CentralNode is the target for commands to allocate resources to form a 
SubArrayNode, and release them again when the SubArrayNode is no longer
needed after the observation; the SubArrayNode is the target for commands 
to configure and scan to take data during the observation.


