# ska-oso-scripting
This project contains code for ska-oso-scripting, the library which provides modules 
for developing observing scripts for SKA along with some example observing scripts.

This project is structured to use Docker containers for development and
testing so that the build environment, test environment and test results are
all completely reproducible and are independent of host environment. It uses
``make`` to provide a consistent UI (full details can be found in the ```docs```).

First, clone this repository with:

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/oso/ska-oso-scripting.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

Install all dependencies using Poetry:
First go to the poetry shell, It opens the poetry virtual environment and then run poetry install command

```
> poetry shell

> poetry install
```

To update the poetry.lock file, use command:

```
> poetry update
```

Execute the unit tests, code format, and lint the project with:

```
poetry run make python-test 
poetry run make python-format
poetry run make python-lint
```

To release a new version, see the SKAO developer portal instructions [here](https://developer.skao.int/en/latest/tutorial/release-management/automate-release-process.html#how-to-make-a-release) 


## Running integration tests locally

### With k8s-test

To start the test environment on Kubernetes run
```
`make k8s-install-chart && make k8s-wait
```

To run the integration tests using the `k8s-test` target run
```
make k8s-test
```

### With devpod

> ``📝`` **Note**
> 
> NFS file sharing from host to Kubernetes MUST be set up for the following to work!
> Follow the steps in [OSO Developer Setup](https://confluence.skatelescope.org/x/6X9EEQ) set up your machine.

For more finegrained integration test execution the tests can be run from inside a `make devpod` session. 
As a first step, run `make oci-build` to build the devpod image with latest developer dependencies.

`make devpod` launches a new pod inside the ska-oso-scripting namespace and NFS mounts 
the project workspace so your filesystem is synchronised between pod and host. Once
inside the devpod, unit tests and integration test can be run by executing 
`poetry run make test` and `poetry run pytest --forked tests/integration`, respectively.

If wanting direct view of the content of the ODA during the test execution, setting `MOUNT_ODA` environment
variable to `true` (`make MOUNT_ODA=true devpod`) will create `./oda` directory (if necessary) and will act 
as the backing  store for the ODA, containing all OSO entities persisted by the ODA.

The example session below shows unit tests and integration tests run inside a `make devpod`. 

```
bob@workpc:~/ska/src/ska-oso-scripting$ make devpod
... Helm output omitted ...
===================================================================
           OSO developer/integration environment setup
===================================================================

Addresses to connect to from host:

    * ODA Swagger UI: http://192.168.49.2/ska-oso-scripting/api/v5/ui
    * ODA REST API: http://192.168.49.2/ska-oso-scripting/api/v5/

pod/devpod condition met
Now launching a bash terminal inside devpod...

# example: run the unit tests
tango@devpod:/app$ cd /app
tango@devpod:/app$ poetry run make python-test

# run the integration tests
tango@devpod:/app$ cd /app
tango@devpod:/app$ poetry run pytest --forked tests/integration
```

### With pytest

Integration tests can also be run using `pytest` within the Poetry environment (using `--forked` flag as mentioned above). 

Additionally to run tests with `pytest`, the `ODA_URL` variable has to be set. This is can be done in`pyproject.toml` 
`tool.pytest.ini_options` section and the commented out example points to the ODA deployment in minikube. If running 
against a different ODA deployment, change the variable point to the desired ODA URL.


## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-oso-scripting/badge/?version=latest)](https://developer.skao.int/projects/ska-oso-scripting/en/latest/?badge=latest)

Documentation can be found in the ``docs`` folder. To build docs, install the 
documentation specific requirements:

```
poetry install --with=docs
```

and build the documentation (will be built in docs/build folder) with 

```
poetry run make docs-build html
```