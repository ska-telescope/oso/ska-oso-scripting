import os
from functools import partial
from pathlib import Path
from unittest.mock import DEFAULT, MagicMock, patch

import pytest
from ska_oso_pdm import SBDefinition
from ska_ser_skuid.client import SkuidClient

# TODO: There's now only one allocate_and_observe script,
# delete this whole test file at some point in the near future.
# ~bjmc Aug 2024
from scripts import allocate_and_observe_sb as mid_script
from ska_oso_scripting.functions import pdm_transforms
from ska_oso_scripting.objects import SubArray

PATH_TO_TEST = Path(__file__).parents[1]
PATH_TO_MID_SB = PATH_TO_TEST.joinpath(
    "resources", "pdm_transforms_test_cases", "mid_5point_no_sdp", "sb_definition.json"
)


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with patch.dict(
        os.environ,
        {
            "EB_ID": "eb-mvp01-20241118-99999",
            "SKUID_URL": "http://localhost:4000/no/where",
        },
    ):
        yield


def test_init():
    subarray_id = 1
    mid_script.init(subarray_id)
    assert isinstance(mid_script.main, partial)
    assert mid_script.main.args == (subarray_id,)


@patch.multiple(
    "scripts.allocate_and_observe_sb",
    devicecontrol=DEFAULT,
    autospec=True,
)
# Turn off workaround that does not allow offsets in targets (TMC limitation)
@patch(
    "ska_oso_scripting.functions.devicecontrol.WORKAROUNDS.disable_pointing_groups",
    new=False,
)
def test_observe(**mocks):
    subarray_id = 1
    with open(PATH_TO_MID_SB, "r", encoding="utf-8") as fh:
        sbi = SBDefinition.model_validate_json(fh.read())

    # Act...
    mid_script.observe(SubArray(subarray_id), sbi)

    mocks["devicecontrol"].configure_from_cdm.assert_called()
    mocks["devicecontrol"].scan.assert_called()


@patch.object(SkuidClient, "fetch_skuid", MagicMock(side_effect="pb-1"))
def test_assign_resources():
    wrapped_pdm_transforms = MagicMock(wraps=pdm_transforms)
    with patch.multiple(
        "scripts.allocate_and_observe_sb",
        devicecontrol=DEFAULT,
        pdm_transforms=wrapped_pdm_transforms,
    ) as mocks:
        subarray_id = 1
        with open(PATH_TO_MID_SB, "r", encoding="utf-8") as fh:
            sbi = SBDefinition.model_validate_json(fh.read())

        # Act...
        mid_script.assign_resources(SubArray(subarray_id), sbi)

        mocks["devicecontrol"].assign_resources_from_cdm.assert_called()


@patch.multiple(
    "scripts.allocate_and_observe_sb",
    sb=DEFAULT,
    observe=DEFAULT,
    assign_resources=DEFAULT,
    release_all_resources=DEFAULT,
    oda_helper=DEFAULT,
    autospec=True,
)
def test_main(**mocks):
    # Arrange
    red, blue = "FAKE_EB_ID", "FAKE_SBI_ID"
    fake_sbi = MagicMock(autospec=SBDefinition)
    mocks["oda_helper"].create_eb.return_value = red
    mocks["sb"].load_sbd.return_value = fake_sbi

    # Act
    mid_script._main(subarray_id=1, sb_json="some_file.json", sbi_id=blue)

    # Assert
    mocks["oda_helper"].create_eb.assert_called_once()

    expected_sa = SubArray(1)
    mocks["assign_resources"].assert_called_once_with(expected_sa, fake_sbi)
    mocks["observe"].assert_called_once_with(expected_sa, fake_sbi)
    mocks["release_all_resources"].assert_called_once_with(expected_sa.id)
