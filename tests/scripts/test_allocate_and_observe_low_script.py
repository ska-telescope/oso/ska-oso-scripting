import os
from functools import partial
from pathlib import Path
from unittest.mock import DEFAULT, MagicMock, patch

import pytest
from ska_oso_pdm import SBDefinition
from ska_ser_skuid.client import SkuidClient

from scripts import allocate_and_observe_sb as low_script
from ska_oso_scripting.functions import pdm_transforms
from ska_oso_scripting.objects import SubArray

PATH_TO_TEST = Path(__file__).parents[1]
PATH_TO_LOW_SB = PATH_TO_TEST.joinpath(
    "resources",
    "pdm_transforms_test_cases",
    "low_no_tied_array_no_sdp",
    "sb_definition.json",
)

with open(PATH_TO_LOW_SB, "r", encoding="utf-8") as fh:
    FIXTURE_SB = SBDefinition.model_validate_json(fh.read())


@pytest.fixture(autouse=True)
def mock_settings_env_vars():
    with patch.dict(
        os.environ,
        {
            "EB_ID": "eb-mvp01-20241118-99999",
            "SKUID_URL": "http://localhost:4000/no/where",
        },
    ):
        yield


@patch("scripts.allocate_and_observe_sb._main", MagicMock(spec=low_script._main))
def test_init():
    subarray_id = 1
    low_script.init(subarray_id)
    assert isinstance(low_script.main, partial)
    assert low_script.main.args == (subarray_id,)


@patch.multiple(
    "scripts.allocate_and_observe_sb",
    sb=DEFAULT,
    assign_resources=DEFAULT,
    release_all_resources=DEFAULT,
    observe=DEFAULT,
    oda_helper=DEFAULT,
    autospec=True,
)
def test_main(**mocks):
    # Arrange
    red, blue = "FAKE_EB_ID", "FAKE_SBI_ID"
    fake_sbi = MagicMock(autospec=SBDefinition)
    mocks["oda_helper"].create_eb.return_value = red
    mocks["sb"].load_sbd.return_value = fake_sbi

    # Act
    low_script._main(subarray_id=1, sb_json="some_file.json", sbi_id=blue)

    # Assert
    mocks["oda_helper"].create_eb.assert_called_once()

    expected_sa = SubArray(1)
    mocks["assign_resources"].assert_called_once_with(expected_sa, fake_sbi)
    mocks["observe"].assert_called_once_with(expected_sa, fake_sbi)
    mocks["release_all_resources"].assert_called_once_with(expected_sa.id)


@patch.object(SkuidClient, "fetch_skuid", MagicMock(side_effect="pb-1"))
def test_assign_resources():
    wrapped_pdm_transforms = MagicMock(wraps=pdm_transforms)
    with patch.multiple(
        "scripts.allocate_and_observe_sb",
        devicecontrol=DEFAULT,
        pdm_transforms=wrapped_pdm_transforms,
    ) as mocks:
        subarray_id = 1

        # Act...
        low_script.assign_resources(SubArray(subarray_id), FIXTURE_SB)

        mocks["devicecontrol"].assign_resources_from_cdm.assert_called()


@patch.multiple(
    "scripts.allocate_and_observe_sb",
    devicecontrol=DEFAULT,
    autospec=True,
)
def test_observe(**mocks):
    # Arrange...
    subarray_id = 1
    # Act...
    low_script.observe(SubArray(subarray_id), FIXTURE_SB)

    mocks["devicecontrol"].configure_from_cdm.assert_called()
    mocks["devicecontrol"].scan.assert_called()
