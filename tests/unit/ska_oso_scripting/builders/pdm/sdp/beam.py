"""
This module contains code used to help create test fixtures for PDM Beams.
"""
import random
import uuid

from ska_oso_pdm.sb_definition.sdp import Beam, BeamFunction


class BeamBuilder:
    """
    BeamBuilder is a test data builder for PDM Beam objects.

    By default, BeamBuilder will build a Beam suitable for visibility
    observing.
    """

    def __init__(self):
        self.beam_id = str(uuid.uuid4())
        self.function = BeamFunction.VISIBILITIES
        self.search_beam_id = None
        self.timing_beam_id = None
        self.vlbi_beam_id = None

    def with_beam_id(self, new_id) -> "BeamBuilder":
        self.beam_id = str(new_id)
        return self

    def with_function(self, function: BeamFunction) -> "BeamBuilder":
        self.function = function
        return self

    def with_search_beam_id(self, beam_id: int) -> "BeamBuilder":
        self.search_beam_id = beam_id
        return self

    def with_timing_beam_id(self, beam_id: int) -> "BeamBuilder":
        self.timing_beam_id = beam_id
        return self

    def with_vlbi_beam_id(self, beam_id: int) -> "BeamBuilder":
        self.vlbi_beam_id = beam_id
        return self

    def for_visibilities(self) -> "BeamBuilder":
        """
        Return a BeamBuilder configured for visibility observing.
        """
        # Don't know what valid beam IDs would be for any particular beam type
        # or if there's overlap. For now, use a random integer partitioned by
        # type.
        beam_id = random.randint(1, 10)
        return (
            BeamBuilder()
            .with_function(BeamFunction.VISIBILITIES)
            .with_beam_id(f"vis-{beam_id}")
        )

    def for_transient_buffer(self) -> "BeamBuilder":
        """
        Return a BeamBuilder configured for transient buffer processing.
        """
        # Assume that transient buffer would be recording visibility beams.
        # Not sure whether this is correct.
        return self.for_visibilities().with_function(BeamFunction.TRANSIENT_BUFFER)

    def for_pss(self) -> "BeamBuilder":
        """
        Return a BeamBuilder configured for pulsar search observations.
        """
        beam_id = random.randint(11, 20)
        return (
            BeamBuilder()
            .with_function(BeamFunction.PULSAR_SEARCH)
            .with_beam_id(f"pss-{beam_id}")
            .with_search_beam_id(beam_id)
        )

    def for_pst(self) -> "BeamBuilder":
        """
        Return a BeamBuilder configured for pulsar timing observations.
        """
        beam_id = random.randint(21, 30)
        return (
            BeamBuilder()
            .with_function(BeamFunction.PULSAR_TIMING)
            .with_beam_id(f"pst-{beam_id}")
            .with_timing_beam_id(beam_id)
        )

    def for_vlbi(self) -> "BeamBuilder":
        """
        Return a BeamBuilder configured for VLBI observing.
        """
        beam_id = random.randint(31, 40)
        return (
            BeamBuilder()
            .with_function(BeamFunction.VLBI)
            .with_beam_id(f"vlbi-{beam_id}")
            .with_vlbi_beam_id(beam_id)
        )

    def build(self) -> Beam:
        """
        Return a Beam based on the current state of this builder.
        """
        return Beam(
            beam_id=self.beam_id,
            function=self.function,
            search_beam_id=self.search_beam_id,
            timing_beam_id=self.timing_beam_id,
            vlbi_beam_id=self.vlbi_beam_id,
        )
