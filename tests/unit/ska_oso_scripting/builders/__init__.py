"""
The builders package contains code to help create test fixtures using the
'builder' pattern.

See http://www.natpryce.com/articles/000714.html and references therein for
background on test data builders.
"""
