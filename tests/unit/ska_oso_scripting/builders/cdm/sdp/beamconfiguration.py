"""
This module contains code used to help create test fixtures for CDM
BeamConfigurations.
"""
import random
import uuid

from ska_tmc_cdm.messages.central_node.sdp import BeamConfiguration


class BeamConfigurationBuilder:
    """
    BeamConfigurationBuilder is a test data builder for CDM BeamConfiguration
    objects.

    By default, BeamConfigurationBuilder will build a BeamConfiguration
    suitable for visibility observing.
    """

    def __init__(self):
        self.beam_id = str(uuid.uuid4())
        self.function = "visibilities"
        self.search_beam_id = None
        self.timing_beam_id = None
        self.vlbi_beam_id = None

    def with_beam_id(self, new_id) -> "BeamConfigurationBuilder":
        self.beam_id = str(new_id)
        return self

    def with_function(self, function: str) -> "BeamConfigurationBuilder":
        self.function = function
        return self

    def with_search_beam_id(self, beam_id: int) -> "BeamConfigurationBuilder":
        self.search_beam_id = beam_id
        return self

    def with_timing_beam_id(self, beam_id: int) -> "BeamConfigurationBuilder":
        self.timing_beam_id = beam_id
        return self

    def with_vlbi_beam_id(self, beam_id: int) -> "BeamConfigurationBuilder":
        self.vlbi_beam_id = beam_id
        return self

    def for_visibilities(self) -> "BeamConfigurationBuilder":
        """
        Return a BeamConfigurationBuilder configured for visibility observing.
        """
        # Don't know what valid beam IDs would be for any particular beam type
        # or if there's overlap. For now, use a random integer partitioned by
        # type.
        beam_id = random.randint(1, 10)
        return (
            BeamConfigurationBuilder()
            .with_function("visibilities")
            .with_beam_id(f"vis-{beam_id}")
        )

    def for_transient_buffer(self) -> "BeamConfigurationBuilder":
        """
        Return a BeamConfigurationBuilder configured for transient buffer
        processing.
        """
        # Assume that transient buffer would be recording visibility beams.
        # Not sure whether this is correct.
        return self.for_visibilities().with_function("transient buffer")

    def for_pss(self) -> "BeamConfigurationBuilder":
        """
        Return a BeamConfigurationBuilder configured for pulsar search
        observations.
        """
        beam_id = random.randint(11, 20)
        return (
            BeamConfigurationBuilder()
            .with_beam_id(f"pss-{beam_id}")
            .with_function("pulsar search")
            .with_search_beam_id(beam_id)
        )

    def for_pst(self) -> "BeamConfigurationBuilder":
        """
        Return a BeamConfigurationBuilder configured for pulsar timing
        observations.
        """
        beam_id = random.randint(21, 30)
        return (
            BeamConfigurationBuilder()
            .with_beam_id(f"pst-{beam_id}")
            .with_function("pulsar timing")
            .with_timing_beam_id(beam_id=beam_id)
        )

    def for_vlbi(self) -> "BeamConfigurationBuilder":
        """
        Return a BeamConfigurationBuilder configured for VLBI observations.
        """
        # Don't know what valid beam IDs would be for any particular beam type
        # or if there's overlap. For now, use a random integer partitioned by
        # type.
        beam_id = random.randint(31, 40)
        return (
            self.with_function("vlbi")
            .with_beam_id(f"vlbi-{beam_id}")
            .with_vlbi_beam_id(beam_id)
        )

    def build(self) -> BeamConfiguration:
        """
        Return a CDM BeamConfiguration based on the current state of this
        builder.
        """
        return BeamConfiguration(
            beam_id=self.beam_id,
            function=self.function,
            search_beam_id=self.search_beam_id,
            timing_beam_id=self.timing_beam_id,
            vlbi_beam_id=self.vlbi_beam_id,
        )
