"""
Unit tests for the ska_oso_scripting.messages module
"""
from pubsub import pub

from ska_oso_scripting.event import user_topics
from ska_oso_scripting.functions import messages


def test_publish_event_message_verify_default_topic_assigned():
    """
    Publishing an event using the general observing task should
    publish it on a preset topic user.script.announce
    """
    topic_list = []

    def catch_topic(msg_src, msg):  # pylint: disable=unused-argument
        topic_list.append(msg)

    pub.subscribe(catch_topic, user_topics.script.announce)
    messages.publish_event_message(msg="test message")
    assert "test message" in topic_list
