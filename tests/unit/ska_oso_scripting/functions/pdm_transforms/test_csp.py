"""
Unit tests for the ska_oso_scripting.pdm_transforms module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import copy
from contextlib import nullcontext
from datetime import timedelta
from unittest import mock

import pytest
import ska_oso_pdm.sb_definition as pdm
import ska_tmc_cdm.messages.subarray_node.configure.csp as cdm
from astropy import units
from ska_oso_pdm import SubArrayLOW, TelescopeType
from ska_oso_pdm.sb_definition import CSPConfiguration
from ska_oso_pdm.sb_definition.csp.midcbf import (
    ChannelAveragingFactor,
    CorrelationSPWConfiguration,
    MidCBFConfiguration,
    ReceiverBand,
    Subband,
)
from ska_tmc_cdm.messages.central_node.csp import PSTConfiguration

from ska_oso_scripting.functions.pdm_transforms.csp import (
    CSP_LOW_SCHEMA_URL,
    CSP_MID_SCHEMA_URL,
    _consume_fsps,
    convert_cbfconfiguration,
    convert_commonconfiguration,
    convert_csp_id,
    convert_cspconfiguration,
    convert_fspconfiguration,
    convert_lowcbfconfiguration,
    convert_midcbfconfiguration,
    convert_pstconfiguration_centralnode,
    create_pss_config,
    create_pst_config,
)


class PDMData:
    """
    Python object to hold PDM test data.
    """

    mid_fsp1 = pdm.csp.FSPConfiguration(
        fsp_id=1,
        function_mode=pdm.csp.FSPFunctionMode.CORR,
        frequency_slice_id=1,
        integration_factor=1,
        zoom_factor=0,
        channel_averaging_map=[(0, 2), (744, 0)],
        output_link_map=[(0, 0), (200, 1)],
        channel_offset=0,
    )
    mid_fsp2 = pdm.csp.FSPConfiguration(
        fsp_id=2,
        function_mode=pdm.csp.FSPFunctionMode.CORR,
        frequency_slice_id=2,
        integration_factor=1,
        zoom_factor=1,
        channel_averaging_map=[(0, 2), (744, 0)],
        output_link_map=[(0, 4), (200, 5)],
        channel_offset=744,
        zoom_window_tuning=4700000,
    )

    calibrator_target = pdm.Target(
        target_id="Polaris Australis",
        reference_coordinate=pdm.EquatorialCoordinates(
            ra="21:08:47.92", dec="-88:57:22.9"
        ),
        pointing_pattern=pdm.PointingPattern(
            active=pdm.PointingKind.FIVE_POINT,
            parameters=[
                pdm.FivePointParameters(offset_arcsec=5.0),
                pdm.RasterParameters(
                    row_length_arcsec=1.23,
                    row_offset_arcsec=4.56,
                    n_rows=2,
                    pa=7.89,
                    unidirectional=True,
                ),
                pdm.StarRasterParameters(
                    row_length_arcsec=1.23,
                    n_rows=2,
                    row_offset_angle=4.56,
                    unidirectional=True,
                ),
            ],
        ),
        radial_velocity=pdm.RadialVelocity(
            quantity=units.Quantity(
                value=-12.345, unit=pdm.RadialVelocityUnits.KM_PER_SEC
            ),
            definition=pdm.RadialVelocityDefinition.OPTICAL,
            reference_frame=pdm.RadialVelocityReferenceFrame.LSRK,
            redshift=1.23,
        ),
        tied_array_beams=pdm.TiedArrayBeams(
            pst_beams=[
                pdm.Beam(
                    beam_id=1,
                    beam_coordinate=pdm.EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204R",
                        reference_frame=pdm.EquatorialCoordinatesReferenceFrame.ICRS,
                        ra_str="00:24:05.670",
                        dec_str="-72:04:52.62",
                        pm_ra=4.8,
                        pm_dec=-3.3,
                    ),
                    stn_weights=[1.0, 1.0],
                )
            ]
        ),
    )

    science_target = pdm.Target(
        target_id="M83",
        reference_coordinate=pdm.EquatorialCoordinates(
            ra="13:37:00.919", dec="-29:51:56.74"
        ),
        pointing_pattern=pdm.PointingPattern(
            active=pdm.PointingKind.SINGLE_POINT,
            parameters=[pdm.SinglePointParameters()],
        ),
        radial_velocity=pdm.RadialVelocity(),
        tied_array_beams=pdm.TiedArrayBeams(
            pst_beams=[
                pdm.Beam(
                    beam_id=1,
                    beam_coordinate=pdm.EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204R",
                        reference_frame=pdm.EquatorialCoordinatesReferenceFrame.ICRS,
                        ra_str="00:24:05.670",
                        dec_str="-72:04:52.62",
                        pm_ra=4.8,
                        pm_dec=-3.3,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
                pdm.Beam(
                    beam_id=2,
                    beam_coordinate=pdm.EquatorialCoordinatesPST(
                        target_id="PSR J0024-7204W",
                        reference_frame=pdm.EquatorialCoordinatesReferenceFrame.ICRS,
                        ra_str="00:24:06.0580",
                        dec_str="-72:04:49.088",
                        pm_ra=6.1,
                        pm_dec=-2.6,
                    ),
                    stn_weights=[1.0, 1.0],
                ),
            ]
        ),
    )

    science_target_2 = pdm.Target(
        target_id="AR Sco",
        reference_coordinate=pdm.EquatorialCoordinates(
            ra="16:21:47.283", dec="-22:53:10.386"
        ),
        pointing_pattern=pdm.PointingPattern(
            active=pdm.PointingKind.SINGLE_POINT,
            parameters=[pdm.SinglePointParameters()],
        ),
        radial_velocity=pdm.RadialVelocity(),
    )

    targets = [science_target, calibrator_target, science_target_2]

    mccs_allocation = pdm.MCCSAllocation(
        mccs_allocation_id="mccs config 1",
        selected_subarray_definition=SubArrayLOW.AA05_ALL,
        subarray_beams=[
            pdm.mccs.mccs_allocation.SubarrayBeamConfiguration(
                subarray_beam_id=1,
                number_of_channels=6,
                apertures=[
                    pdm.mccs.mccs_allocation.Aperture(station_id=1, substation_id=2)
                ],
            )
        ],
    )

    correlation_spw_midcbf1 = CorrelationSPWConfiguration(
        spw_id=1,
        logical_fsp_ids=[1, 2],
        zoom_factor=0,
        centre_frequency=500e6,
        number_of_channels=1000,
        channel_averaging_factor=ChannelAveragingFactor.ONE,
        time_integration_factor=1,
    )

    correlation_spw_midcbf2 = CorrelationSPWConfiguration(
        spw_id=2,
        logical_fsp_ids=[3],
        zoom_factor=0,
        centre_frequency=600e6,
        number_of_channels=1000,
        channel_averaging_factor=ChannelAveragingFactor.ONE,
        time_integration_factor=1,
    )

    subbands = [
        Subband(
            frequency_slice_offset=units.Quantity(value=1.23, unit="MHz"),
            correlation_spws=[correlation_spw_midcbf1],
        )
    ]

    correlation_spws = [
        pdm.csp.lowcbf.Correlation(
            spw_id=1,
            logical_fsp_ids=[0, 1],
            zoom_factor=0,
            centre_frequency=199609375.0,
            number_of_channels=6,
            integration_time_ms=timedelta(milliseconds=849),
        )
    ]

    low_cbf = pdm.csp.lowcbf.LowCBFConfiguration(correlation_spws=correlation_spws)


class CDMData:
    """
    Python object to hold CDM test data.
    """

    stn_beams = cdm.StnBeamConfiguration(beam_id=1, freq_ids=[400])

    vis_station_beam_configuration = cdm.VisStnBeamConfiguration(
        stn_beam_id=1, integration_ms=850
    )

    fsp = cdm.VisFspConfiguration(function_mode="vis", fsp_ids=[1])

    station_configuration = cdm.StationConfiguration(
        stns=[[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1]],
        stn_beams=[cdm.StnBeamConfiguration(beam_id=1, freq_ids=[400])],
    )

    vis_configuration = cdm.VisConfiguration(
        fsp=cdm.VisFspConfiguration(function_mode="vis", fsp_ids=[0, 1, 1, 2]),
        stn_beams=[
            cdm.VisStnBeamConfiguration(
                stn_beam_id=1,
                integration_ms=850,
            )
        ],
    )

    low_cbf = cdm.LowCBFConfiguration(
        stations=cdm.StationConfiguration(
            stns=[[1, 2]],
            stn_beams=[
                cdm.StnBeamConfiguration(
                    beam_id=1,
                    freq_ids=[253, 254, 255, 256, 257, 258],
                )
            ],
        ),
        vis=cdm.VisConfiguration(
            fsp=cdm.VisFspConfiguration(firmware="vis", fsp_ids=[1]),
            stn_beams=[
                cdm.VisStnBeamConfiguration(
                    stn_beam_id=1,
                    integration_ms=849,
                )
            ],
        ),
    )
    midcbf1 = cdm.MidCBFConfiguration(
        correlation=cdm.CorrelationConfiguration(
            processing_regions=[
                cdm.ProcessingRegionConfiguration(
                    fsp_ids=[1, 2],
                    start_freq=493286720,
                    receptors=[],
                    channel_width=13440,
                    channel_count=1000,
                    integration_factor=1,
                    sdp_start_channel_id=0,
                )
            ]
        ),
        vlbi_config=None,
    )

    midcbf2 = cdm.MidCBFConfiguration(
        correlation=cdm.CorrelationConfiguration(
            processing_regions=[
                cdm.ProcessingRegionConfiguration(
                    fsp_ids=[1, 2],
                    start_freq=493286720,
                    receptors=[],
                    channel_width=13440,
                    channel_count=1000,
                    integration_factor=1,
                    sdp_start_channel_id=0,
                ),
                cdm.ProcessingRegionConfiguration(
                    fsp_ids=[3],
                    start_freq=593286720,
                    receptors=[],
                    channel_width=13440,
                    channel_count=1000,
                    integration_factor=1,
                    sdp_start_channel_id=1000,
                ),
            ]
        ),
        vlbi_config=None,
    )


@pytest.mark.parametrize(
    "num_fsps,available_fsps,expectation",
    [
        pytest.param(
            2, [1, 3, 5], nullcontext([1, 3]), id="consume FSP IDs from ordered list"
        ),
        pytest.param(
            2, [3, 5, 1], nullcontext([1, 3]), id="consume FSP IDs from unordered list"
        ),
        pytest.param(
            99,
            [5, 3, 1],
            pytest.raises(ValueError),
            id="num FSPs requested > num FSPs available",
        ),
    ],
)
def test_consume_fsps(num_fsps: int, available_fsps: list[int], expectation):
    """
    Verify FSP selection behaviour:
    - number available must be > 0
    - cannot request more FSPs than are available
    - FSPs are taken by order of lowest value.
    """
    with expectation as expected:
        # nullcontext provides its argument as the context manager target, so
        # if an exception is not raised then this target can be compared as
        # the expected result
        assert _consume_fsps(num_fsps, available_fsps) == expected


def test_pss_configs_must_be_undefined():
    """
    PSS configurations have not been defined yet, hence we don't know how to
    transform them. Verify that exceptions will be raised if PSSConfigurations
    are defined.
    """
    with pytest.raises(NotImplementedError):
        create_pss_config(pdm.csp.PSSConfiguration())


class TestCSPIdConversion:
    """
    Unit tests for CSP ID conversions.
    """

    @staticmethod
    @pytest.mark.parametrize(
        "pdm_val,expected",
        [(pdm.csp.CSPConfigurationID("123"), "123"), (None, None)],
    )
    def test_convert_csp_id_for_non_null_values(pdm_val, expected):
        """
        Verify that CSP IDs are converted from PDM to CDM correctly.
        """
        actual = convert_csp_id(pdm_val)
        assert actual == expected


class TestCBFConfiguration:
    """
    Unit tests for CBFConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_convert_cbfconfiguration_complains_on_invalid_type():
        """
        Verify that convert_cbfconfiguration tests the input argument type.
        """
        with pytest.raises(TypeError):
            convert_cbfconfiguration(1)

    @staticmethod
    def test_convert_cbfconfiguration_raises_exception_when_vlbi_config_is_specified():
        """
        VLBI configurations are unhandled. Confirm that an exception is raised
        if VLBI configurations are specified.
        """
        input_val = pdm.csp.CBFConfiguration(
            fsps=[PDMData.mid_fsp1], vlbi=pdm.csp.cbf.VLBIConfiguration()
        )

        with pytest.raises(NotImplementedError):
            convert_cbfconfiguration(input_val)

    @staticmethod
    def tests_convert_pst_configuration_centralnode():
        """
        Verify that convert PST configuration for assign resources function correctly handles targets
        with tied array beam observations
        """
        expected = PSTConfiguration(pst_beam_ids=[1, 2])
        actual = convert_pstconfiguration_centralnode(pdm_targets=PDMData.targets)

        assert actual == expected

    @staticmethod
    def tests_convert_pst_configuration_centralnode_with_no_tied_array_observations():
        """
        Verify that convert PST configuration for assign resources function correctly handles targets
        with no tied array beam observations
        """
        actual = convert_pstconfiguration_centralnode(
            pdm_targets=[PDMData.science_target_2]
        )

        assert actual is None

    @staticmethod
    def test_convert_cbfconfiguration_converts_to_correct_cdm_entity():
        """
        Verify that convert_cbfconfiguration converts from PDM to CDM correctly.
        """
        input_val = pdm.csp.CBFConfiguration(fsps=[PDMData.mid_fsp1], vlbi=None)

        expected = cdm.CBFConfiguration(
            # FSPConfiguration conversion is tested elsewhere, so we can
            # safely reuse that function here
            fsp_configs=[convert_fspconfiguration(PDMData.mid_fsp1)],
            vlbi_config=None,
        )

        actual = convert_cbfconfiguration(input_val)
        assert actual == expected

    @staticmethod
    def test_convert_cbfconfiguration_returns_none_when_input_is_none():
        """
        Verify that convert_cbfconfiguration works when the CBFConfiguration is
        undefined.
        """
        input_val = None
        expected = None
        actual = convert_cbfconfiguration(input_val)
        assert actual == expected


class TestMidCBFConfiguration:
    """
    Unit tests for MidCBFConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_convert_midcbfconfiguration_complains_on_invalid_type():
        """
        Verify that convert_midcbfconfiguration tests the input argument type.
        """
        with pytest.raises(TypeError):
            convert_midcbfconfiguration(1)

    @staticmethod
    def test_convert_midcbfconfiguration_converts_to_correct_cdm_entity():
        """
        Verify that convert_midcbfconfiguration converts from PDM to CDM correctly.
        """

        input_val = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=PDMData.subbands
        )
        expected = CDMData.midcbf1

        actual = convert_midcbfconfiguration(input_val)
        assert actual == expected

    @staticmethod
    def test_convert_midcbfconfiguration_with_two_spws_converts_to_correct_cdm_entity():
        """
        Verify that convert_midcbfconfiguration converts from PDM to CDM correctly.
        """
        subbands = copy.deepcopy(PDMData.subbands)
        subbands[0].correlation_spws = [
            PDMData.correlation_spw_midcbf1,
            PDMData.correlation_spw_midcbf2,
        ]

        input_val = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=subbands
        )
        expected = CDMData.midcbf2

        actual = convert_midcbfconfiguration(input_val)
        assert actual == expected

    @staticmethod
    @mock.patch(
        "ska_oso_scripting.functions.pdm_transforms.csp.WORKAROUNDS.exclude_frequency_band_offsets",
        new=False,
    )
    def test_convert_midcbfconfiguration_converts_to_correct_cdm_entity_without_workaround():
        """
        Verify that convert_midcbfconfiguration converts from PDM to CDM correctly.
        """

        input_val = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=PDMData.subbands
        )
        expected = copy.copy(CDMData.midcbf1)
        expected.frequency_band_offset_stream1 = 1230000

        actual = convert_midcbfconfiguration(input_val)
        assert actual == expected

    @staticmethod
    @mock.patch(
        "ska_oso_scripting.functions.pdm_transforms.csp.WORKAROUNDS.exclude_frequency_band_offsets",
        new=False,
    )
    def test_convert_midcbfconfiguration_with_two_spws_converts_to_correct_cdm_entity_without_workaround():
        """
        Verify that convert_midcbfconfiguration converts from PDM to CDM correctly.
        """
        subbands = copy.copy(PDMData.subbands)
        subbands[0].correlation_spws = [
            PDMData.correlation_spw_midcbf1,
            PDMData.correlation_spw_midcbf2,
        ]

        input_val = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=subbands
        )
        expected = copy.deepcopy(CDMData.midcbf2)
        expected.frequency_band_offset_stream1 = 1230000

        actual = convert_midcbfconfiguration(input_val)
        assert actual == expected


class TestLowPSTCBFConfigurationConversion:
    @staticmethod
    def test_convert_pst_config_rejects_multi_spws_only_if_tied_array_beams_are_present():
        """
        Verify that convert_pst_config rejects multiple spws definitions
        """
        low_cbf = copy.copy(PDMData.low_cbf)
        low_cbf.correlation_spws = [
            low_cbf.correlation_spws[0],
            low_cbf.correlation_spws[0],
        ]

        pdm_csp_low = pdm.CSPConfiguration(
            config_id="csp-mvp01-20220329-00001", lowcbf=low_cbf
        )
        pdm_targets = copy.copy(PDMData.calibrator_target)

        expected = (
            "Multiple spectral windows cannot be defined for tied array observations"
        )

        with pytest.raises(ValueError, match=expected):
            create_pst_config(pdm_val=pdm_csp_low, pdm_targets=pdm_targets)

        pdm_targets.tied_array_beams = None
        assert create_pst_config(pdm_val=pdm_csp_low, pdm_targets=pdm_targets) is None


class TestLowCBFConfigurationConversion:
    """
    Unit tests for LowCBFConfiguration
    """

    @staticmethod
    def test_convert_lowcbf_configuration():
        actual = convert_lowcbfconfiguration(
            pdm_lowcbf=PDMData.low_cbf, pdm_mccs=PDMData.mccs_allocation
        )
        assert actual == CDMData.low_cbf


class TestCSPConfigurationConversion:
    """
    Unit tests for CSPConfiguration PDM to CDM transforms.
    """

    mid_pdm_entities = dict(
        config_id="sbi-mvp01-20200325-00001-science_A",
        subarray=pdm.csp.SubarrayConfiguration(subarray_name="subarray_name"),
        common=pdm.csp.CommonConfiguration(subarray_id=1),
        midcbf=MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=PDMData.subbands
        ),
        pss=None,
        pst=None,
    )

    @staticmethod
    @pytest.mark.parametrize("bad_val", ["foo", None])
    def test_convert_cspconfiguration_requires_pdm_cspconfiguration_instance(
        bad_val,
    ):
        """
        Verify that convert_cspconfiguration checks input argument types.
        """
        with pytest.raises(TypeError):
            convert_cspconfiguration(
                telescope=TelescopeType.SKA_MID,
                pdm_config=bad_val,
            )

    @staticmethod
    def test_cspconfiguration_throws_error_when_mccs_allocation_provided_for_mid_setup(
        mock_settings_env_vars,
    ):
        """
        Verify that convert_cspconfiguration checks input argument types.
        """
        pdm_config = pdm.CSPConfiguration(
            **TestCSPConfigurationConversion.mid_pdm_entities
        )
        with pytest.raises(ValueError):
            convert_cspconfiguration(
                telescope=TelescopeType.SKA_MID,
                pdm_config=pdm_config,
                pdm_mccs_allocation=PDMData.mccs_allocation,
            )

    @staticmethod
    def test_cspconfiguration_converts_mid_pdm_csp_configuration_to_cdm_correctly(
        mock_settings_env_vars,
    ):
        """
        Verify conversion of a complete CSP configuration.
        """
        # pdm_rx = ReceiverBand.BAND_1

        pdm_config = pdm.CSPConfiguration(
            **TestCSPConfigurationConversion.mid_pdm_entities
        )

        # We've already verified that the lower-level conversions work
        # correctly so it is safe to use that functionality here
        expected_cdm_entities = dict(
            interface=CSP_MID_SCHEMA_URL,
            common=convert_commonconfiguration(pdm_config),
            midcbf=convert_midcbfconfiguration(
                TestCSPConfigurationConversion.mid_pdm_entities["midcbf"]
            ),
            pss_config=None,
            pst_config=None,
        )

        expected = cdm.CSPConfiguration(**expected_cdm_entities)
        actual = convert_cspconfiguration(
            telescope=TelescopeType.SKA_MID,
            pdm_config=pdm_config,
        )

        assert expected == actual

    @staticmethod
    def test_multiple_cprs_are_assigned_contiguous_fsp_ids():
        """
        Verifies that multiple correlation spws are assigned consecutive FSP
        IDs.
        """
        corr_spw_args = dict(
            logical_fsp_ids=[],
            receptors=["SKA063", "SKA001", "SKA100"],
            zoom_factor=0,
            centre_frequency=700e6,
            channel_averaging_factor=ChannelAveragingFactor.ONE,
            time_integration_factor=1,
        )

        input_val = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1,
            subbands=[
                Subband(
                    frequency_slice_offset=units.Quantity(value=0, unit="MHz"),
                    correlation_spws=[
                        # minimum number of channels for 1 FSP
                        CorrelationSPWConfiguration(
                            spw_id=1, number_of_channels=20, **corr_spw_args
                        ),
                        # max channels for 1 FSP at 700 MHz
                        CorrelationSPWConfiguration(
                            spw_id=2, number_of_channels=940, **corr_spw_args
                        ),
                        # min for 2 FSPs at 700 MHz
                        CorrelationSPWConfiguration(
                            spw_id=3, number_of_channels=960, **corr_spw_args
                        ),
                        # max for 2 FSPs at 700 MHz
                        CorrelationSPWConfiguration(
                            spw_id=4, number_of_channels=28540, **corr_spw_args
                        ),
                        # should require 3 FSPs
                        CorrelationSPWConfiguration(
                            spw_id=5, number_of_channels=28560, **corr_spw_args
                        ),
                    ],
                ),
            ],
        )
        converted = convert_midcbfconfiguration(input_val)

        expected = [[1], [2], [3, 4], [5, 6], [7, 8, 9]]
        actual = [cpr.fsp_ids for cpr in converted.correlation.processing_regions]
        assert actual == expected

    @staticmethod
    def test_cspconfiguration_converts_low_pdm_with_no_pst_csp_configuration_to_cdm_correctly(
        mock_settings_env_vars,
    ):
        pdm_csp_low = pdm.CSPConfiguration(
            config_id="csp-mvp01-20220329-00001",
            lowcbf=PDMData.low_cbf,
            common=pdm.csp.CommonConfiguration(),
        )

        converted = convert_cspconfiguration(
            telescope=TelescopeType.SKA_LOW,
            pdm_config=pdm_csp_low,
            pdm_mccs_allocation=PDMData.mccs_allocation,
            target=PDMData.science_target_2,
        )

        expected = cdm.CSPConfiguration(
            interface=CSP_LOW_SCHEMA_URL,
            common=cdm.CommonConfiguration(
                config_id="csp-mvp01-20220329-00001", eb_id="eb-mvp01-20241118-99999"
            ),
            lowcbf=CDMData.low_cbf,
        )
        assert expected == converted

    @staticmethod
    def test_create_pst_fails_if_do_pst_positive_but_no_pst_in_targets():
        low_cbf = copy.copy(PDMData.low_cbf)
        low_cbf.do_pst = True
        csp = CSPConfiguration(lowcbf=low_cbf)
        target = PDMData.science_target_2
        with pytest.raises(ValueError):
            create_pst_config(csp, target)


class TestCommonConfigurationConversion:
    """
    Unit tests for CommonConfiguration PDM to CDM transforms.
    """

    pdm_csp_mid = pdm.csp.CSPConfiguration(
        **TestCSPConfigurationConversion.mid_pdm_entities
    )

    @staticmethod
    def test_convert_common_config_complains_when_no_eb_id_os_env_set():
        with pytest.raises(RuntimeError):
            convert_commonconfiguration(
                pdm.csp.CSPConfiguration(
                    **TestCSPConfigurationConversion.mid_pdm_entities
                )
            )

    @staticmethod
    def test_convert_commonconfig_complains_on_invalid_type(mock_settings_env_vars):
        """
        Verify that convert_commonconfiguration raises an exception if no CSPConfiguration passed.
        """
        with pytest.raises(TypeError):
            convert_commonconfiguration(1)

    @staticmethod
    def test_convert_commonconfig_complains_if_neither_mid_or_low_cbf_are_set(
        mock_settings_env_vars,
    ):
        empty_mid_and_low_cbf = copy.copy(TestCommonConfigurationConversion.pdm_csp_mid)
        empty_mid_and_low_cbf.midcbf = None
        with pytest.raises(ValueError):
            convert_commonconfiguration(empty_mid_and_low_cbf)

    @staticmethod
    def test_convert_common_works_as_expected_for_mid(mock_settings_env_vars):
        actual = convert_commonconfiguration(
            TestCommonConfigurationConversion.pdm_csp_mid
        )

        expected = cdm.CommonConfiguration(
            config_id="sbi-mvp01-20200325-00001-science_A",
            frequency_band=cdm.core.ReceiverBand.BAND_1,
            eb_id="eb-mvp01-20241118-99999",
        )
        assert expected == actual

    @staticmethod
    def test_convert_common_works_as_expected_for_low(mock_settings_env_vars):
        pdm_csp_low = copy.copy(TestCommonConfigurationConversion.pdm_csp_mid)
        pdm_csp_low.lowcbf = PDMData.low_cbf
        pdm_csp_low.midcbf = None

        actual = convert_commonconfiguration(pdm_csp_low)

        expected = cdm.CommonConfiguration(
            config_id="sbi-mvp01-20200325-00001-science_A",
            eb_id="eb-mvp01-20241118-99999",
        )
        assert expected == actual


class TestFSPConfigurationConversion:
    """
    Unit tests for FSPConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_fspconfiguration_converts_to_cdm_correctly():
        """
        Verify that to_fspconfiguration converts from PDM to CDM correctly.
        """
        expected = cdm.FSPConfiguration(
            fsp_id=1,
            function_mode=cdm.FSPFunctionMode.CORR,
            frequency_slice_id=1,
            integration_factor=1,
            zoom_factor=0,
            channel_averaging_map=[(0, 2), (744, 0)],
            output_link_map=[(0, 0), (200, 1)],
            channel_offset=0,
        )

        actual = convert_fspconfiguration(PDMData.mid_fsp1)
        assert actual == expected

        expected = cdm.FSPConfiguration(
            fsp_id=2,
            function_mode=cdm.FSPFunctionMode.CORR,
            frequency_slice_id=2,
            integration_factor=1,
            zoom_factor=1,
            channel_averaging_map=[(0, 2), (744, 0)],
            output_link_map=[(0, 4), (200, 5)],
            channel_offset=744,
            zoom_window_tuning=4700000,
        )

        actual = convert_fspconfiguration(PDMData.mid_fsp2)
        assert actual == expected

    @staticmethod
    @pytest.mark.parametrize("bad_val", ["foo", None])
    def test_convert_fspconfiguration_requires_pdm_fspconfiguration_instance(
        bad_val,
    ):
        """
        Verify that convert_fspconfiguration checks the input argument type.
        """
        with pytest.raises(TypeError):
            convert_fspconfiguration(bad_val)
