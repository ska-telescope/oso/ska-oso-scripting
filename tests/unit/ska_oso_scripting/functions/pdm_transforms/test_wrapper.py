"""
Unit tests for the ska_oso_scripting.pdm_transforms module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import copy
from unittest import mock

import pytest
from ska_oso_pdm import TelescopeType, builders
from ska_ser_skuid.client import SkuidClient
from ska_ser_skuid.services import SKAEntityTypeService
from ska_tmc_cdm.messages.central_node.assign_resources import AssignResourcesRequest
from ska_tmc_cdm.messages.central_node.common import DishAllocation
from ska_tmc_cdm.messages.central_node.mccs import MCCSAllocate
from ska_tmc_cdm.messages.central_node.sdp import SDPConfiguration
from ska_tmc_cdm.messages.subarray_node.configure import ConfigureRequest

from ska_oso_scripting.functions.pdm_transforms.wrapper import (
    create_cdm_assign_resources_request_from_scheduling_block,
    create_cdm_configure_request_from_scheduling_block,
)

from .conftest import (
    PDM_TRANSFORMS_TEST_RESOURCES,
    TEST_TIME,
    load_cdm_from_file,
    load_sbd_from_file,
)


@pytest.fixture
def skuid_generator():
    counter = {}

    def closure(entity_type: SKAEntityTypeService.DefaultEntityTypes):
        entity_counter = counter.setdefault(entity_type, 1)
        counter[entity_type] += 1
        return f"{entity_type.value}-mvp01-{TEST_TIME.strftime('%Y%m%d')}-{format(entity_counter, '05d')}"

    return closure


class TestCreateCDMConfigureRequest:
    """
    Unit tests for the wrapper function which maps an SBDefinition to a CDM ConfigureRequest
    """

    @pytest.mark.parametrize(
        "sb_definition_path,cdm_configure_paths",
        [
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_with_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_no_tied_array_with_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_tied_array_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_2spws_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_no_tied_array_2spws_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_with_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_tied_array_with_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_tied_array_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp_pre_ADR-63/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_tied_array_no_sdp_pre_ADR-63/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_no_tied_array_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_galactic_no_tied_array_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_tied_array_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "low_galactic_tied_array_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_2single_pointing_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_2single_pointing_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES / "mid_5point_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp/cdm_configure.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp/cdm_configure_p1.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp/cdm_configure_p2.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp/cdm_configure_p3.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp/cdm_configure_p4.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_galactic_2single_pointing_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_galactic_2single_pointing_no_sdp/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_5point_no_sdp_pre_ADR-63/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_5point_no_sdp_pre_ADR-63/cdm_configure.json",
                ],
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES / "mid_mosaic_no_sdp/sb_definition.json",
                [
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_mosaic_no_sdp/cdm_configure.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_mosaic_no_sdp/cdm_configure_p1.json",
                    PDM_TRANSFORMS_TEST_RESOURCES
                    / "mid_mosaic_no_sdp/cdm_configure_p2.json",
                ],
            ),
        ],
        ids=(
            "low_no_tied_array_with_sdp",
            "low_no_tied_array_no_sdp",
            "low_no_tied_array_2spws_no_sdp",
            "low_tied_array_with_sdp",
            "low_tied_array_no_sdp",
            "low_tied_array_no_sdp_pre_ADR-63",
            "low_galactic_no_tied_array_no_sdp",
            "low_galactic_tied_array_no_sdp",
            "mid_2single_pointing_no_sdp",
            "mid_5point_no_sdp",
            "mid_galactic_2single_pointing_no_sdp",
            "mid_5point_no_sdp_pre_ADR-63",
            "mid_mosaic_no_sdp",
        ),
    )
    # Turn off workaround that does not allow offsets in targets (TMC limitation)
    @mock.patch(
        "ska_oso_scripting.functions.devicecontrol.WORKAROUNDS.disable_pointing_groups",
        new=False,
    )
    def test_create_cdm_configure_request_from_scheduling_block(
        self, mock_settings_env_vars, sb_definition_path, cdm_configure_paths
    ):
        """
        This tests the end to end transform from an SBDefinition to a ConfigureRequest, using JSON test files as the input and output.
        """
        sbd = load_sbd_from_file(sb_definition_path)
        result = create_cdm_configure_request_from_scheduling_block(sbd)

        for i, cdm_configure_path in enumerate(cdm_configure_paths):
            expected = load_cdm_from_file(ConfigureRequest, cdm_configure_path)
            assert expected == result[sbd.scan_definitions[0].scan_definition_id][i]

    class TestMid:
        """
        Mid specific tests that check the correct Mid fields are populated and the Low fields are not.
        """

        def test_create_function_creates_dict_with_correct_keys(
            self, mock_settings_env_vars, mid_sbd
        ):
            mid_configure_request = create_cdm_configure_request_from_scheduling_block(
                mid_sbd
            )

            assert len(mid_configure_request.keys()) == 3
            assert "pointing calibration" in mid_configure_request
            assert "science scan with pointing model update" in mid_configure_request
            assert "science scan" in mid_configure_request

        def test_create_function_handles_mid_and_low_scheduling_blocks_correctly(
            self, mock_settings_env_vars, mid_sbd
        ):
            mid_configure_request = create_cdm_configure_request_from_scheduling_block(
                mid_sbd
            )

            assert mid_configure_request["science scan"][0].mccs is None
            assert mid_configure_request["science scan"][0].dish is not None

        def test_create_function_handles_single_point_scan_correctly(
            self, mock_settings_env_vars, mid_sbd
        ):
            mid_configure_request = create_cdm_configure_request_from_scheduling_block(
                mid_sbd
            )

            assert isinstance(mid_configure_request["science scan"], list)
            assert len(mid_configure_request["science scan"]) == 1
            assert isinstance(
                mid_configure_request["science scan"][0],
                ConfigureRequest,
            )
            assert (
                mid_configure_request["science scan"][0].tmc.partial_configuration
                is False
            )

        def test_create_function_handles_five_point_scan_correctly(
            self, mock_settings_env_vars, mid_sbd
        ):
            mid_configure_request = create_cdm_configure_request_from_scheduling_block(
                mid_sbd
            )

            scan_def_id = "pointing calibration"
            assert isinstance(mid_configure_request[scan_def_id], list)
            assert len(mid_configure_request[scan_def_id]) == 5

            configure_requests = mid_configure_request[scan_def_id]
            assert configure_requests[0].tmc.partial_configuration is False
            for cr in configure_requests[1:5]:
                assert cr.tmc.partial_configuration is True

        def test_create_function_creates_four_unique_partial_configuration_for_a_five_point_scan(
            self, mock_settings_env_vars, mid_sbd
        ):
            mid_configure_request = create_cdm_configure_request_from_scheduling_block(
                mid_sbd
            )

            partial_configure_requests = mid_configure_request["pointing calibration"][
                1:5
            ]
            for cr in partial_configure_requests:
                other_configure_requests = copy.copy(partial_configure_requests)
                other_configure_requests.remove(cr)
                for o in other_configure_requests:
                    assert cr != o

    class TestLow:
        """
        Low specific tests that check the correct Low fields are populated and the Mid fields are not.
        """

        SCAN_DEFINITION_ID = "science"

        def test_create_function_creates_dict_with_correct_keys(
            self, mock_settings_env_vars, low_sbd
        ):
            low_configure_request = create_cdm_configure_request_from_scheduling_block(
                low_sbd
            )
            assert len(low_configure_request.keys()) == 1
            assert self.SCAN_DEFINITION_ID in low_configure_request

        def test_create_function_low_scheduling_blocks_correctly(
            self, mock_settings_env_vars, low_sbd
        ):
            low_configure_request = create_cdm_configure_request_from_scheduling_block(
                low_sbd
            )

            assert low_configure_request[self.SCAN_DEFINITION_ID][0].dish is None
            assert low_configure_request[self.SCAN_DEFINITION_ID][0].mccs is not None


@mock.patch(
    "ska_oso_scripting.functions.pdm_transforms.sdp_create.datetime", scope="class"
)
@mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
class TestCreateCDMAssignResourcesRequest:
    """
    Unit tests for the wrapper function which maps an SBDefinition to a CDM AssignResourcesRequest
    """

    @pytest.mark.parametrize(
        "sb_definition_path,cdm_assign_resources_path",
        [
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_with_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_with_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_2spws_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_no_tied_array_2spws_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_with_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_with_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp_pre_ADR-63/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_tied_array_no_sdp_pre_ADR-63/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_no_tied_array_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_no_tied_array_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_tied_array_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_galactic_tied_array_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_2single_pointing_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_2single_pointing_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES / "mid_5point_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_5point_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_galactic_2single_pointing_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_galactic_2single_pointing_no_sdp/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_5point_no_sdp_pre_ADR-63/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_5point_no_sdp_pre_ADR-63/cdm_assign_resources.json",
            ),
            (
                PDM_TRANSFORMS_TEST_RESOURCES / "mid_mosaic_no_sdp/sb_definition.json",
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_mosaic_no_sdp/cdm_assign_resources.json",
            ),
        ],
        ids=(
            "low_no_tied_array_with_sdp",
            "low_no_tied_array_no_sdp",
            "low_no_tied_array_2spws_no_sdp",
            "low_tied_array_with_sdp",
            "low_tied_array_no_sdp",
            "low_tied_array_no_sdp_pre_ADR-63",
            "low_galactic_no_tied_array_no_sdp",
            "low_galactic_tied_array_no_sdp",
            "mid_2single_pointing_no_sdp",
            "mid_5point_no_sdp",
            "mid_galactic_2single_pointing_no_sdp",
            "mid_5point_no_sdp_pre_ADR-63",
            "mid_mosaic_no_sdp",
        ),
    )
    def test_create_cdm_assign_resources_request_from_scheduling_block(
        self,
        mock_fetch_fn,
        mock_datetime,
        mock_settings_env_vars,
        sb_definition_path,
        cdm_assign_resources_path,
        skuid_generator,
    ):
        """
        This tests the end to end transform from an SBDefinition to an AssignResourcesRequest, using JSON test files as the input and output.
        """
        mock_fetch_fn.side_effect = skuid_generator

        mock_datetime.now.return_value = TEST_TIME
        sbd = load_sbd_from_file(sb_definition_path)
        expected = load_cdm_from_file(AssignResourcesRequest, cdm_assign_resources_path)

        if sbd.telescope == TelescopeType.SKA_MID:
            expected.sdp_config.resources["receptors"] = sorted(
                expected.sdp_config.resources["receptors"]
            )

        result = create_cdm_assign_resources_request_from_scheduling_block(
            subarray_id=1, scheduling_block=sbd
        )

        assert expected == result

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    class TestMid:
        """
        Mid specific tests that check the correct Mid fields are populated and the Low fields are not.
        """

        def test_mid_assign_resources_has_correct_components(
            self, mock_fetch_fn, mock_settings_env_vars, mid_sbd, skuid_generator
        ):
            mock_fetch_fn.side_effect = skuid_generator
            request = create_cdm_assign_resources_request_from_scheduling_block(
                1, mid_sbd
            )
            assert isinstance(request.dish, DishAllocation)
            assert isinstance(
                request.sdp_config,
                SDPConfiguration,
            )
            assert request.mccs is None

        def test_create_non_sidereal_creates_dict_with_correct_keys(
            self,
            mock_fetch_fn,
            mock_settings_env_vars,
            mid_nonsidereal_sbd,
            skuid_generator,
        ):
            mock_fetch_fn.side_effect = skuid_generator
            mid_nonsidereal_request = (
                create_cdm_configure_request_from_scheduling_block(mid_nonsidereal_sbd)
            )

            assert "calibrator_scan" in mid_nonsidereal_request
            assert "science_scan" in mid_nonsidereal_request

        def test_mid_assign_resources_creates_correct_components(
            self, mock_fetch_fn, mock_settings_env_vars, mid_no_sdp_sbd, skuid_generator
        ):
            mock_fetch_fn.side_effect = skuid_generator

            request = create_cdm_assign_resources_request_from_scheduling_block(
                1, mid_no_sdp_sbd
            )
            assert request.sdp_config is not None

    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    class TestLow:
        """
        LOW specific tests that check the correct Low fields are populated and the MID fields are not.
        """

        def test_low_assign_resources_has_correct_components(
            self, mock_settings_env_vars, low_sbd
        ):
            request = create_cdm_assign_resources_request_from_scheduling_block(
                1, low_sbd
            )

            assert isinstance(request.mccs, MCCSAllocate)
            assert request.sdp_config is not None
            assert request.dish is None

        def test_low_assign_resources_creates_correct_components(
            self, mock_fetch_fn, mock_settings_env_vars, low_no_sdp_sbd, skuid_generator
        ):
            mock_fetch_fn.side_effect = skuid_generator

            request = create_cdm_assign_resources_request_from_scheduling_block(
                1, low_no_sdp_sbd
            )
            assert request.sdp_config is not None


class TestFromBuilders:
    @pytest.mark.parametrize(
        "telescope, cdm_assign_resources_path",
        [
            (
                TelescopeType.SKA_LOW,
                PDM_TRANSFORMS_TEST_RESOURCES
                / "low_from_builder/cdm_assign_resources.json",
            ),
            (
                TelescopeType.SKA_MID,
                PDM_TRANSFORMS_TEST_RESOURCES
                / "mid_from_builder/cdm_assign_resources.json",
            ),
        ],
    )
    @mock.patch(
        "ska_oso_scripting.functions.pdm_transforms.sdp_create.datetime", scope="class"
    )
    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_assign_resources_with_builders(
        self,
        mock_fetch_fn,
        mock_datetime,
        mock_settings_env_vars,
        telescope,
        cdm_assign_resources_path,
        skuid_generator,
    ):
        mock_fetch_fn.side_effect = skuid_generator
        mock_datetime.now.return_value = TEST_TIME

        match telescope:
            case TelescopeType.SKA_LOW:
                sbd = builders.low_imaging_sb()
            case TelescopeType.SKA_MID:
                sbd = builders.mid_imaging_sb()
            case _:
                raise ValueError(f"{telescope} is not supported")

        request = create_cdm_assign_resources_request_from_scheduling_block(
            subarray_id=1, scheduling_block=sbd
        )

        expected = load_cdm_from_file(AssignResourcesRequest, cdm_assign_resources_path)

        # this next bit is because our tests are a stupid and can't handle the generated IDs
        expected.sdp_config.execution_block.fields[
            0
        ].field_id = request.sdp_config.execution_block.fields[0].field_id
        expected.sdp_config.execution_block.scan_types[
            0
        ].scan_type_id = request.sdp_config.execution_block.scan_types[0].scan_type_id
        expected.sdp_config.execution_block.scan_types[0].beams["vis0"].field_id = (
            request.sdp_config.execution_block.scan_types[0].beams["vis0"].field_id
        )

        assert request == expected

    @pytest.mark.parametrize(
        "telescope, cdm_configure_path",
        [
            (
                TelescopeType.SKA_LOW,
                PDM_TRANSFORMS_TEST_RESOURCES / "low_from_builder/cdm_configure.json",
            ),
            (
                TelescopeType.SKA_MID,
                PDM_TRANSFORMS_TEST_RESOURCES / "mid_from_builder/cdm_configure.json",
            ),
        ],
    )
    @mock.patch(
        "ska_oso_scripting.functions.pdm_transforms.sdp_create.datetime", scope="class"
    )
    # Turn off workaround that does not allow offsets in targets (TMC limitation)
    @mock.patch(
        "ska_oso_scripting.functions.devicecontrol.WORKAROUNDS.disable_pointing_groups",
        new=False,
    )
    @mock.patch.object(SkuidClient, "fetch_skuid", scope="class")
    def test_create_configure_with_builders(
        self,
        mock_fetch_fn,
        mock_datetime,
        mock_settings_env_vars,
        telescope,
        cdm_configure_path,
        skuid_generator,
    ):
        mock_fetch_fn.side_effect = skuid_generator
        mock_datetime.now.return_value = TEST_TIME

        match telescope:
            case TelescopeType.SKA_LOW:
                sbd = builders.low_imaging_sb(scan_duration=100000)
            case TelescopeType.SKA_MID:
                sbd = builders.mid_imaging_sb(scan_duration=200000)
            case _:
                raise ValueError(f"{telescope} is not supported")

        request = create_cdm_configure_request_from_scheduling_block(
            scheduling_block=sbd
        )

        expected = load_cdm_from_file(ConfigureRequest, cdm_configure_path)

        request = request[sbd.scan_definitions[0].scan_definition_id][0]
        # again this next bit is because our tests are a stupid and can't handle the generated IDs
        expected.sdp.scan_type = request.sdp.scan_type
        expected.csp.common.config_id = request.csp.common.config_id
        if telescope == TelescopeType.SKA_MID:
            expected.pointing.target.target_name = request.pointing.target.target_name
            expected.pointing.groups[0].field.target_name = request.pointing.groups[
                0
            ].field.target_name

        assert request == expected
