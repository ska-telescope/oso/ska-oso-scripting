import datetime
import os
from pathlib import Path
from unittest import mock

import pytest
from ska_oso_pdm import SBDefinition
from ska_tmc_cdm.schemas import CODEC

TEST_RESOURCES = Path(__file__).parents[4] / "resources"
PDM_TRANSFORMS_TEST_RESOURCES = TEST_RESOURCES / "pdm_transforms_test_cases"
TEST_TIME = datetime.datetime.fromisoformat("2024-07-31T12:26:34.729430+00:00")


def load_cdm_from_file(entity_cls, file):
    json_path = Path(__file__).parent.resolve() / file
    return CODEC.load_from_file(entity_cls, json_path)


def load_sbd_from_file(file: str | Path) -> SBDefinition:
    path = Path(__file__).parents[0].resolve() / file
    with open(path, "r", encoding="utf-8") as fh:
        return SBDefinition.model_validate_json(fh.read())


@pytest.fixture()
def mock_settings_env_vars():
    with mock.patch.dict(
        os.environ,
        {
            "EB_ID": "eb-mvp01-20241118-99999",
            "SKUID_URL": "http://localhost:4000/no/where",
        },
    ):
        yield


@pytest.fixture
def mid_sbd() -> SBDefinition:
    return load_sbd_from_file(
        TEST_RESOURCES / "integration_test_cases/testfile_SS120_sb_mid.json"
    )


@pytest.fixture
def low_sbd() -> SBDefinition:
    return load_sbd_from_file(
        PDM_TRANSFORMS_TEST_RESOURCES / "low_no_tied_array_with_sdp/sb_definition.json"
    )


@pytest.fixture
def mid_nonsidereal_sbd() -> SBDefinition:
    return load_sbd_from_file(
        TEST_RESOURCES / "integration_test_cases/testfile_nonsidereal_sb_mid.json"
    )


@pytest.fixture
def mid_no_sdp_sbd() -> SBDefinition:
    return load_sbd_from_file(
        PDM_TRANSFORMS_TEST_RESOURCES / "mid_2single_pointing_no_sdp/sb_definition.json"
    )


@pytest.fixture
def low_no_sdp_sbd() -> SBDefinition:
    return load_sbd_from_file(
        PDM_TRANSFORMS_TEST_RESOURCES / "low_no_tied_array_no_sdp/sb_definition.json"
    )
