"""
Unit tests for the ska_oso_scripting.pdm_transforms module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import pytest
from pydantic import ValidationError
from ska_oso_pdm import SBDefinition, SubArrayLOW
from ska_oso_pdm.sb_definition import (
    CSPConfiguration,
    EquatorialCoordinates,
    EquatorialCoordinatesReferenceFrame,
    HorizontalCoordinates,
    SolarSystemObject,
    SolarSystemObjectName,
    Target,
)
from ska_oso_pdm.sb_definition.csp.lowcbf import Correlation as pdm_Correlation
from ska_oso_pdm.sb_definition.csp.lowcbf import (
    LowCBFConfiguration as pdm_LowCBFConfiguration,
)
from ska_oso_pdm.sb_definition.mccs.mccs_allocation import Aperture as pdm_Aperture
from ska_oso_pdm.sb_definition.mccs.mccs_allocation import (
    MCCSAllocation as pdm_MCCSAllocation,
)
from ska_oso_pdm.sb_definition.mccs.mccs_allocation import (
    SubarrayBeamConfiguration as pdm_SubarrayBeamConfiguration,
)
from ska_tmc_cdm.messages.central_node.mccs import (
    ApertureConfiguration as cdm_ApertureConfiguration,
)
from ska_tmc_cdm.messages.central_node.mccs import MCCSAllocate as cdm_MCCSAllocate
from ska_tmc_cdm.messages.central_node.mccs import (
    SubArrayBeamsConfiguration as cdm_SubArrayBeamsConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.mccs import (
    MCCSConfiguration as cdm_MCCSConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.mccs import (
    SubarrayBeamAperatures as cdm_SubarrayBeamAperatures,
)
from ska_tmc_cdm.messages.subarray_node.configure.mccs import (
    SubarrayBeamConfiguration as cdm_SubarrayBeamConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.mccs import (
    SubarrayBeamSkyCoordinates as cdm_SubarrayBeamSkyCoordinates,
)

from ska_oso_scripting.functions.pdm_transforms import (
    convert_mccs_configuration,
    create_mccs_allocation,
)
from ska_oso_scripting.functions.pdm_transforms.mccs import (
    get_allocation_apertures,
    get_aperture_id,
    get_sky_coordinates_based_on_reference_coordinates,
    get_station_ids,
    get_subarray_beam_ids,
)


class TestMCCSAllocationConversion:
    """
    Unit tests for MCCSAllocation PDM to CDM transforms.
    """

    pdm_apertures_with_1_aperture = [
        pdm_Aperture(station_id=344, substation_id=1, weighting_key="uniform"),
    ]
    pdm_apertures_with_2_apertures = [
        pdm_Aperture(station_id=344, substation_id=1, weighting_key="uniform"),
        pdm_Aperture(station_id=349, substation_id=1, weighting_key="uniform"),
    ]
    pdm_apertures_with_4_apertures = [
        pdm_Aperture(station_id=344, substation_id=1, weighting_key="uniform"),
        pdm_Aperture(station_id=349, substation_id=1, weighting_key="uniform"),
        pdm_Aperture(station_id=350, substation_id=1, weighting_key="uniform"),
        pdm_Aperture(station_id=362, substation_id=1, weighting_key="uniform"),
    ]
    pdm_subarray_beams_with_2_apertures = pdm_SubarrayBeamConfiguration(
        apertures=pdm_apertures_with_2_apertures,
        number_of_channels=96,
        subarray_beam_id=1,
    )
    pdm_subarray_beams_with_4_apertures = pdm_SubarrayBeamConfiguration(
        apertures=pdm_apertures_with_4_apertures,
        number_of_channels=6,
        subarray_beam_id=1,
    )
    pdm_allocation_with_4_apertures = pdm_MCCSAllocation(
        mccs_allocation_id="mccs config 1",
        selected_subarray_definition=SubArrayLOW.AA05_ALL,
        subarray_beams=[pdm_subarray_beams_with_4_apertures],
    )
    cdm_apertures_with_1_aperture = [
        cdm_ApertureConfiguration(station_id=344, aperture_id="AP344.01"),
    ]
    cdm_apertures_with_2_apertures = [
        cdm_ApertureConfiguration(station_id=344, aperture_id="AP344.01"),
        cdm_ApertureConfiguration(station_id=349, aperture_id="AP349.01"),
    ]
    cdm_apertures_with_4_apertures = [
        cdm_ApertureConfiguration(station_id=344, aperture_id="AP344.01"),
        cdm_ApertureConfiguration(station_id=349, aperture_id="AP349.01"),
        cdm_ApertureConfiguration(station_id=350, aperture_id="AP350.01"),
        cdm_ApertureConfiguration(station_id=362, aperture_id="AP362.01"),
    ]

    @pytest.mark.parametrize(
        "station_id,substation_id,expected",
        [
            (10, 50, "AP010.50"),
            (7, 1, "AP007.01"),
        ],
    )
    def test_get_aperture_id(self, station_id, substation_id, expected):
        assert get_aperture_id(station_id, substation_id) == expected

    @pytest.mark.parametrize(
        "station_id,substation_id",
        [
            (10, 50),
        ],
    )
    def test_aperture_valid_data(self, station_id, substation_id):
        aperture_instance = pdm_Aperture(
            station_id=station_id, substation_id=substation_id
        )
        assert aperture_instance is not None
        assert isinstance(aperture_instance, pdm_Aperture)
        assert aperture_instance.station_id == 10
        assert aperture_instance.substation_id == 50
        assert aperture_instance.weighting_key == "uniform"

    @pytest.mark.parametrize(
        "station_id,substation_id",
        [
            (-1, 50),  # station_id out of range (too low)
            (600, 50),  # station_id out of range (too high)
        ],
    )
    def test_aperture_data_out_of_range(self, station_id, substation_id):
        with pytest.raises(ValidationError):
            _ = pdm_Aperture(station_id=station_id, substation_id=substation_id)

    @pytest.mark.parametrize(
        "station_id,substation_id,weighting_key",
        [
            ("invalid_string", 50, ""),  # wrong station_id type
            (10, "invalid_string", ""),  # wrong substation_id type
            (10, 50, 123),  # wrong weighting_key type
        ],
    )
    def test_aperture_invalid_data_type(self, station_id, substation_id, weighting_key):
        with pytest.raises(ValueError):
            _ = pdm_Aperture(
                station_id=station_id,
                substation_id=substation_id,
                weighting_key=weighting_key,
            )

    @pytest.mark.parametrize(
        "number_of_channels",
        [
            -1,  # number_of_channels out of range (too low)
            996,  # number_of_channels out of range (too high)
            "test",  # number_of_channels wrong type
        ],
    )
    def test_get_subarray_beam_configuration_on_invalid_number_of_channels_range(
        self, number_of_channels
    ):
        pdm_apertures = [
            pdm_Aperture(station_id=344, substation_id=1, weighting_key="uniform"),
        ]
        with pytest.raises(ValidationError):
            _ = pdm_SubarrayBeamConfiguration(
                pdm_apertures=pdm_apertures,
                number_of_channels=number_of_channels,
                subarray_beam_id=1,
            )

    @pytest.mark.parametrize(
        "subarray_beam_id",
        [
            -1,  # subarray_beam_id out of range (too low)
            100,  # subarray_beam_id out of range (too high)
            "test",  # subarray_beam_id wrong type
        ],
    )
    def test_get_subarray_beam_configuration_on_invalid_subarray_beam_id_range(
        self, subarray_beam_id
    ):
        with pytest.raises(ValidationError):
            _ = pdm_SubarrayBeamConfiguration(
                pdm_apertures=self.pdm_apertures_with_1_aperture,
                number_of_channels=96,
                subarray_beam_id=subarray_beam_id,
            )

    @pytest.mark.parametrize(
        "configuration",
        [
            # 3,  # configuration wrong type
            "",  # configuration mandatory non-empty
        ],
    )
    def test_get_mccs_allocation_on_invalid_configuration_type(self, configuration):
        pdm_subarray_beams = pdm_SubarrayBeamConfiguration(
            apertures=self.pdm_apertures_with_1_aperture,
            number_of_channels=96,
            subarray_beam_id=1,
        )
        with pytest.raises(ValidationError):
            _ = pdm_MCCSAllocation(
                mccs_allocation_id="mccs config 123",
                selected_subarray_definition=configuration,
                subarray_beams=pdm_subarray_beams,
            )

    def test_get_subarray_beam_ids(self):
        pdm_allocation = pdm_MCCSAllocation(
            mccs_allocation_id="mccs config 123",
            selected_subarray_definition=SubArrayLOW.AA05_ALL,
            subarray_beams=[
                pdm_SubarrayBeamConfiguration(
                    apertures=self.pdm_apertures_with_2_apertures,
                    number_of_channels=96,
                    subarray_beam_id=1,
                )
            ],
        )
        actual = get_subarray_beam_ids(pdm_allocation)
        assert actual == [1]

    def test_get_station_ids(self):
        pdm_allocation = pdm_MCCSAllocation(
            mccs_allocation_id="mccs config 123",
            selected_subarray_definition=SubArrayLOW.AA05_ALL,
            subarray_beams=[
                pdm_SubarrayBeamConfiguration(
                    apertures=self.pdm_apertures_with_2_apertures,
                    number_of_channels=96,
                    subarray_beam_id=1,
                ),
            ],
        )
        actual = get_station_ids(pdm_allocation)
        assert actual[0][1] == 349

    def test_get_apertures(self):
        actual = get_allocation_apertures(self.pdm_subarray_beams_with_2_apertures)
        assert actual == self.cdm_apertures_with_2_apertures

    def test_create_mccs_allocation(self):
        pdm_csp_low = CSPConfiguration(
            config_id="csp-mvp01-20220329-00001",
            lowcbf=pdm_LowCBFConfiguration(
                correlation_spws=[
                    pdm_Correlation(
                        spw_id=1,
                        logical_fsp_ids=[0, 1],
                        zoom_factor=0,
                        centre_frequency=199.609375e6,
                        number_of_channels=88,
                        integration_time_ms=849,
                    ),
                    pdm_Correlation(
                        spw_id=1,
                        logical_fsp_ids=[0, 1],
                        zoom_factor=0,
                        centre_frequency=199.609375e6,
                        number_of_channels=12,
                        integration_time_ms=849,
                    ),
                ]
            ),
        )
        actual = create_mccs_allocation(
            SBDefinition(
                mccs_allocation=self.pdm_allocation_with_4_apertures,
                csp_configurations=[pdm_csp_low],
            )
        )
        expected_allocation_with_4_apertures = cdm_MCCSAllocate(
            interface="https://schema.skao.int/ska-low-mccs-controller-allocate/3.0",
            subarray_beams=[
                cdm_SubArrayBeamsConfiguration(
                    apertures=self.cdm_apertures_with_4_apertures,
                    number_of_channels=100,
                    subarray_beam_id=1,
                )
            ],
        )
        assert actual == expected_allocation_with_4_apertures


class TestMCCSConfigurationConversion:
    """
    Unit tests for MCCSConfiguration PDM to CDM transforms.
    """

    horizontal_target = Target(
        target_id="target #1",
        reference_coordinate=HorizontalCoordinates(
            az=0.0,
            el=45.0,
            unit=("deg", "deg"),
        ),
    )
    equatorial_target = Target(
        target_id="test_target",
        reference_coordinate=EquatorialCoordinates(
            ra=0.0,
            dec=60.0,
            reference_frame=EquatorialCoordinatesReferenceFrame.ICRS,
            unit=["hourangle", "deg"],
        ),
    )
    expected_horizontal = cdm_SubarrayBeamSkyCoordinates(
        c1=0.0,
        c2=45.0,
        target_name="target #1",
        reference_frame="topocentric",
    )
    expected_equatorial = cdm_SubarrayBeamSkyCoordinates(
        c1=0.0,
        c2=60.0,
        target_name="target #1",
        reference_frame="ICRS",
    )
    cbf_config = pdm_LowCBFConfiguration(
        correlation_spws=[
            pdm_Correlation(
                spw_id=1,
                logical_fsp_ids=[0, 1],
                zoom_factor=0,
                centre_frequency=199.609375e6,
                number_of_channels=96,
                integration_time_ms=849,
            )
        ]
    )
    cdm_apertures_with_2_apertures = [
        cdm_SubarrayBeamAperatures(aperture_id="AP344.01", weighting_key_ref="uniform"),
        cdm_SubarrayBeamAperatures(aperture_id="AP349.01", weighting_key_ref="uniform"),
    ]
    pdm_apertures_with_2_apertures = [
        pdm_Aperture(station_id=344, substation_id=1, weighting_key="uniform"),
        pdm_Aperture(station_id=349, substation_id=1, weighting_key="uniform"),
    ]
    pdm_subarray_beams_with_2_apertures = pdm_SubarrayBeamConfiguration(
        apertures=pdm_apertures_with_2_apertures,
        number_of_channels=96,
        subarray_beam_id=1,
    )
    pdm_allocation_with_2_apertures = pdm_MCCSAllocation(
        mccs_allocation_id="mccs config 1",
        selected_subarray_definition=SubArrayLOW.AA05_ALL,
        subarray_beams=[pdm_subarray_beams_with_2_apertures],
    )

    def test_get_sky_coordinates_based_on_invalid_target(self):
        """
        Verify that get_sky_coordinates_based_on_reference_coordinates throws type exception on invalidate target type.
        """
        with pytest.raises(TypeError):
            _ = get_sky_coordinates_based_on_reference_coordinates("abc")
        with pytest.raises(TypeError):
            _ = get_sky_coordinates_based_on_reference_coordinates(123)

    def test_get_sky_coordinates_based_on_invalid_coordinate_type(self):
        """
        Verify that get_sky_coordinates_based_on_reference_coordinates throw validate exception on unsupported target coordinate refer.
        """
        sso_target = Target(
            target_id="test_target",
            reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.URANUS),
        )
        with pytest.raises(TypeError):
            _ = get_sky_coordinates_based_on_reference_coordinates(sso_target)

    def test_get_sky_coordinates_based_with_valid_coordinate(self):
        """
        Verify that get_sky_coordinates_based_on_reference_coordinates converts to expected sky_coordinates.
        Test the supported horizontal reference_coordinate and equatorial reference_coordinate
        """
        actual_horizontal = get_sky_coordinates_based_on_reference_coordinates(
            self.horizontal_target
        )
        assert actual_horizontal == self.expected_horizontal
        actual_equatorial = get_sky_coordinates_based_on_reference_coordinates(
            self.equatorial_target
        )
        assert actual_equatorial == self.expected_equatorial

    def test_convert_mccs_configuration_with_horizontal_target(self):
        """
        Verify that convert_mccs_configuration converts to expected cdm_MCCSConfiguration.
        """
        expected_sky_coordinates = cdm_SubarrayBeamSkyCoordinates(
            reference_frame="topocentric",
            c1=0.0,
            c2=45.0,
        )
        expected_subarray_beams = cdm_SubarrayBeamConfiguration(
            update_rate=0.0,
            logical_bands=[{"start_channel": 208, "number_of_channels": 96}],
            apertures=self.cdm_apertures_with_2_apertures,
            sky_coordinates=expected_sky_coordinates,
            subarray_beam_id=1,
        )
        expected_mccs_configuration = cdm_MCCSConfiguration(
            subarray_beams=[expected_subarray_beams]
        )
        actual_mccs_configuration = convert_mccs_configuration(
            allocation=self.pdm_allocation_with_2_apertures,
            lowcbf=self.cbf_config,
            target=self.horizontal_target,
        )
        assert actual_mccs_configuration == expected_mccs_configuration

    def test_convert_mccs_configuration_with_equatorial_target(self):
        """
        Verify that convert_mccs_configuration converts to expected cdm_MCCSConfiguration.
        """
        expected_sky_coordinates = cdm_SubarrayBeamSkyCoordinates(
            reference_frame="ICRS",
            c1=0.0,
            c2=60.0,
        )
        expected_subarray_beams = cdm_SubarrayBeamConfiguration(
            update_rate=0.0,
            logical_bands=[{"start_channel": 208, "number_of_channels": 96}],
            apertures=self.cdm_apertures_with_2_apertures,
            sky_coordinates=expected_sky_coordinates,
            subarray_beam_id=1,
        )
        expected_mccs_configuration = cdm_MCCSConfiguration(
            subarray_beams=[expected_subarray_beams]
        )
        actual_mccs_configuration = convert_mccs_configuration(
            allocation=self.pdm_allocation_with_2_apertures,
            lowcbf=self.cbf_config,
            target=self.equatorial_target,
        )
        assert actual_mccs_configuration == expected_mccs_configuration
