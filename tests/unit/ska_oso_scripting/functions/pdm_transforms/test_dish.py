"""
Unit tests for the ska_oso_scripting.pdm_transforms module.

The purpose of these tests is to confirm that the pdm_transforms code converts
CDM entities to the expected equivalent PDM entities.
"""
import copy
from unittest.mock import patch

import pytest
from astropy import units as u
from ska_oso_pdm.sb_definition import (
    AltAzCoordinates,
    DishAllocation,
    EquatorialCoordinates,
    EquatorialCoordinatesReferenceFrame,
    FivePointParameters,
    PointingCorrection,
    PointingKind,
    PointingPattern,
    SinglePointParameters,
    SolarSystemObject,
    SolarSystemObjectName,
    SpiralParameters,
    Target,
)
from ska_oso_pdm.sb_definition.csp.midcbf import MidCBFConfiguration, ReceiverBand
from ska_tmc_cdm.messages.central_node.common import (
    DishAllocation as cdm_DishAllocation,
)
from ska_tmc_cdm.messages.skydirection import ICRSField as cdm_ICRSField
from ska_tmc_cdm.messages.skydirection import SpecialField as cdm_SpecialField
from ska_tmc_cdm.messages.subarray_node.configure import (
    DishConfiguration as cdm_DishConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure import (
    PointingConfiguration as cdm_PointingConfiguration,
)
from ska_tmc_cdm.messages.subarray_node.configure.core import (
    PointingCorrection as cdm_PointingCorrection,
)
from ska_tmc_cdm.messages.subarray_node.configure.core import (
    ReceiverBand as cdm_ReceiverBand,
)
from ska_tmc_cdm.messages.subarray_node.configure.core import (
    SpecialTarget as cdm_SpecialTarget,
)
from ska_tmc_cdm.messages.subarray_node.configure.core import Target as cdm_Target
from ska_tmc_cdm.messages.subarray_node.configure.receptorgroup import (
    FixedTrajectory as cdm_FixedTrajectory,
)
from ska_tmc_cdm.messages.subarray_node.configure.receptorgroup import (
    Projection as cdm_Projection,
)
from ska_tmc_cdm.messages.subarray_node.configure.receptorgroup import (
    ReceptorGroup as cdm_ReceptorGroup,
)

from ska_oso_scripting.functions.pdm_transforms.dish import (
    _create_field,
    _create_projection,
    _create_trajectory,
    convert_dishallocation,
    convert_pointingconfiguration,
    create_dishconfiguration,
)
from tests.unit.ska_oso_scripting.functions.pdm_transforms.test_csp import PDMData


class TestPointingConfigurationConversion:
    """
    Unit tests for PointingConfiguration PDM to CDM transforms.
    """

    ra = 0.1
    dec = 0.2
    name = "target1"
    frame = EquatorialCoordinatesReferenceFrame.ICRS
    unit = ["hourangle", "deg"]
    ref_coord = EquatorialCoordinates(ra=ra, dec=dec, reference_frame=frame, unit=unit)

    pdm_target = Target(target_id=name, reference_coordinate=ref_coord)

    pdm_target_altaz = Target(
        target_id="a survey",
        reference_coordinate=AltAzCoordinates(az=270.0, el=10.0),
    )

    pdm_target_sso = Target(
        target_id="mars",
        reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.MARS),
        pointing_pattern=PointingPattern(),
    )

    FivePoint = FivePointParameters(offset_arcsec=5.0)
    SinglePoint = SinglePointParameters(offset_x_arcsec=-5.0)
    Spiral = SpiralParameters()

    cdm_target = cdm_Target(
        ra=ra,
        dec=dec,
        unit=unit,
        reference_frame=frame.value,
        target_name=name,
    )

    cdm_field = cdm_ICRSField(
        target_name=name,
        attrs=cdm_ICRSField.Attrs(
            c1=u.Quantity(ra, unit[0]).to("deg").value,
            c2=u.Quantity(dec, unit[1]).to("deg").value,
            epoch=2000.0,
            radial_velocity=0.0,
        ),
    )

    def test_create_field_correctly_handles_equatorial_target(self):
        result = _create_field(TestPointingConfigurationConversion.pdm_target)
        assert result == TestPointingConfigurationConversion.cdm_field

    def test_create_field_correctly_handles_solar_system_target(self):
        result = _create_field(TestPointingConfigurationConversion.pdm_target_sso)
        expected = cdm_SpecialField(target_name="mars")
        assert expected == result

    def test_create_field_throws_exception_when_passed_an_unsupported_type(self):
        with pytest.raises(NotImplementedError):
            _create_field(TestPointingConfigurationConversion.pdm_target_altaz)

    def test_create_projection(self):
        projection = _create_projection(TestPointingConfigurationConversion.pdm_target)
        projection_altaz = _create_projection(
            TestPointingConfigurationConversion.pdm_target_altaz
        )
        projection_sso = _create_projection(
            TestPointingConfigurationConversion.pdm_target_sso
        )

        assert projection.alignment == "ICRS"
        assert projection_altaz.alignment == "AltAz"
        assert projection_sso.alignment == "ICRS"

    def test_create_trajectory_throws_error_when_passed_unsupported_pattern(self):
        target = copy.copy(TestPointingConfigurationConversion.pdm_target)
        target.pointing_pattern = PointingPattern(
            active=PointingKind.SPIRAL,
            parameters=[TestPointingConfigurationConversion.Spiral],
        )
        with pytest.raises(NotImplementedError):
            _create_trajectory(target)

    def test_create_trajectory_handles_single_point_multiple_parameters(self):
        target = copy.copy(TestPointingConfigurationConversion.pdm_target)
        target.pointing_pattern = PointingPattern(
            active=PointingKind.SINGLE_POINT,
            parameters=[
                TestPointingConfigurationConversion.FivePoint,
                TestPointingConfigurationConversion.SinglePoint,
            ],
        )
        result = _create_trajectory(target)

        assert isinstance(result, cdm_FixedTrajectory)
        assert result.attrs.x == -5.0
        assert result.attrs.y == 0

    def test_create_trajectory_handles_five_point_multiple_parameters(self):
        target = copy.copy(TestPointingConfigurationConversion.pdm_target)
        target.pointing_pattern = PointingPattern(
            active=PointingKind.FIVE_POINT,
            parameters=[
                TestPointingConfigurationConversion.FivePoint,
                TestPointingConfigurationConversion.SinglePoint,
            ],
        )
        result = _create_trajectory(target)

        assert isinstance(result, cdm_FixedTrajectory)
        assert result.attrs.x == 0
        assert result.attrs.y == 0

    @pytest.mark.parametrize(
        "pdm_correction,cdm_correction",
        [
            (PointingCorrection.MAINTAIN, None),
            (PointingCorrection.RESET, cdm_PointingCorrection.RESET),
            (PointingCorrection.UPDATE, cdm_PointingCorrection.UPDATE),
        ],
    )
    @patch(
        "ska_oso_scripting.functions.pdm_transforms.dish.WORKAROUNDS.disable_pointing_groups",
        new=False,
    )
    def test_transform(self, pdm_correction, cdm_correction):
        expected = cdm_PointingConfiguration(
            target=TestPointingConfigurationConversion.cdm_target,
            groups=[
                cdm_ReceptorGroup(
                    field=TestPointingConfigurationConversion.cdm_field,
                    trajectory=cdm_FixedTrajectory(
                        attrs=cdm_FixedTrajectory.Attrs(x=0.0, y=0.0)
                    ),
                    projection=cdm_Projection(alignment="ICRS"),
                )
            ],
            correction=cdm_correction,
        )
        actual = convert_pointingconfiguration(
            TestPointingConfigurationConversion.pdm_target, pdm_correction
        )

        assert actual == expected

    @pytest.mark.parametrize(
        "pdm_correction,cdm_correction",
        [
            (PointingCorrection.MAINTAIN, None),
            (PointingCorrection.RESET, cdm_PointingCorrection.RESET),
            (PointingCorrection.UPDATE, cdm_PointingCorrection.UPDATE),
        ],
    )
    def test_transform_with_pointing_groups_workaround(
        self, pdm_correction, cdm_correction
    ):
        """ "
        Test for convert_pointingconfiguration with the pointing groups workaround in place. Expect
        that when WORKAROUNDS.disable_pointing_groups is True (default) the groups is set to an empty
         list in the CDM PointingConfiguration.
        """
        expected = cdm_PointingConfiguration(
            target=TestPointingConfigurationConversion.cdm_target,
            groups=[],
            correction=cdm_correction,
        )
        actual = convert_pointingconfiguration(
            TestPointingConfigurationConversion.pdm_target, pdm_correction
        )

        assert actual == expected

    @staticmethod
    @patch(
        "ska_oso_scripting.functions.pdm_transforms.dish.WORKAROUNDS.disable_pointing_groups",
        new=False,
    )
    def test_convert_pointingconfiguration_complains_on_invalid_type():
        """
        Verify that convert_pointingconfiguration checks input argument type.
        """
        with pytest.raises(TypeError):
            convert_pointingconfiguration(1, None)

        # Only ra + dec supported currently so check that an error
        # is raised if any other coordinate system is used
        target = Target(
            target_id="target2",
            reference_coordinate=AltAzCoordinates(
                az=0.0,
                el=45.0,
                unit=["deg", "deg"],
            ),
        )
        with pytest.raises(NotImplementedError):
            convert_pointingconfiguration(target, PointingCorrection.MAINTAIN)

    @staticmethod
    @patch(
        "ska_oso_scripting.functions.pdm_transforms.dish.WORKAROUNDS.disable_pointing_groups",
        new=False,
    )
    def test_convert_pointingconfiguration_special():
        """
        Verify that convert_pointingconfiguration converts a special target.
        """

        pdm_target = Target(
            target_id="target3",
            reference_coordinate=SolarSystemObject(name=SolarSystemObjectName.MARS),
        )

        expected = cdm_PointingConfiguration(
            target=cdm_SpecialTarget(target_name=SolarSystemObjectName.MARS),
            groups=[
                cdm_ReceptorGroup(
                    field=cdm_SpecialField(target_name=SolarSystemObjectName.MARS),
                    trajectory=cdm_FixedTrajectory(
                        attrs=cdm_FixedTrajectory.Attrs(x=0.0, y=0.0)
                    ),
                    projection=cdm_Projection(alignment="ICRS"),
                )
            ],
            correction=None,
        )

        actual = convert_pointingconfiguration(pdm_target, PointingCorrection.MAINTAIN)

        assert actual == expected


class TestDishConfigurationConversion:
    """
    Unit tests for DishConfiguration PDM to CDM transforms.
    """

    @staticmethod
    def test_create_dishconfiguration_creates_expected_cdm_entity():
        midcbf = MidCBFConfiguration(
            frequency_band=ReceiverBand.BAND_1, subbands=PDMData.subbands
        )
        pdm_rx = ReceiverBand.BAND_1
        expected = cdm_DishConfiguration(receiver_band=cdm_ReceiverBand(pdm_rx.value))
        actual = create_dishconfiguration(midcbf)
        assert actual == expected

    @staticmethod
    def test_convert_dishconfiguration_complains_on_invalid_type():
        """
        Verify that convert_tmcconfiguration checks input argument type.
        """
        with pytest.raises(TypeError):
            create_dishconfiguration(1)


class TestDishAllocationConversion:
    """
    Unit tests for DishAllocation PDM to CDM transforms.
    """

    @staticmethod
    def test_covert_dishallocation_converts_to_expected_cdm_entity():
        pdm_dish_allocation = DishAllocation(dish_ids=["SKA001", "SKA002"])
        cdm_dish_allocation = cdm_DishAllocation(receptor_ids=["SKA001", "SKA002"])
        expected = convert_dishallocation(pdm_dish_allocation)
        assert expected == cdm_dish_allocation
