"""
Unit tests for the ska_oso_scripting.environment module
"""
import os
from unittest import mock

import pytest

from ska_oso_scripting.functions import environment


def test_ska_mid_is_assumed_when_when_environment_variable_is_not_set():
    """
    Verify execution environment is set to SKA MID when environment variable
    is not set.
    """
    assert environment.is_ska_mid_environment() is True
    assert environment.is_ska_low_environment() is False


@pytest.mark.parametrize(
    "vals,is_mid",
    [
        # SKA Mid values should be recognised
        (["mid", "MID", "ska mid", "SKA-mid", "SKA Mid"], True),
        # SKA Low values should be recognised
        (["low", "LOW", "ska low", "SKA-low", "SKA Low"], False),
        # unrecognised values should default to Mid
        (["", "foo"], True),
    ],
)
def test_telescope_environment_variable(vals, is_mid):
    """
    is_ska_mid_environment should return True when SKA-Mid is set in the
    environment variable or the value is not recognised.
    """
    for val in vals:
        with mock.patch.dict(os.environ, {"SKA_TELESCOPE": val}):
            assert environment.is_ska_mid_environment() is is_mid
            assert environment.is_ska_low_environment() is not is_mid


@mock.patch.object(environment, "is_ska_low_environment")
def test_check_environment_for_consistency_raises_exception_if_low_env(
    mock_low_env_fn, mid_allocation_request_json
):
    """
    Verify that check_environment_for_consistency raises an exception when
    Request is for Mid environment and we are in Low environment
    """
    mock_low_env_fn.return_value = True
    with pytest.raises(environment.ExecutionEnvironmentError):
        environment.check_environment_for_consistency(mid_allocation_request_json)


@mock.patch.object(environment, "is_ska_mid_environment")
def test_check_environment_for_consistency_raises_exception_if_mid_env(
    mock_mid_env_fn, low_allocation_request_json
):
    """
    Verify that check_environment_for_consistency raises an exception when
    Request is for Low environment and we are in Mid environment
    """
    mock_mid_env_fn.return_value = True
    with pytest.raises(environment.ExecutionEnvironmentError):
        environment.check_environment_for_consistency(low_allocation_request_json)
