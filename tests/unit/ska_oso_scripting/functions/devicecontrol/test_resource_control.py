"""
Unit tests for the ska_oso_scripting.functions.devicecontrol.resource_control module
"""
import os
import unittest.mock as mock
from pathlib import Path

import pytest
import ska_tmc_cdm.messages.central_node.release_resources as cdm_release
from ska_ost_osd.telvalidation.schematic_validation_exceptions import (
    SchematicValidationError,
)
from ska_tango_base.control_model import ObsState
from ska_tmc_cdm import schemas
from ska_tmc_cdm.messages.central_node.assign_resources import DishAllocation

from ska_oso_scripting.functions.devicecontrol import common, resource_control
from ska_oso_scripting.functions.pdm_transforms.wrapper import (
    create_cdm_assign_resources_request_from_scheduling_block,
)

from ... import utils

# Messages used for comparison in tests
VALID_LOW_DEALLOCATE_REQUEST = """{
  "interface": "https://schema.skao.int/ska-low-tmc-releaseresources/3.0",
  "subarray_id": 1,
  "release_all": true
}"""
VALID_MID_DEALLOCATE_REQUEST = """{
  "interface": "https://schema.skao.int/ska-tmc-releaseresources/2.1",
  "subarray_id": 1,
  "release_all": true
}"""


def test_get_assign_resources_command(mid_allocation_request_json):
    """
    Test get_assign_resources_command returns the expected command
    """
    command_expected = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "AssignResources",
        mid_allocation_request_json,
    )
    command_returned = resource_control.get_assign_resources_command(
        mid_allocation_request_json
    )

    utils.command_non_strict_equals(command_returned, command_expected)


@mock.patch.object(resource_control, "_call_and_wait_for_transition")
def test_assign_resources_defines_obsstate_transitions_correctly(
    mock_call_and_wait_fn,
    mid_allocation_request_cdm,
    mid_allocation_request_json_path,
):
    """
    Verify that assign_resources defines obsstate transitions correctly
    """
    subarray_id = 1

    resource_control.assign_resources(
        subarray_id=subarray_id,
        request=Path(mid_allocation_request_json_path),
        with_processing=False,
    )

    utils.validate_call_and_wait_for_transition_args(
        mock_call_and_wait_fn,  # pass in mock function used for this test
        "AssignResources",  # 'scan' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to SAN1, obsState read from SAN1
        "obsState",  # scan command should wait on the obsState
        [
            ObsState.RESOURCING,
            ObsState.IDLE,
        ],
    )


@mock.patch.object(resource_control, "_call_and_wait_for_transition")
@mock.patch.dict(os.environ, {"EB_ID": "eb-test-00000000-99999"})
def test_assign_resources_sets_eb_id(
    mock_call_and_wait_fn,
    mid_allocation_request_cdm,
    mid_allocation_request_json_path,
):
    """
    Test that the eb_id is taken from the environment variable and inserted into the AssignResourcesRequest, when with_processing=True
    """
    resource_control.assign_resources(
        subarray_id=1,
        request=Path(mid_allocation_request_json_path),
        with_processing=True,
    )
    request_json = mock_call_and_wait_fn.call_args.args[0].args[0]

    assert "eb-test-00000000-99999" in request_json


@mock.patch.object(resource_control, "_call_and_wait_for_transition")
def test_semantic_validation_for_invalid_assign_sbd(
    mock_call_and_wait_fn, semantically_invalid_mid_sbd
):
    """
    Test that semantic validation errors are raised and that no Tango call
    is made.
    """
    invalid_mid_sbd_assign_convert = (
        create_cdm_assign_resources_request_from_scheduling_block(
            1, semantically_invalid_mid_sbd
        )
    )
    with pytest.raises(SchematicValidationError):
        resource_control.assign_resources(
            subarray_id=1,
            request=invalid_mid_sbd_assign_convert,
            with_processing=True,
        )
    assert mock_call_and_wait_fn.call_count == 0


@mock.patch.object(resource_control.TANGO_REGISTRY, "get_central_node")
@mock.patch.object(resource_control, "get_cdm_interface")
def test_get_release_resources_command_with_request(
    mock_interface_fn,
    mock_get_cn_fn,
):
    """
    Test get_release_resources_command returns the expected command
    """
    subarray_id = 1
    interface = "https://schema.skao.int/ska-tmc-releaseresources/2.1"
    mock_get_cn_fn.return_value = utils.SKA_MID_CENTRAL_NODE_FQDN
    mock_interface_fn.return_value = interface
    request = cdm_release.ReleaseResourcesRequest(
        subarray_id=subarray_id,
        release_all=False,
        interface=interface,
        dish_allocation=DishAllocation(receptor_ids=["SKA001", "SKA002", "SKA003"]),
    )
    expected_cmd = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "ReleaseResources",
        schemas.CODEC.dumps(request),
    )
    cmd = resource_control.get_release_resources_command(
        subarray_id, release_all=False, request=request
    )
    utils.command_non_strict_equals(cmd, expected_cmd)


def test_get_release_resources_command_with_release_all():
    """
    Test get_release_resources_command returns the expected command when
    release_all parameter is set to True
    """
    subarray_id = 1
    expected_cmd = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "ReleaseResources",
        VALID_MID_DEALLOCATE_REQUEST,
    )
    cmd = resource_control.get_release_resources_command(subarray_id, release_all=True)
    utils.command_non_strict_equals(cmd, expected_cmd)


def test_get_release_resources_command_adds_missing_interface():
    """
    Verify that the release resources command adds an interface to the JSON
    if it is not present.
    """
    subarray_id = 1
    request = cdm_release.ReleaseResourcesRequest(
        subarray_id=subarray_id,
        release_all=False,
        dish_allocation=DishAllocation(receptor_ids=["SKA001", "SKA002", "SKA003"]),
    )
    cmd = resource_control.get_release_resources_command(
        subarray_id, release_all=False, request=request
    )
    request.interface = "https://schema.skao.int/ska-tmc-releaseresources/2.1"
    expected_cmd = common.Command(
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "ReleaseResources",
        schemas.CODEC.dumps(request),
    )
    utils.command_non_strict_equals(cmd, expected_cmd)


def test_release_resources_must_define_resources_argument_if_not_releasing_all():
    """
    Verify that the resources argument is defined if the command is not a
    command to release all sub-array resources.
    """
    try:
        resource_control.release_resources(1, release_all=False)
        # Fail test if ValueError is not raised
        assert False
    except ValueError as e:
        assert str(e) == "Either release_all or request must be defined"


def test_release_resources_enforces_boolean_release_all_argument():
    """
    Verify that the boolean release_all argument is required.
    """
    try:
        resource_control.release_resources(1, release_all=1)
        # Fail test if ValueError is not raised
        assert False
    except ValueError as e:
        assert str(e) == "release_all must be a boolean"


@mock.patch.object(resource_control, "_call_and_wait_for_transition")
def test_release_resources_defines_obsstate_transitions_correctly(
    mock_execute_fn,
):
    """
    Verify that release_resources defines sub-array obsState transitions as
    expected.
    """
    resource_control.release_resources(1, release_all=True)

    # Test that _call_and_wait_for_transition was correctly invoked.
    utils.validate_call_and_wait_for_transition_args(
        mock_execute_fn,
        "ReleaseResources",
        utils.SKA_MID_CENTRAL_NODE_FQDN,
        "obsState",
        [ObsState.RESOURCING, ObsState.EMPTY],
        VALID_MID_DEALLOCATE_REQUEST,
    )


@mock.patch.object(resource_control.TANGO_REGISTRY, "get_central_node")
@mock.patch.object(resource_control, "_call_and_wait_for_transition")
def test_release_resources_defines_obsstate_transitions_correctly_low(
    mock_execute_fn, get_cn_fn
):
    """
    Verify that release_resources defines sub-array obsState transitions as
    expected when using SKA Low telescope.
    """
    with mock.patch.dict(os.environ, {"SKA_TELESCOPE": "low"}):
        get_cn_fn.return_value = utils.SKA_LOW_CENTRAL_NODE_FQDN
        resource_control.release_resources(1, release_all=True)

        # Test that _call_and_wait_for_transition was correctly invoked.
        utils.validate_call_and_wait_for_transition_args(
            mock_execute_fn,
            "ReleaseResources",
            utils.SKA_LOW_CENTRAL_NODE_FQDN,
            "obsState",
            [ObsState.RESOURCING, ObsState.EMPTY],
            VALID_LOW_DEALLOCATE_REQUEST,
        )
