from unittest import mock

import pytest
import tango

from ska_oso_scripting.functions.devicecontrol import telescope_control

from ... import utils


@mock.patch.object(telescope_control, "_call_and_wait_for_transition")
def test_telescope_start_up_calls_tango_executor(mock_fn):
    """
    Test that the 'start telescope devices' command calls the target Tango
    device once only.
    """
    telescope_control.telescope_on()
    kwargs = mock_fn.call_args_list[0][1]
    assert kwargs["optional_transitions"] == [tango.DevState.UNKNOWN]

    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "TelescopeOn",  # 'startup' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to central node
        "telescopeState",  # startup command should wait on the telescopeState
        [tango.DevState.ON],  # happy path sequence is ON
    )


@mock.patch.object(telescope_control, "_call_and_wait_for_transition")
def test_telescope_start_up_calls_tango_executor_low(mock_call_and_wait_fn):
    """
    Test that the 'start telescope devices' command calls the target Tango
    device once only in SKA-Low environment.
    """
    telescope_control.telescope_on()
    kwargs = mock_call_and_wait_fn.call_args_list[0][1]
    assert kwargs["optional_transitions"] == [tango.DevState.UNKNOWN]

    utils.validate_call_and_wait_for_transition_args(
        mock_call_and_wait_fn,  # pass in mock function used for this test
        "TelescopeOn",  # 'startup' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to central node
        "telescopeState",  # startup command should wait on the State in Low environment
        [tango.DevState.ON],  # happy path sequence is ON
    )


@mock.patch.object(telescope_control, "_call_and_wait_for_transition")
def test_telescope_off_calls_tango_executor_with_default_final_state(mock_fn):
    """
    Test that the 'telescope devices to off' command expects OFF
    final state.
    """
    telescope_control.telescope_off()
    kwargs = mock_fn.call_args_list[0][1]
    assert kwargs["optional_transitions"] == [tango.DevState.UNKNOWN]
    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "TelescopeOff",  # 'off' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to central node
        "telescopeState",  # startup command should wait on the telescopeState
        [tango.DevState.OFF],  # happy path sequence is OFF
    )


@mock.patch.object(telescope_control, "_call_and_wait_for_transition")
@pytest.mark.parametrize("final_state", [tango.DevState.STANDBY, tango.DevState.OFF])
def test_telescope_off_calls_tango_executor_with_optional_args(mock_fn, final_state):
    """
    Test that the 'telescope devices to off' command calls the target
    Tango device once only, passing the optional final state argument when
    supplied.
    """
    telescope_control.telescope_off(final_state)
    kwargs = mock_fn.call_args_list[0][1]
    assert kwargs["optional_transitions"] == [tango.DevState.UNKNOWN]
    utils.validate_call_and_wait_for_transition_args(
        mock_fn,  # pass in mock function used for this test
        "TelescopeOff",  # 'off' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to central node
        "telescopeState",  # startup command should wait on the telescopeState
        [final_state],  # happy path sequence is given final_state parameter
    )


@mock.patch.object(telescope_control, "_call_and_wait_for_transition")
def test_telescope_off_calls_tango_executor_low(mock_call_and_wait_fn):
    """
    Test that the 'telescope devices to off' command calls the target
    Tango device once only.
    """
    telescope_control.telescope_off()
    telescope_control.get_telescope_off_command()

    utils.validate_call_and_wait_for_transition_args(
        mock_call_and_wait_fn,  # pass in mock function used for this test
        "TelescopeOff",  # 'off' command is requested
        utils.SKA_MID_CENTRAL_NODE_FQDN,  # command sent to central node
        "telescopeState",  # startup command should wait on the State in Low environment
        [tango.DevState.OFF],  # happy path sequence is OFF
    )


def test_get_start_telescope_command():
    """
    Verify that a 'telescope on' Command is targeted and structured
    correctly.
    """
    cmd = telescope_control.get_telescope_start_up_command()
    assert cmd.device == utils.SKA_MID_CENTRAL_NODE_FQDN
    assert cmd.command_name == "TelescopeOn"
    assert not cmd.args


def test_get_telescope_off_command():
    """
    Verify that a 'telescope off' Command is targeted and structured
    correctly.
    """
    cmd = telescope_control.get_telescope_off_command()
    assert cmd.device == utils.SKA_MID_CENTRAL_NODE_FQDN
    assert cmd.command_name == "TelescopeOff"
    assert not cmd.args
