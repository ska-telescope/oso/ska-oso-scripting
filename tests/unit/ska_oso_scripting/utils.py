import json
import unittest.mock as mock
from typing import List

from ska_oso_scripting.functions.devicecontrol.common import Command, DeviceStateTypes

SKA_MID_CENTRAL_NODE_FQDN = "ska_mid/tm_central/central_node"
SKA_MID_SUBARRAY_NODE_1_FQDN = "ska_mid/tm_subarray_node/1"
SKA_LOW_CENTRAL_NODE_FQDN = "ska_low/tm_central/central_node"
SKA_LOW_SUBARRAY_NODE_1_FQDN = "ska_low/tm_subarray_node/1"

MID_CN_ASSIGN_RESOURCES_SUCCESS_RESPONSE = ("SKA001", "SKA002")


def command_non_strict_equals(command: Command, expected: Command):
    """
    Confirms if two Command objects are equal, ignoring any difference arising
    from whitespace in JSON payload.
    """
    assert command.device == expected.device
    assert command.command_name == expected.command_name

    command_json = json.loads(command.args[0])
    expected_command_json = json.loads(expected.args[0])
    assert command_json == expected_command_json


def validate_call_and_wait_for_transition_args(
    mock_fn: mock.MagicMock,
    command_name: str,
    command_device: str,
    expected_attribute: str,
    happy_path: List[DeviceStateTypes],
    command_json: str = None,
):
    """
    Helper function for testing observing tasks that call
    _call_and_wait_for_transition.

    :param mock_fn: mock _call_and_wait_for_transition function
    :param command_name: command that should be called
    :param command_device: device command should be sent to
    :param expected_attribute: attribute that should be monitored
    :param happy_path: happy path sequence of ObsStates
    :param command_json: optional, if not None, checked against
    JSON sent in Command object
    :return:
    """
    # verify fn called exactly once
    assert mock_fn.call_count == 1

    # get call args from that call
    cmd, wait_values, attribute_name = mock_fn.call_args_list[0][0]

    if command_json:
        # TODO: Change this helper function to take Command object
        #  and modify all tests that use this to pass in the object
        command_non_strict_equals(
            cmd, Command(command_device, command_name, command_json)
        )
    else:
        # assert expected command called on expected device
        assert cmd.command_name == command_name
        assert cmd.device == command_device

    # assert happy path values are as expected
    assert happy_path == wait_values

    # assert the correct attribute was monitored
    assert expected_attribute == attribute_name
