"""
Unit tests for the ska_oso_scripting.objects module.
"""
import unittest.mock as mock

import pytest

import ska_oso_scripting.functions.devicecontrol as devicecontrol
from ska_oso_scripting.objects import SubArray, Telescope


class TestSubArray:
    """
    Unit tests for ska_oso_scripting.objects.SubArray
    """

    @pytest.mark.parametrize("subarray_id", [1, 1.0, "1"])
    def test_constructor_accepts_valid_arguments(self, subarray_id):
        """
        Check that the SubArray constructor accepts integer IDs.
        """
        sub_array = SubArray(subarray_id)
        assert sub_array.id == 1

    @pytest.mark.parametrize("subarray_id", [-1, 0, "1a", None])
    def test_constructor_rejects_invalid_arguments(self, subarray_id):
        """
        Verify that the SubArray constructor requires IDs to be positive.
        """
        with pytest.raises(ValueError):
            _ = SubArray(subarray_id)

    def test_repr(self):
        """
        Verify that the SubArray repr is formatted correctly.
        """
        sub_array = SubArray(1)
        assert repr(sub_array) == "<SubArray(1)>"

    @pytest.mark.parametrize(
        "cmd,fn",
        [
            (SubArray.abort, devicecontrol.abort),
            (SubArray.end, devicecontrol.end),
            (SubArray.reset, devicecontrol.obsreset),
            (SubArray.restart, devicecontrol.restart),
            (SubArray.scan, devicecontrol.scan),
        ],
    )
    def test_commands_call_correct_observing_task(self, cmd, fn):
        """
        Confirm that the Telescope commands call the expected observing task
        exactly once.
        """
        cmd_name = cmd.__name__
        fn_name = fn.__name__

        subarray = SubArray(1)
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            cmd = getattr(subarray, cmd_name)
            cmd()
        mock_fn = getattr(mock_module, fn_name)
        mock_fn.assert_called_once()

    def test_assign_from_file_calls_correct_observing_task(self):
        """
        confirm that the 'configure a subarray from exported CDM' command calls
        the correct observing task exactly once, setting with_processing to True
        by default so that scan IDs etc. are made consistent across the
        configuration.
        """
        subarray = SubArray(1)
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            subarray.assign_from_file("cdm_file", timeout=None)
        mock_module.assign_resources_from_file.assert_called_once_with(
            subarray.id, "cdm_file", with_processing=True, timeout=None
        )

    def test_assign_from_cdm_calls_correct_observing_task(self):
        """
        confirm that the 'allocate a subarray from CDM' command calls the correct
        observing task exactly once.
        """
        subarray = SubArray(1)
        mock_cdm = mock.Mock()
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            subarray.assign_from_cdm(mock_cdm, timeout=None)
        mock_module.assign_resources_from_cdm.assert_called_once_with(
            subarray.id, mock_cdm, timeout=None
        )

    def test_configure_from_cdm_calls_correct_observing_task(self):
        """
        confirm that the 'configure a subarray from exported CDM' command calls
        the correct observing task exactly once.
        """
        subarray = SubArray(1)
        mock_cdm = mock.Mock()
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            subarray.configure_from_cdm(mock_cdm, timeout=None)
        mock_module.configure_from_cdm.assert_called_once_with(
            subarray.id, mock_cdm, timeout=None
        )

    def test_configure_from_file_calls_correct_observing_task(self):
        """
        confirm that the 'configure a subarray from exported CDM' command calls
        the correct observing task exactly once, setting with_processing to True
        by default so that scan IDs etc. are made consistent across the
        configuration.
        """
        subarray = SubArray(1)
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            subarray.configure_from_file("foo", timeout=None)
        mock_module.configure_from_file.assert_called_once_with(
            subarray.id, "foo", with_processing=True, timeout=None
        )


class TestTelescope:
    """
    Unit tests for ska_oso_scripting.objects.Telescope
    """

    def test_telescope_repr(self):
        """
        Verify that the Telescope repr is formatted correctly.
        """
        telescope = Telescope()
        assert repr(telescope) == "<Telescope>"

    @pytest.mark.parametrize(
        "cmd,fn",
        [
            (Telescope.on, devicecontrol.telescope_on),
            (Telescope.off, devicecontrol.telescope_off),
        ],
    )
    def test_commands_call_correct_observing_task(self, cmd, fn):
        """
        Confirm that the Telescope commands call the expected observing task
        exactly once.
        """
        cmd_name = cmd.__name__
        fn_name = fn.__name__

        telescope = Telescope()
        with mock.patch("ska_oso_scripting.objects.devicecontrol") as mock_module:
            cmd = getattr(telescope, cmd_name)
            cmd()
        mock_fn = getattr(mock_module, fn_name)
        mock_fn.assert_called_once()
