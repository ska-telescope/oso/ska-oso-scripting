import logging
import os

from ska_control_model import ObsState
from ska_oso_pdm.sb_definition.sb_definition import TelescopeType
from ska_oso_tmc_integration_tests.tmcsim import CentralNode, SubArrayNode
from ska_ser_logging import configure_logging
from ska_tango_testing.harness import TangoTestHarness

from ska_oso_scripting.functions.devicecontrol.common import EXECUTOR
from ska_oso_scripting.functions.devicecontrol.tango_executor import (
    TangoDeviceProxyFactory,
)

configure_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)


class OSOTestHarness:
    """
    OSOTestHarness is an integration test harness for OSO scripting. It
    populates a Tango test context with simulations of TMC devices that
    implement the minimal functionality required to test script execution.
    """

    def __init__(self, telescope: TelescopeType = TelescopeType.SKA_MID):
        """
        Create a new OSOTestHarness.

        The default base_uri targets SKA MID and MUST be overridden for SKA LOW tests.
        """
        if telescope is TelescopeType.SKA_MID:
            self._base_uri = "ska_mid"
        elif telescope is TelescopeType.SKA_LOW:
            self._base_uri = "ska_low"
        else:
            raise ValueError(f"Unsupported: {telescope}")
        self.telescope = telescope
        self._tango_test_harness = TangoTestHarness()

        self.fail_after = {}

    def add_central_node(self):
        self._tango_test_harness.add_device(
            device_name=f"{self._base_uri}/tm_central/central_node",
            device_class=CentralNode,
            base_uri=self._base_uri,
        )

    def add_subarray(
        self,
        subarray_id: int,
        initial_obsstate: ObsState = ObsState.EMPTY,
    ):
        """
        Add a subarray to the test harness.

        The subarray obsState will be set to the default obsState of EMPTY
        unless overridden.
        """
        fqdn = os.path.join(f"{self._base_uri}/tm_subarray_node", str(subarray_id))
        # TODO pass in duration and fail_after values?
        device_props = dict(initial_obsstate=initial_obsstate.value)
        self._tango_test_harness.add_device(
            device_name=fqdn, device_class=SubArrayNode, **device_props
        )

    def add_subarray_fault_injection(self, subarray_id, states_str: str):
        self.fail_after = {subarray_id: states_str}

    def __enter__(self):
        ctx = self._tango_test_harness.__enter__()
        # Hold onto this so we can restore it, so we're not permanently
        # modifying the environment...
        self._old_env_var = os.environ.get("SKA_TELESCOPE")
        os.environ["SKA_TELESCOPE"] = str(self.telescope)
        # monkeypatch the scripting ProxyFactory to return DeviceProxy instances from
        # our test harness rather than 'real' DeviceProxy instances - but first, cache
        # the original implementation so that it can be restored later, as other tests
        # may depend on the original behaviour
        self._orig_impl = TangoDeviceProxyFactory.__call__
        TangoDeviceProxyFactory.__call__ = ctx.get_device

        return ctx

    def __exit__(self, exc_type, exception, trace):
        # SubscriptionManager event subscriptions prevent the TangoTestHarness
        # from exiting its context, resulting in hanging tests.
        EXECUTOR._evt_strategy._subscription_manager._unsubscribe_all()
        if self._old_env_var is None:
            del os.environ["SKA_TELESCOPE"]
        else:
            os.environ["SKA_TELESCOPE"] = self._old_env_var

        # now restore original implementation
        TangoDeviceProxyFactory.__call__ = self._orig_impl

        return self._tango_test_harness.__exit__(exc_type, exception, trace)
